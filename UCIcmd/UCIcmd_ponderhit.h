/**************************************************************************
**
**   Name: UCIcmd_ponderhit.h
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#ifndef UCICMD_PONDERHIT_H
#define UCICMD_PONDERHIT_H

#include "spdlog/spdlog.h"
#include "UCIcmd/UCIcmd.h"

class UCIcmd_ponderhit : public UCIcmd
{
public:
    UCIcmd_ponderhit(const std::string& id_name, const std::string& restOfCmd, Communicator* comm):
        UCIcmd(id_name, restOfCmd, comm)
    {

    }

    virtual void executeCmd();
};

#endif // UCICMD_PONDERHIT_H

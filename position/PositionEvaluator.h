/**************************************************************************
**
**   Name: PositionEvaluator.h
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#ifndef POSITIONEVALUATOR_H
#define POSITIONEVALUATOR_H

#include "PositionBB.h"
#include "move_generation/MoveGenerator.h"

class PositionEvaluator
{
public:
    PositionEvaluator(const MoveGenerator& move_gen);

    const MoveGenerator& move_gen_;
    static const std::array<double, 6> pieceValue;
    static constexpr double mateValue = 10000;
    const std::array<U8, 64> DistanceFromCenter { {
        6, 5, 4, 3, 3, 4, 5, 6,
        5, 4, 3, 2, 2, 3, 4, 5,
        4, 3, 2, 1, 1, 2, 3, 4,
        3, 2, 1, 0, 0, 1, 2, 3,
        3, 2, 1, 0, 0, 1, 2, 3,
        4, 3, 2, 1, 1, 2, 3, 4,
        5, 4, 3, 2, 2, 3, 4, 5,
        6, 5, 4, 3, 3, 4, 5, 6
    }};

    enum class EvalType {NORMAL, ENDGAME, MATE_ONLY};

    /**
     * @brief Calculates
     * @param sq1
     * @param sq2
     * @return
     */
    U8 distance(Square sq1, Square sq2) const;

    double eval_without_see(const PositionBB& pos) const;

    double eval_with_see(const PositionBB& pos) const;

    double eval_endgame(const PositionBB& pos) const;

    double eval_mateOnly(const PositionBB& pos) const;

    double eval(const PositionBB& pos, EvalType evalType) const;

    bool isKingInCheck(const PositionBB& pos, const SideToPlay color_of_king) const;

    bool arePawnsOnBoard(const PositionBB& pos);

    /**
     * Calculate static exchange evaluation - SEE for given square.
     * Basically, it will find out, if the exchange is worth playing out.
     * See documentation for more details.
     *
     * Positive result means
     * side to play will get value equal to result
     * by taking figure on square sq. The exchange should be done.
     *
     * Negative value means side to play
     * gains nothing by playing the exchange out.
     */
    double see(const PositionBB& pos, Square sq) const;

    /**
     * Takes all squares attacked by side to play
     * and calculates SEE for each one of them.
     *
     * Return max of this set times sign, if the "max" is greater than 0.
     * Otherwise, returns 0.
     *
     * Sign is +1 if side to play is white and -1 if it is black.
     */
    double see_max(const PositionBB& pos) const;

    /**
     * Generate set of all attackers that can attack square sq.
     * Also generate set of possible X ray attackers.
     *
     * X ray attackers cannot directly attack sq now,
     * because of other attacking figures blocking their view.
     * They will be able to attack it in future,
     * if the blocking figure will move out.
     *
     */
    void getAttackersOnSq(const PositionBB &pos, const Square sq,
                          BB &currentAttackers,
                          BB &possibleXrayAttackers) const;


    BB getLowestAttacker(const BB attackers, const SideToPlay sideToMove, const std::array<BB, 12> &pieces, FigureType &lowestType) const;

    BB addXrayAttacker(const BB tempOccupied, const Square sq, FigureType prev_attacker, BB &possibleXrayAttackers) const;
};

#endif // POSITIONEVALUATOR_H

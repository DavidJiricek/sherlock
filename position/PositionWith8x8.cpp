/**************************************************************************
**
**   Name: PositionWith8x8.cpp
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#include "PositionWith8x8.h"

const std::map<char,FigureTypeColoured> PositionWith8x8::FEN_FigureTypeMap = {
    {'P', W_PAWN}, {'N', W_KNIGHT}, {'B', W_BISHOP}, {'R', W_ROOK}, {'Q', W_QUEEN},{'K', W_KING},
    {'p', B_PAWN}, {'n', B_KNIGHT}, {'b', B_BISHOP}, {'r', B_ROOK}, {'q', B_QUEEN},{'k', B_KING}
};

const std::string PositionWith8x8::startposFEN = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";

PositionWith8x8::PositionWith8x8(const std::string &FENstring) {

    for(auto &&x:chessBoard){
        x = NONE;
    }

    // Format of FEN string should be correct
    auto iter = std::find(FENstring.cbegin(), FENstring.cend(), ' ');

    std::string allRanks(FENstring.cbegin(), iter);
    std::string restOfFEN(++iter, FENstring.cend());


    // First deal with allRanks
    auto iterLeader = allRanks.cbegin();
    auto iterBehind = allRanks.cbegin();

    int rankNumber = 7;
    while (iterLeader != allRanks.cend()) {
        iterLeader = std::find(iterBehind, allRanks.cend(), '/');
        FENrank_ToChessBoard(rankNumber,std::string(iterBehind, iterLeader));
        iterBehind = iterLeader;
        ++iterBehind; //jump over '/'
        --rankNumber;
    }

    // Then deal with rest of the information
    iterLeader = restOfFEN.cbegin();
    iterBehind = restOfFEN.cbegin();
    enum partOfFEN {SIDE, CASTLING, ENPASSANT, HALFMOVE, FULLMOVE, ALL_OK};
    partOfFEN currentPart = SIDE;
    while (iterLeader != restOfFEN.cend()) {
        iterLeader = std::find(iterBehind, restOfFEN.cend(), ' ');
        std::string s(iterBehind, iterLeader);
        switch (currentPart) {
        case SIDE:
            if (s[0] == 'w'){
                sideToPlay = WHITE;
            } else if (s[0] == 'b') {
                sideToPlay = BLACK;
            }
            currentPart = CASTLING;
            break;
        case CASTLING:
            if (s.find('K') != std::string::npos){
                castlingWhiteKside = true;
            } else {
                castlingWhiteKside = false;
            }
            if (s.find('k') != std::string::npos){
                castlingBlackKside = true;
            } else {
                castlingBlackKside = false;
            }
            if (s.find('Q') != std::string::npos){
                castlingWhiteQside = true;
            } else {
                castlingWhiteQside = false;
            }
            if (s.find('q') != std::string::npos){
                castlingBlackQside = true;
            } else {
                castlingBlackQside = false;
            }
            currentPart = ENPASSANT;
            break;
        case ENPASSANT:
            if (s.size()==2){
                enPassantPossible = true;
                enPassantTargetSquare = static_cast<Square>(8*(s[1]-'1')+(s[0]-'a'));
            } else {
                enPassantPossible = false;
                enPassantTargetSquare = static_cast<Square>(0);
            }
            currentPart = HALFMOVE;
            break;
        case HALFMOVE:
            halfMoveClock = std::stoi(s);
            currentPart = FULLMOVE;
            break;
        case FULLMOVE:
            fullMoveClock = std::stoi(s);
            currentPart = ALL_OK;
            break;
        default:
            break;
        }
        iterBehind = iterLeader;
        ++iterBehind; //jump over ' '
    }
    if (currentPart == HALFMOVE){
        halfMoveClock = 0;
        fullMoveClock = 0;
    } else if (currentPart == FULLMOVE) {
        fullMoveClock = 0;
    }
}

void PositionWith8x8::update(Move_LongNotation move)
{
    this->fullMoveClock++;
    this->halfMoveClock++;
    this->enPassantPossible = false;
    this->enPassantTargetSquare = A1;

    FigureTypeColoured moved_piece = this->chessBoard[move.fromSq_];
    FigureTypeColoured target_piece = this->chessBoard[move.toSq_];

    // Reset halfMoveClock if pawn was moved or piece was captured
    if (target_piece != NONE || moved_piece == W_PAWN || moved_piece == B_PAWN){
        halfMoveClock = 0;
    }

    // Check castling rights
    switch (move.fromSq_) {
    case E1:
        this->castlingWhiteKside = false;
        this->castlingWhiteQside = false;
        break;
    case A1:
        this->castlingWhiteQside = false;
        break;
    case H1:
        this->castlingWhiteKside = false;
        break;
    case E8:
        this->castlingBlackKside = false;
        this->castlingBlackQside = false;
        break;
    case A8:
        this->castlingBlackQside = false;
        break;
    case H8:
        this->castlingBlackKside = false;
        break;
    default:
        break;
    }

    // Find enpassant square for white pawn
    if (moved_piece == W_PAWN && (move.toSq_ - move.fromSq_) == 16){
        this->enPassantPossible=true;
        this->enPassantTargetSquare=static_cast<Square>(move.fromSq_ + 8);
    }
    // Find enpassant square for black pawn
    if (moved_piece == B_PAWN && (move.fromSq_ - move.toSq_) == 16){
        this->enPassantPossible=true;
        this->enPassantTargetSquare=static_cast<Square>(move.fromSq_ - 8);
    }

    sideToPlay = static_cast<SideToPlay>(~sideToPlay);

    // Alter chess board
    // ------------------
    chessBoard[move.fromSq_]=NONE;
    chessBoard[move.toSq_]=moved_piece;

    // Promotion
    // ---------
    switch (move.flag_) {
    case Move_LongNotation::Promotion_Flag::KNIGHT:
        if (move.toSq_ > Square::H7){
            chessBoard[move.toSq_]=W_KNIGHT;
        } else {
            chessBoard[move.toSq_]=B_KNIGHT;
        }
        break;
    case Move_LongNotation::Promotion_Flag::BISHOP:
        if (move.toSq_ > Square::H7){
            chessBoard[move.toSq_]=W_BISHOP;
        } else {
            chessBoard[move.toSq_]=B_BISHOP;
        }
        break;
    case Move_LongNotation::Promotion_Flag::ROOK:
        if (move.toSq_ > Square::H7){
            chessBoard[move.toSq_]=W_ROOK;
        } else {
            chessBoard[move.toSq_]=B_ROOK;
        }
        break;
    case Move_LongNotation::Promotion_Flag::QUEEN:
        if (move.toSq_ > Square::H7){
            chessBoard[move.toSq_]=W_QUEEN;
        } else {
            chessBoard[move.toSq_]=B_QUEEN;
        }
        break;
    default:
        break;
    }

    // White Kside castling
    if (moved_piece == W_KING && move.fromSq_ == E1 && move.toSq_ == G1){
        chessBoard[H1]=NONE;
        chessBoard[F1]=W_ROOK;
    }
    // White Qside castling
    if (moved_piece == W_KING && move.fromSq_ == E1 && move.toSq_ == C1){
        chessBoard[A1]=NONE;
        chessBoard[D1]=W_ROOK;
    }
    // Black Kside castling
    if (moved_piece == W_KING && move.fromSq_ == E8 && move.toSq_ == G8){
        chessBoard[H8]=NONE;
        chessBoard[F8]=B_ROOK;
    }
    // Black Qside castling
    if (moved_piece == W_KING && move.fromSq_ == E8 && move.toSq_ == C8){
        chessBoard[A8]=NONE;
        chessBoard[D8]=B_ROOK;
    }

    // Enpassant
    if (moved_piece == W_PAWN && target_piece == NONE && (move.toSq_ - move.fromSq_)%8 != 0){
        chessBoard[move.toSq_-8]=NONE;
    }
    if (moved_piece == B_PAWN && target_piece == NONE && (move.fromSq_ - move.toSq_)%8 != 0){
        chessBoard[move.toSq_+8]=NONE;
    }


}

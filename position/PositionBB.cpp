/**************************************************************************
**
**   Name: PositionBB.cpp
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2018
**
**************************************************************************/
#include "PositionBB.h"

void PositionBB::alterChessBoard(BB figPositions, FigureTypeColoured figType, std::array<FigureTypeColoured, 64> &chessBoard) const {
    for (uint i = 0; i < chessBoard.size(); ++i) {
        if (figPositions & (1ULL << i)){
            chessBoard[i] = figType;
        }
    }
}

PositionWith8x8 PositionBB::toPositionWith8x8() const {

    std::array<FigureTypeColoured, 64> chessBoard;

    std::fill(std::begin(chessBoard), std::end(chessBoard), NONE);

    const std::array<FigureTypeColoured, 13> FigureTypeColouredArray
    {{W_PAWN, W_KNIGHT, W_BISHOP, W_ROOK, W_QUEEN, W_KING,
                    B_PAWN, B_KNIGHT, B_BISHOP, B_ROOK, B_QUEEN, B_KING, NONE}} ;

    for (auto &&figTypeColor : FigureTypeColouredArray){
        if (figTypeColor == NONE) break;
        alterChessBoard(pieces[figTypeColor], figTypeColor, chessBoard);
    }

    AbstractPosition a = *this;

    PositionWith8x8 result(a,chessBoard);
    return result;
}

void PositionBB::visualize() const{
    PositionWith8x8 thisPositionIn8x8 = (this->toPositionWith8x8());
    thisPositionIn8x8.visualize();
}

PositionBB::PositionBB(PositionWith8x8 pos)
    : AbstractPosition(pos)
{
    const std::array<FigureTypeColoured, 64> chessBoard = pos.chessBoard;
    const std::array<FigureTypeColoured, 13> FigureTypeColouredArray
    {{W_PAWN, W_KNIGHT, W_BISHOP, W_ROOK, W_QUEEN, W_KING,
                    B_PAWN, B_KNIGHT, B_BISHOP, B_ROOK, B_QUEEN, B_KING, NONE}} ;

    std::map<FigureTypeColoured, std::array<bool, 64>> BBsInArrays;
    for (auto &&figType : FigureTypeColouredArray){
        std::array<bool, 64> array;
        std::fill(std::begin(array), std::end(array), false);

        BBsInArrays.emplace(std::make_pair(figType, array));
    }

    for (uint i = 0; i < chessBoard.size(); ++i) {
        FigureTypeColoured figOnSquare = chessBoard[i];
        (BBsInArrays.find(figOnSquare)->second)[i] = true;
    }

    for (auto &&figType : FigureTypeColouredArray){
        BB anotherBB = Bitboard::arrayToBB(BBsInArrays.find(figType)->second);
        if (figType == NONE) break;

        int index = static_cast<int>(figType);

        pieces[figType] = anotherBB;
        pieces[index] = anotherBB;
    }
    whiteOccupies = 0;
    blackOccupies = 0;
    for (int i = 0; i < 6; ++i){
        whiteOccupies |= pieces[i];
        blackOccupies |= pieces[i+6];
    }
}

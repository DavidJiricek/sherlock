/**************************************************************************
**
**   Name: Tests.h
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#ifndef TESTS_H
#define TESTS_H

#include "types.h"
#include "position/PositionBB.h"
#include "move_generation/MoveGenerator.h"

/**
 * Class that is used only for testing.
 */
class Tests
{
public:
    Tests();

    const std::map<std::string, U64> perft5_results =
        {{"r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0", 193690690},
         {"n1rq1bk1/1b2r1pn/1p2p1Bp/pP1pN2P/P2P1BP1/4Q3/3N1P2/R2R2K1 w - - 1 29", 57360551},
        { "rnbqkbnr/ppppp1pp/8/5p2/8/1P2P3/P1PP1PPP/RNBQKBNR b KQkq - 0 2", 9945075},
        { "4R3/8/6P1/8/6kP/8/8/r5K1 w - - 3 127", 340635}};


    static U64 depthFirstSearch(const PositionBB &root, int max_depth, int current_depth, const MoveGenerator &gen, bool divide = true);

    static U64 onlyCapSearch(const PositionBB &root, int max_depth, int current_depth, const MoveGenerator &gen, bool divide = true);

    static void run2();

    static void run();

    static void run3();

    static void run_just_kernel(const std::string& FEN);
    static void perft(const std::string &FEN);
    static void capture_perft(const std::string &FEN);
    static void test_see(const std::string &FEN);
    static void test_eval(const std::string &FEN);
    static void test_distance();
};

#endif // TESTS_H

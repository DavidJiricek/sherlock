/**************************************************************************
**
**   Name: PositionEvaluator.cpp
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#include "PositionEvaluator.h"
#include "utilities/Bitboard.h"

const std::array<double, 6> PositionEvaluator::pieceValue = {1.0,3.25,3.25,5.0,9.0,10000.0};

U8 PositionEvaluator::distance(Square sq1, Square sq2) const
{
    return std::abs((static_cast<U8>(sq1)%8 - static_cast<U8>(sq2)%8)) +
            std::abs((static_cast<U8>(sq1)/8 - static_cast<U8>(sq2)/8));
}

PositionEvaluator::PositionEvaluator(const MoveGenerator& move_gen)
    : move_gen_(move_gen)
{

}

double PositionEvaluator::eval_without_see(const PositionBB &pos) const
{
    double eval = 0;
    eval += ( Bitboard::count_1s(pos.pieces[0]) - Bitboard::count_1s(pos.pieces[6]) ) * pieceValue[0];
    eval += ( Bitboard::count_1s(pos.pieces[1]) - Bitboard::count_1s(pos.pieces[7]) ) * pieceValue[1];
    eval += ( Bitboard::count_1s(pos.pieces[2]) - Bitboard::count_1s(pos.pieces[8]) ) * pieceValue[2];
    eval += ( Bitboard::count_1s(pos.pieces[3]) - Bitboard::count_1s(pos.pieces[9]) ) * pieceValue[3];
    eval += ( Bitboard::count_1s(pos.pieces[4]) - Bitboard::count_1s(pos.pieces[10])) * pieceValue[4];

    eval += Bitboard::count_1s(pos.whiteOccupies & Bitboard::smallCenter)*0.02;
    eval += Bitboard::count_1s(pos.whiteOccupies & Bitboard::bigCenter)*0.01;

    eval -= Bitboard::count_1s(pos.blackOccupies & Bitboard::smallCenter)*0.02;
    eval -= Bitboard::count_1s(pos.blackOccupies & Bitboard::bigCenter)*0.01;

    eval -= Bitboard::count_1s((pos.pieces[1] | pos.pieces[2]) & Bitboard::rank1)*0.02;
    eval += Bitboard::count_1s((pos.pieces[7] | pos.pieces[8]) & Bitboard::rank8)*0.02;

    eval -= Bitboard::count_1s(pos.pieces[5] & pos.attackedSqByBlack)*0.15;
    eval += Bitboard::count_1s(pos.pieces[11] & pos.attackedSqByWhite)*0.15;

    return eval;
}

double PositionEvaluator::eval_with_see(const PositionBB &pos) const
{
    double eval = 0;
    eval += ( Bitboard::count_1s(pos.pieces[0]) - Bitboard::count_1s(pos.pieces[6]) ) * pieceValue[0];
    eval += ( Bitboard::count_1s(pos.pieces[1]) - Bitboard::count_1s(pos.pieces[7]) ) * pieceValue[1];
    eval += ( Bitboard::count_1s(pos.pieces[2]) - Bitboard::count_1s(pos.pieces[8]) ) * pieceValue[2];
    eval += ( Bitboard::count_1s(pos.pieces[3]) - Bitboard::count_1s(pos.pieces[9]) ) * pieceValue[3];
    eval += ( Bitboard::count_1s(pos.pieces[4]) - Bitboard::count_1s(pos.pieces[10])) * pieceValue[4];

    eval += Bitboard::count_1s(pos.whiteOccupies & Bitboard::smallCenter)*0.02;
    eval += Bitboard::count_1s(pos.whiteOccupies & Bitboard::bigCenter)*0.01;

    eval -= Bitboard::count_1s(pos.blackOccupies & Bitboard::smallCenter)*0.02;
    eval -= Bitboard::count_1s(pos.blackOccupies & Bitboard::bigCenter)*0.01;

    eval -= Bitboard::count_1s((pos.pieces[1] | pos.pieces[2]) & Bitboard::rank1)*0.02;
    eval += Bitboard::count_1s((pos.pieces[7] | pos.pieces[8]) & Bitboard::rank8)*0.02;

    eval -= Bitboard::count_1s(pos.pieces[5] & pos.attackedSqByBlack)*0.15;
    eval += Bitboard::count_1s(pos.pieces[11] & pos.attackedSqByWhite)*0.15;

    eval += see_max(pos);

    return eval;
}

double PositionEvaluator::eval_endgame(const PositionBB &pos) const
{
    double eval = 0;
    eval += ( Bitboard::count_1s(pos.pieces[0]) - Bitboard::count_1s(pos.pieces[6]) ) * pieceValue[0];
    eval += ( Bitboard::count_1s(pos.pieces[1]) - Bitboard::count_1s(pos.pieces[7]) ) * pieceValue[1];
    eval += ( Bitboard::count_1s(pos.pieces[2]) - Bitboard::count_1s(pos.pieces[8]) ) * pieceValue[2];
    eval += ( Bitboard::count_1s(pos.pieces[3]) - Bitboard::count_1s(pos.pieces[9]) ) * pieceValue[3];
    eval += ( Bitboard::count_1s(pos.pieces[4]) - Bitboard::count_1s(pos.pieces[10])) * pieceValue[4];

    BB edge = Bitboard::rank8 | Bitboard::rank1 | Bitboard::fileA | Bitboard::fileH;

    if (eval > 0) {
        eval += 0.15*DistanceFromCenter[static_cast<U8>(Bitboard::find_lsb(pos.pieces[11]))];
        eval -= 0.08*distance(static_cast<Square>(Bitboard::find_lsb(pos.pieces[5])),
                                static_cast<Square>(Bitboard::find_lsb(pos.pieces[11])));
        eval -= Bitboard::count_1s(pos.pieces[5] & edge)*0.4;
    } else if (eval < 0) {
        eval -= 0.15*DistanceFromCenter[static_cast<U8>(Bitboard::find_lsb(pos.pieces[5]))];
        eval += 0.08*distance(static_cast<Square>(Bitboard::find_lsb(pos.pieces[5])),
                                static_cast<Square>(Bitboard::find_lsb(pos.pieces[11])));
        eval += Bitboard::count_1s(pos.pieces[11] & edge)*0.4;
    }

    eval += see_max(pos);

    return eval;
}

double PositionEvaluator::eval_mateOnly(const PositionBB &pos) const
{
    if (isKingInCheck(pos,pos.sideToPlay)){
        std::vector<MV> mate_evasions;
        move_gen_.allPossibleMoves(pos, mate_evasions);
        if (mate_evasions.empty()){
            return (1-2*pos.sideToPlay)*PositionEvaluator::mateValue;
        } else {
            return 0;
        }
    } else {
        return 0;
    }
}

double PositionEvaluator::eval(const PositionBB &pos, PositionEvaluator::EvalType evalType) const
{
    switch (evalType) {
    case PositionEvaluator::EvalType::NORMAL:
        return  eval_with_see(pos);
        break;
    case PositionEvaluator::EvalType::ENDGAME:
        return  eval_endgame(pos);
        break;
    case PositionEvaluator::EvalType::MATE_ONLY:
        return  eval_mateOnly(pos);
        break;
    default:
        // Should never return, but better than leave it uninitialized
        return PositionEvaluator::mateValue;
    }
}

bool PositionEvaluator::isKingInCheck(const PositionBB &pos, const SideToPlay color_of_king) const
{
    if (color_of_king == WHITE){
       return pos.pieces[5] & pos.attackedSqByBlack;
    } else {
       return pos.pieces[11] & pos.attackedSqByWhite;
    }
}

bool PositionEvaluator::arePawnsOnBoard(const PositionBB &pos)
{
    return static_cast<bool>(pos.pieces[0] | pos.pieces[6]);
}

double PositionEvaluator::see(const PositionBB &pos, Square sq) const
{
    SideToPlay currentSideToMove = pos.sideToPlay;
    std::array<double,32> gain;

    BB sqBB = 1ULL << sq;
    if (currentSideToMove == WHITE){
        for (int i = 0; i < 6; ++i) {
            if (sqBB & pos.pieces[i+6]){
                gain[0] = pieceValue[i];
            }
        }
    } else {
        for (int i = 0; i < 6; ++i) {
            if (sqBB & pos.pieces[i]){
                gain[0] = pieceValue[i];
            }
        }
    }

    BB currentAttackers;
    BB possibleXrayAttackers;
    BB tempOccup = pos.whiteOccupies | pos.blackOccupies;
    getAttackersOnSq(pos, static_cast<Square>(sq), currentAttackers, possibleXrayAttackers);
    //Bitboard::visualizeBB(currentAttackers);
    //Bitboard::visualizeBB(possibleXrayAttackers);


    int i = 0;
    while (true) {
        FigureType lowestType;
        BB lowest = getLowestAttacker(currentAttackers, currentSideToMove, pos.pieces, lowestType);
        if (lowest){
            i++;
            gain[i] = pieceValue[lowestType] - gain[i-1];
        } else {
            if (i == 0){
                i++; //abych nesahl do prazdneho mista v pameti
            }
            break;
        }
        //if (std::max(-gain[i-1], gain[i]) < 0) break;
        currentAttackers ^= lowest;
        tempOccup ^= lowest;
        if (possibleXrayAttackers){
            currentAttackers |= addXrayAttacker(tempOccup,sq,lowestType,possibleXrayAttackers);
        }
        currentSideToMove = static_cast<SideToPlay>(~currentSideToMove);
    }
//    if (i < 2){
//        return 0;
//    } else {
//        i = i - 2;
//    }
    while (--i){
       gain[i-1]= -std::max (-gain[i-1], gain[i]);
    }
    return gain[0];
}

double PositionEvaluator::see_max(const PositionBB &pos) const
{
    double max_see = 0.0;
    if (pos.sideToPlay == WHITE){
       BB attackedPieces = pos.attackedSqByWhite & pos.blackOccupies;
       while (attackedPieces) {
           max_see = std::max(max_see, see(pos, static_cast<Square>(Bitboard::zero_lsb(attackedPieces))));
       }
       return max_see;
    } else {
       BB attackedPieces = pos.attackedSqByBlack & pos.whiteOccupies;
       while (attackedPieces) {
           max_see = std::max(max_see, see(pos, static_cast<Square>(Bitboard::zero_lsb(attackedPieces))));
       }
       return -max_see;
    }
}

void PositionEvaluator::getAttackersOnSq(const PositionBB &pos, const Square sq,
                                         BB& currentAttackers,
                                         BB& possibleXrayAttackers) const
{
    BB pawnAttackers = 0;
    // Solve white pawn attackers
    if (sq > 15) {
        pawnAttackers |= (( 1ULL << (sq-7) ) + ( 1ULL << (sq-9) )) & pos.pieces[0];
    }
    // Solve black pawn attackers
    if (sq < 47) {
        pawnAttackers |= (( 1ULL << (sq+7) ) + ( 1ULL << (sq+9) )) & pos.pieces[6];
    }

    // Update current and possible attackers - pawns
    currentAttackers = pawnAttackers;
    possibleXrayAttackers = 0;

    BB diagonalSliders = pos.pieces[2] | pos.pieces[4] | pos.pieces[8] | pos.pieces[10];
    BB straightSliders = pos.pieces[3] | pos.pieces[4] | pos.pieces[9] | pos.pieces[10];

    BB occupied = pos.whiteOccupies | pos.blackOccupies;

    BB bishAllBlockers = move_gen_.BishopMasks[sq] & occupied;
    BB bishNoDiagBlockers = move_gen_.BishopMasks[sq] & (occupied & (~diagonalSliders) & (~pawnAttackers));

    BB rookAllBlockers = move_gen_.RookMasks[sq] & occupied;
    BB rookNoStraigBlockers = move_gen_.RookMasks[sq] & ( occupied & (~straightSliders) );


    int bishHashAll = move_gen_.computeHash(bishAllBlockers,
                                     move_gen_.bishopMagicNumbers[sq],
                                     move_gen_.BishopIndexBitCounts[sq]);
    int bishHashNoDiag = move_gen_.computeHash(bishNoDiagBlockers,
                                     move_gen_.bishopMagicNumbers[sq],
                                     move_gen_.BishopIndexBitCounts[sq]);
    int rookHashAll = move_gen_.computeHash(rookAllBlockers,
                                     move_gen_.rookMagicNumbers[sq],
                                     move_gen_.RookIndexBitCounts[sq]);
    int rookHashNoStraig = move_gen_.computeHash(rookNoStraigBlockers,
                                     move_gen_.rookMagicNumbers[sq],
                                     move_gen_.RookIndexBitCounts[sq]);


    // Solve diagonal attackers - bishops, queen
    currentAttackers |= move_gen_.bishopHashedAttacks[sq][bishHashAll] & diagonalSliders;
    possibleXrayAttackers = move_gen_.bishopHashedAttacks[sq][bishHashNoDiag] & diagonalSliders;

    // Solve straight attackers - rooks, queen
    currentAttackers |= move_gen_.rookHashedAttacks[sq][rookHashAll] & straightSliders;
    possibleXrayAttackers |= move_gen_.rookHashedAttacks[sq][rookHashNoStraig] & straightSliders;

    currentAttackers |= move_gen_.KnightMasks[sq] & (pos.pieces[1] | pos.pieces[7]);
    currentAttackers |= move_gen_.KingMasks[sq] & (pos.pieces[5] | pos.pieces[11]);

    // Possible X ray attackers cannot attack now
    possibleXrayAttackers = possibleXrayAttackers & (~currentAttackers);
}

BB PositionEvaluator::addXrayAttacker(const BB tempOccupied, const Square sq, FigureType prev_attacker, BB& possibleXrayAttackers) const
{
    BB result = 0;
    if (prev_attacker == FigureType::PAWN
            || prev_attacker == FigureType::BISHOP
            || prev_attacker == FigureType::QUEEN) {
        BB bishAllBlockers = move_gen_.BishopMasks[sq] & tempOccupied;
        int bishHashAll = move_gen_.computeHash(bishAllBlockers,
                                         move_gen_.bishopMagicNumbers[sq],
                                         move_gen_.BishopIndexBitCounts[sq]);
        result |= move_gen_.bishopHashedAttacks[sq][bishHashAll] & possibleXrayAttackers;
    }

    if (prev_attacker == FigureType::ROOK
            || prev_attacker == FigureType::QUEEN) {
        BB rookAllBlockers = move_gen_.RookMasks[sq] & tempOccupied;
        int rookHashAll = move_gen_.computeHash(rookAllBlockers,
                                         move_gen_.rookMagicNumbers[sq],
                                         move_gen_.RookIndexBitCounts[sq]);
        result |= move_gen_.rookHashedAttacks[sq][rookHashAll] & possibleXrayAttackers;
    }
    possibleXrayAttackers &= (~result);
    return result;
}

BB PositionEvaluator::getLowestAttacker(const BB attackers, const SideToPlay sideToMove,
                                        const std::array<BB, 12>& pieces_in_eval_pos, FigureType& lowestType) const
{
    if (sideToMove == WHITE){
        for (int i = 0; i < 6; ++i) {
            BB subset = attackers & pieces_in_eval_pos[i];
            if (subset){
                lowestType = static_cast<FigureType>(i);
                return (1ULL << Bitboard::find_lsb(subset));
            }
        }
    } else {
        for (int i = 0; i < 6; ++i) {
            BB subset = attackers & pieces_in_eval_pos[i+6];
            if (subset){
                lowestType = static_cast<FigureType>(i);
                return (1ULL << Bitboard::find_lsb(subset));
            }
        }
    }
    lowestType = static_cast<FigureType>(0);
    return 0ULL;
}

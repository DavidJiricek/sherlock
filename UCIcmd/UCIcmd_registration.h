/**************************************************************************
**
**   Name: UCIcmd_registration.h
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#ifndef UCICMD_REGISTRATION_H
#define UCICMD_REGISTRATION_H

#include "UCIcmd/UCIcmd.h"

class UCIcmd_registration : public UCIcmd
{
public:
    UCIcmd_registration(const std::string& id_name, const std::string& restOfCmd, Communicator* comm):
        UCIcmd(id_name, restOfCmd, comm)
    {

    }

    virtual void executeCmd();
};

#endif // UCICMD_REGISTRATION_H

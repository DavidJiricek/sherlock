/**************************************************************************
**
**   Name: UCIcmd_stop.h
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#ifndef UCICMD_STOP_H
#define UCICMD_STOP_H

#include "spdlog/spdlog.h"
#include "UCIcmd/UCIcmd.h"

class UCIcmd_stop : public UCIcmd
{
public:
    UCIcmd_stop(const std::string& id_name, const std::string& restOfCmd, Communicator* comm):
        UCIcmd(id_name, restOfCmd, comm)
    {

    }

    virtual void executeCmd();
};

#endif // UCICMD_STOP_H

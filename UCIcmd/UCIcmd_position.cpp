/**************************************************************************
**
**   Name: UCIcmd_position.cpp
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#include "UCIcmd_position.h"
#include "utilities/TextProccesing.h"
#include "position/PositionWith8x8.h"

void UCIcmd_position::executeCmd(){

    /* position [fen <fenstring> | startpos ]  moves <move1> .... <movei>
        set up the position described in fenstring on the internal board and
        play the moves on the internal chess board.
        if the game was played  from the start position the string "startpos" will be sent
        Note: no "new" command is needed. However, if this position is from a different game than
        the last position sent to the engine, the GUI should have sent a "ucinewgame" inbetween.

        Example

        position startpos moves e2e4
        position fen rnbqkbnr/pppppppp/8/8/3P4/8/PPP1PPPP/RNBQKBNR w KQkq - 0 1 moves e2e3
        go wtime 300000 btime 300000 winc 0 binc 0
    */


    std::string FEN_part;
    std::string FEN_itself;
    std::vector<std::string> moves;


    // String restOfCmd is already without duplicate whitespaces
    // and all whitespaces are equal to ordinary space ' '
    size_t index;
    std::string delim = " moves ";
    if ((index = restOfCmd_.find(delim)) != std::string::npos){
        FEN_part = restOfCmd_.substr(0, index);
        std::string moves_part = restOfCmd_.substr(index + delim.length());
        moves = TextProccesing::separateString(moves_part, [](char a){return isspace(a);});
    } else {
        FEN_part = restOfCmd_;
        // no moves
    }

    if (FEN_part == "startpos"){
        FEN_itself = PositionWith8x8::startposFEN;
    } else {
        // format is : fen <FEN_itself>
        // we need to remove first part "fen "
        FEN_itself = FEN_part.substr(4);
    }

    PositionWith8x8 pos(FEN_itself);
    for (auto&& x : moves){
        Move_LongNotation moveLN(x);
        pos.update(moveLN);
    }

    comm_->kernel_.setRootPosBB(pos);
    comm_->sendToGUI("Set up position with FEN " + comm_->kernel_.getRootPosFEN());
}


/**************************************************************************
**
**   Name: UCIcmd_uci.h
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#ifndef UCICMD_UCI_H
#define UCICMD_UCI_H

#include "spdlog/spdlog.h"

#include "UCIcmd/UCIcmd.h"

class UCIcmd_uci : public UCIcmd
{
public:
    UCIcmd_uci(const std::string& id_name, const std::string& restOfCmd, Communicator* comm):
        UCIcmd(id_name, restOfCmd, comm)
    {

    }

    virtual void executeCmd();
};

#endif // UCICMD_UCI_H

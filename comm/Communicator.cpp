/**************************************************************************
**
**   Name: Communicator.cpp
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2018
**
**************************************************************************/
#include <thread>
#include <pthread.h>

#include "Communicator.h"

#include "UCIcmd/UCIcmd.h"

#include "UCIcmd/UCIcmd_uci.h"
#include "UCIcmd/UCIcmd_ready.h"
#include "UCIcmd/UCIcmd_newgame.h"
#include "UCIcmd/UCIcmd_position.h"
#include "UCIcmd/UCIcmd_go.h"
#include "UCIcmd/UCIcmd_debug.h"
#include "UCIcmd/UCIcmd_registration.h"
#include "UCIcmd/UCIcmd_setoption.h"
#include "UCIcmd/UCIcmd_quit.h"
#include "UCIcmd/UCIcmd_ponderhit.h"
#include "UCIcmd/UCIcmd_stop.h"
#include "UCIcmd/UCIcmd_perft.h"

#include "spdlog/spdlog.h"
#include "comm/Reciever.h"

Communicator::Communicator(std::istream& inStream,
                           std::ostream& outStream,
                           Kernel &kernel)
   :kernel_(kernel),inStream_(inStream), outStream_(outStream),
    debugMode_(true), isCommunicatorWorking(true)
{
    spdlog::info("Setting up comm object...");

    setDebugMode(true);

    std::thread rec{Reciever(buffer_, inStream_)};
    rec.detach();
    spdlog::info("Setting up comm object... Done.");
}

void Communicator::setDebugMode(bool debugMode){
    debugMode_ = debugMode;
    if (debugMode){
        spdlog::flush_on(spdlog::level::debug);
        spdlog::set_level(spdlog::level::debug);
    } else {
        spdlog::set_level(spdlog::level::info);
    }
}

void Communicator::commWithGUI()
{
    while (isCommunicatorWorking) {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));

        std::string cmd_from_buffer = buffer_.try_pop();
        if (cmd_from_buffer == ""){
            continue;
        } else {
            std::unique_ptr<UCIcmd> cmd = parseCmd(cmd_from_buffer);
            cmd->executeCmd();
        }
    }
}

std::unique_ptr<UCIcmd> Communicator::parseCmd(const std::string &input)
{
    std::string cmd = TextProccesing::removeExtraWhitespaces(input);

    std::string id;
    if (!cmd.empty()) {
       id = TextProccesing::popFirstPart(cmd, [](char a){return isspace(a);});
       return std::move(builtUCIcmd(id, cmd));
    } else {
        throw CommExcep();
    }
}

void Communicator::sendToGUI(const std::string& message) const
{
    outStream_ << message << std::endl;
    spdlog::debug("Engine to GUI: " + message);
}

void Communicator::sendAndPrintInGUI(const std::string& message) const
{
    sendToGUI("info string " + message);
    spdlog::debug("Engine to GUI: info string " + message);
}

void Communicator::quit()
{
    isCommunicatorWorking = false;
    spdlog::info("Turning off communicator...");
}

std::unique_ptr<UCIcmd> Communicator::builtUCIcmd(std::string& id, std::string& restOfCmd)
{
    if (id == "uci"){
        return std::move(std::make_unique<UCIcmd_uci>(id,restOfCmd, this));
    } else if (id == "isready") {
        return std::move(std::make_unique<UCIcmd_ready>(id,restOfCmd, this));
    } else if (id == "ucinewgame") {
        return std::move(std::make_unique<UCIcmd_newgame>(id,restOfCmd, this));
    } else if (id == "position") {
        return std::move(std::make_unique<UCIcmd_position>(id,restOfCmd, this));
    } else if (id == "go") {
        return std::move(std::make_unique<UCIcmd_go>(id,restOfCmd, this));
    } else if (id == "debug") {
        return std::move(std::make_unique<UCIcmd_debug>(id,restOfCmd, this));
    } else if (id == "setoption") {
        return std::move(std::make_unique<UCIcmd_setoption>(id,restOfCmd, this));
    } else if (id == "registration") {
        return std::move(std::make_unique<UCIcmd_registration>(id,restOfCmd, this));
    } else if (id == "quit") {
        return std::move(std::make_unique<UCIcmd_quit>(id,restOfCmd, this));
    } else if (id == "ponderhit") {
        return std::move(std::make_unique<UCIcmd_ponderhit>(id,restOfCmd, this));
    } else if (id == "stop") {
        return std::move(std::make_unique<UCIcmd_stop>(id,restOfCmd, this));
    } else if (id == "perft") {
        return std::move(std::make_unique<UCIcmd_perft>(id,restOfCmd, this));
    } else {
        throw CommExcep(id + " " + restOfCmd);
    }
}







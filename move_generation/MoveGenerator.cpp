/**************************************************************************
**
**   Name: MoveGenerator.cpp
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2018
**
**************************************************************************/
#include "MoveGenerator.h"
#include "MagicGenerator.h"
#include "utilities/Bitboard.h"

#include <chrono>

MoveGenerator::MoveGenerator()
{
    for (int sq = 0; sq < 64; ++sq) {
        RookMasks[sq] = generateMask(static_cast<Square>(sq), ROOK);
        BishopMasks[sq] = generateMask(static_cast<Square>(sq), BISHOP);
        QueenMasks[sq] = generateMask(static_cast<Square>(sq), QUEEN);
        KingMasks[sq] = generateMask(static_cast<Square>(sq), KING);
        KnightMasks[sq] = generateMask(static_cast<Square>(sq), KNIGHT);
    }

    MagicGenerator magic(RookMasks, BishopMasks);


    for (int sq = 0; sq < 64; ++sq) {
        bishopHashedAttacks.resize(64);
        rookHashedAttacks.resize(64);
        //U64 mag;
        //std::array<BB, 4096> haAt;
        //magic.generateMagicAndHashTable(static_cast<Square>(sq), BISHOP, mag, haAt);

        //magic.generateMagicAndHashTable(static_cast<Square>(sq), ROOK, mag, haAt);

        magic.fillHashTable(static_cast<Square>(sq), BISHOP, bishopMagicNumbers[sq], bishopHashedAttacks[sq]);
        magic.fillHashTable(static_cast<Square>(sq), ROOK, rookMagicNumbers[sq], rookHashedAttacks[sq]);




        /*auto bishopPair = magic.generateMagicPair(static_cast<Square>(sq), BISHOP);
        auto rookPair = magic.generateMagicPair(static_cast<Square>(sq), ROOK);
        bishopMagicNumbers[sq] = bishopPair.first;
        bishopHashedAttacks.push_back(bishopPair.second);
        rookMagicNumbers[sq] = rookPair.first;
        rookHashedAttacks.push_back(rookPair.second);*/
    }
}

BB MoveGenerator::generateMask(Square sq, FigureType figType)
{
    int sq_rank = sq/8;
    int sq_file = sq%8;
    BB mask = 0;

    int rank = sq_rank;
    int file = sq_file;
    if (figType == BISHOP) {
        for (rank = sq_rank + 1, file = sq_file + 1; rank < 7 && file < 7; ++rank, ++file){
            mask |= (1ULL << (rank*8 + file));
        }
        for (rank = sq_rank - 1, file = sq_file + 1; rank > 0  && file < 7; --rank, ++file){
            mask |= (1ULL  << (rank*8 + file));
        }
        for (rank = sq_rank - 1, file = sq_file - 1; rank > 0  && file > 0; --rank, --file){
            mask |= (1ULL  << (rank*8 + file));
        }
        for (rank = sq_rank + 1, file = sq_file - 1; rank < 7  && file > 0; ++rank, --file){
            mask |= (1ULL  << (rank*8 + file));
        }
    } else if (figType == ROOK) {
        for (rank = sq_rank + 1, file = sq_file; rank < 7; ++rank){
            mask |= (1ULL  << (rank*8 + file));
        }
        for (rank = sq_rank, file = sq_file + 1; file < 7; ++file){
            mask |= (1ULL  << (rank*8 + file));
        }
        for (rank = sq_rank - 1, file = sq_file; rank > 0; --rank){
            mask |= (1ULL  << (rank*8 + file));
        }
        for (rank = sq_rank, file = sq_file - 1; file > 0;  --file){
            mask |= (1ULL  << (rank*8 + file));
        }
    } else if (figType == QUEEN) {
        mask = generateMask(sq, BISHOP) | generateMask(sq, ROOK);
    } else if (figType == KING) {
        for (rank = sq_rank - 1; rank <= sq_rank + 1; ++rank){
            for (file = sq_file - 1; file <= sq_file + 1; ++file) {
                if (rank >= 0 && rank <= 7 && file >= 0 && file <= 7){
                    mask |= (1ULL << (rank*8 + file));
                } else {
                    continue;
                }
            }
        }
    } else if (figType == KNIGHT) {
        for (int i = 0; i < 8; ++i) {
            rank = sq_rank + KnightHelpTableRanks[i];
            file = sq_file + KnightHelpTableFiles[i];
            if (rank >= 0 && rank <= 7 && file >= 0 && file <= 7){
                 mask |= (1ULL << (rank*8 + file));
            } else {
                continue;
            }
        }
    }
    return mask;
}

bool MoveGenerator::allPossibleMoves(const PositionBB &pos, std::vector<MV>& otherMoves) const
{

    //Determine side to play
    int offset;
    BB attackedSqByEnemy;
    BB friendOccup;
    BB enemyOccup;
    if (pos.sideToPlay == WHITE){
       offset = 0;
       attackedSqByEnemy = pos.attackedSqByBlack;
       friendOccup = pos.whiteOccupies;
       enemyOccup = pos.blackOccupies;
    } else {
       offset = 6;
       attackedSqByEnemy = pos.attackedSqByWhite;
       friendOccup = pos.blackOccupies;
       enemyOccup = pos.whiteOccupies;
    }

    //Check if king is in check
    if (pos.pieces[5 + offset] & attackedSqByEnemy){        
        BB sqToBlockCheck = 0;
        if (isInDoubleCheck(pos, sqToBlockCheck, offset, pos.sideToPlay)){
            // Only king can move
            kingMoves(pos.pieces[5 + offset], enemyOccup, friendOccup, attackedSqByEnemy, otherMoves);
        } else {
            // In check - no castling, just update rights
            BB pin = identifyPinnedFigures(pos, otherMoves, true); // only identification, not moving
            BB nonPin = ~pin;
            enPassant(pos.pieces[0 + offset], 1ULL << pos.enPassantTargetSquare, pos.sideToPlay, otherMoves, pos, sqToBlockCheck);
            pawnPushes(pos.pieces[0 + offset] & nonPin, pos.sideToPlay, friendOccup | enemyOccup, otherMoves, sqToBlockCheck);
            pawnCaptures(pos.pieces[0 + offset] & nonPin, enemyOccup, pos.sideToPlay, otherMoves, sqToBlockCheck);
            knightMoves(pos.pieces[1 + offset] & nonPin, enemyOccup, friendOccup, otherMoves, sqToBlockCheck);
            bishopMoves(pos.pieces[2 + offset] & nonPin, enemyOccup, friendOccup, otherMoves, sqToBlockCheck);
            rookMoves(pos.pieces[3 + offset] & nonPin, enemyOccup, friendOccup, otherMoves, sqToBlockCheck);
            queenMoves(pos.pieces[4 + offset] & nonPin, enemyOccup, friendOccup, otherMoves, sqToBlockCheck);
            kingMoves(pos.pieces[5 + offset], enemyOccup, friendOccup, attackedSqByEnemy, otherMoves);
        }

    } else {
        // Not in check
        BB pin = identifyPinnedFigures(pos, otherMoves, false); // also move them, even solve pinned enPassant
        BB nonPin = ~pin;
        castling(pos, otherMoves);
        enPassant(pos.pieces[0 + offset] & nonPin, 1ULL << pos.enPassantTargetSquare, pos.sideToPlay, otherMoves, pos);
        pawnCaptures(pos.pieces[0 + offset] & nonPin, enemyOccup, pos.sideToPlay, otherMoves);
        knightMoves(pos.pieces[1 + offset] & nonPin, enemyOccup, friendOccup, otherMoves);
        bishopMoves(pos.pieces[2 + offset] & nonPin, enemyOccup, friendOccup, otherMoves);
        queenMoves(pos.pieces[4 + offset] & nonPin, enemyOccup, friendOccup, otherMoves);
        rookMoves(pos.pieces[3 + offset] & nonPin, enemyOccup, friendOccup, otherMoves); // zmena poradi
        pawnPushes(pos.pieces[0 + offset] & nonPin, pos.sideToPlay, friendOccup | enemyOccup, otherMoves); // zmena poradi
        kingMoves(pos.pieces[5+ offset], enemyOccup, friendOccup, attackedSqByEnemy, otherMoves);
    }
    return otherMoves.empty();
}

bool MoveGenerator::allPossibleCaptures(const PositionBB &pos, std::vector<MV> &otherMoves) const
{
    //Determine side to play
    int offset;
    BB attackedSqByEnemy;
    BB friendOccup;
    BB enemyOccup;
    if (pos.sideToPlay == WHITE){
       offset = 0;
       attackedSqByEnemy = pos.attackedSqByBlack;
       friendOccup = pos.whiteOccupies;
       enemyOccup = pos.blackOccupies;
    } else {
       offset = 6;
       attackedSqByEnemy = pos.attackedSqByWhite;
       friendOccup = pos.blackOccupies;
       enemyOccup = pos.whiteOccupies;
    }

    //Check if king is in check
    if (pos.pieces[5 + offset] & attackedSqByEnemy){
        BB sqToBlockCheck = 0;
        if (isInDoubleCheck(pos, sqToBlockCheck, offset, pos.sideToPlay)){
            // Only king can move
            kingCaptures(pos.pieces[5 + offset], enemyOccup, attackedSqByEnemy, otherMoves);
        } else {
            // In check - no castling, just update rights
            BB pin = identifyPinnedFigures_forCapture(pos, otherMoves, true); // only identification, not moving
            BB nonPin = ~pin;
            enPassant(pos.pieces[0 + offset], 1ULL << pos.enPassantTargetSquare, pos.sideToPlay, otherMoves, pos, sqToBlockCheck);
            pawnCaptures(pos.pieces[0 + offset] & nonPin, enemyOccup, pos.sideToPlay, otherMoves, sqToBlockCheck);
            knightCaptures(pos.pieces[1 + offset] & nonPin, enemyOccup, otherMoves, sqToBlockCheck);
            bishopCaptures(pos.pieces[2 + offset] & nonPin, enemyOccup, friendOccup, otherMoves, sqToBlockCheck);
            rookCaptures(pos.pieces[3 + offset] & nonPin, enemyOccup, friendOccup, otherMoves, sqToBlockCheck);
            queenCaptures(pos.pieces[4 + offset] & nonPin, enemyOccup, friendOccup, otherMoves, sqToBlockCheck);
            kingCaptures(pos.pieces[5 + offset], enemyOccup, attackedSqByEnemy, otherMoves);
        }

    } else {
        // Not in check
        BB pin = identifyPinnedFigures_forCapture(pos, otherMoves, false); // also move them, even solve pinned enPassant
        BB nonPin = ~pin;
        // castling(pos, otherMoves);
        enPassant(pos.pieces[0 + offset] & nonPin, 1ULL << pos.enPassantTargetSquare, pos.sideToPlay, otherMoves, pos);
        //pawnPushes(pos.pieces[0 + offset] & nonPin, pos.sideToPlay, friendOccup | enemyOccup, otherMoves);
        pawnCaptures(pos.pieces[0 + offset] & nonPin, enemyOccup, pos.sideToPlay, otherMoves);
        knightCaptures(pos.pieces[1 + offset] & nonPin, enemyOccup, otherMoves);
        bishopCaptures(pos.pieces[2 + offset] & nonPin, enemyOccup, friendOccup, otherMoves);
        //rookMoves(pos.pieces[3 + offset] & nonPin, enemyOccup, friendOccup, otherMoves);
        queenCaptures(pos.pieces[4 + offset] & nonPin, enemyOccup, friendOccup, otherMoves);
        rookCaptures(pos.pieces[3 + offset] & nonPin, enemyOccup, friendOccup, otherMoves); // zmena poradi
        kingCaptures(pos.pieces[5+ offset], enemyOccup, attackedSqByEnemy, otherMoves);
    }
    return otherMoves.empty();
}

void MoveGenerator::visualizeMV(MV move) const
{
    std::string figType;
    switch (((static_cast<U8>(move >> 16)))%100) {
    case 0:
        figType = "Pawn";
        break;
    case 1:
        figType = "Knight";
        break;
    case 2:
        figType = "Bishop";
        break;
    case 3:
        figType = "Rook";
        break;
    case 4:
        figType = "Queen";
        break;
    case 5:
        figType = "King";
        break;
    default:
        break;
    }

    std::string flag;
    switch (static_cast<MV_Flags>(move >> 24)) {
    case MV_Flags::NO:
        flag = "Nothing";
        break;
    default:
        flag = "Something";
        break;
    }

    std::string capture;
    if ((static_cast<U8>(move >> 16)) > 99){
        capture = "Yes";
    } else {
        capture = "No";
    }
    std::cout << "From square: " << SquareToString(static_cast<Square>(static_cast<U8>(move)));
    std:: cout <<  " To square: " << SquareToString(static_cast<Square>(static_cast<U8>(move >> 8)));
    std::cout << " Piece: " << figType << " Capture: " << capture
              << " Flag: " << flag << std::endl;
}

bool MoveGenerator::isInDoubleCheck(const PositionBB &pos, BB& sqToBlockCheck, const int offset, SideToPlay side) const
{
    // offset ma byt 0 pro W a 6 pro B
    BB occupied = pos.whiteOccupies | pos.blackOccupies;

    sqToBlockCheck = 0;
    BB checkingPieces = 0;
    int num_of_checkers = 0;

    U8 sqWithKing = Bitboard::find_lsb(pos.pieces[5 + offset]);

    // Pawns
    if (side == WHITE){
       BB sqOnLeft = (pos.pieces[5 + offset] << 7) & ~fileH;
       BB sqOnRight = (pos.pieces[5 + offset] << 9) & ~fileA;
       checkingPieces |= (sqOnLeft | sqOnRight) & pos.pieces[6 - offset];
    } else {
        BB sqOnLeft = (pos.pieces[5 + offset] >> 9) & ~fileH;
        BB sqOnRight = (pos.pieces[5 + offset] >> 7) & ~fileA;
       checkingPieces |= (sqOnLeft | sqOnRight) & pos.pieces[6 - offset];
    }
    sqToBlockCheck |= checkingPieces;

    while (checkingPieces) {
        Bitboard::zero_lsb(checkingPieces);
        num_of_checkers++;
        if (num_of_checkers > 1){
            return true;
        }
    }

    // Knights
    checkingPieces |= pos.pieces[7 - offset] & KnightMasks[sqWithKing];
    sqToBlockCheck |= checkingPieces;

    while (checkingPieces) {
        Bitboard::zero_lsb(checkingPieces);
        num_of_checkers++;
        if (num_of_checkers > 1){
            return true;
        }
    }

    BB allBlockers = BishopMasks[sqWithKing] & occupied;
    int hashB = computeHash(allBlockers, bishopMagicNumbers[sqWithKing], BishopIndexBitCounts[sqWithKing]);

    checkingPieces |= bishopHashedAttacks[sqWithKing][hashB] & (pos.pieces[8 - offset] | pos.pieces[10 - offset]);
    sqToBlockCheck |= checkingPieces;

    while (checkingPieces) {
        U8 sqWithBish = Bitboard::zero_lsb(checkingPieces);
        num_of_checkers++;
        if (num_of_checkers > 1){
            return true;
        } else {
            if (sqWithKing > sqWithBish){
                sqToBlockCheck |= (bishopHashedAttacks[sqWithKing][hashB] & bishopHashedAttacks[sqWithBish][0])
                        & ((1ULL << sqWithKing) - 1) & ~((1ULL << sqWithBish) - 1);
            } else {
                sqToBlockCheck |= (bishopHashedAttacks[sqWithKing][hashB] & bishopHashedAttacks[sqWithBish][0])
                        & ((1ULL << sqWithBish) - 1) & ~((1ULL << sqWithKing) - 1);
            }
        }
    }

    allBlockers = RookMasks[sqWithKing] & occupied;
    int hashR = computeHash(allBlockers, rookMagicNumbers[sqWithKing], RookIndexBitCounts[sqWithKing]);

    checkingPieces |= (rookHashedAttacks[sqWithKing][hashR] & (pos.pieces[9 - offset] | pos.pieces[10 - offset]));
    sqToBlockCheck |= checkingPieces;

    while (checkingPieces) {
        U8 sqWithRook = Bitboard::zero_lsb(checkingPieces);
        num_of_checkers++;
        if (num_of_checkers > 1){
            return true;
        } else {
            if (sqWithKing > sqWithRook){
                sqToBlockCheck |= (rookHashedAttacks[sqWithKing][hashR] & rookHashedAttacks[sqWithRook][0])
                        & ((1ULL << sqWithKing) - 1) & ~((1ULL << sqWithRook) - 1);
            } else {
                sqToBlockCheck |= (rookHashedAttacks[sqWithKing][hashR] & rookHashedAttacks[sqWithRook][0])
                        & ((1ULL << sqWithRook) - 1) & ~((1ULL << sqWithKing) - 1);
            }
        }
    }

    return false;
}

void MoveGenerator::castling(const PositionBB &pos, std::vector<MV> &otherMoves) const
{
    if (pos.sideToPlay == WHITE) {
        if (pos.castlingWhiteKside){
            if ((castWK & (pos.whiteOccupies | pos.blackOccupies | pos.attackedSqByBlack)) == 0){
                otherMoves.push_back(encodeMV(static_cast<U8>(E1),static_cast<U8>(G1), 5, 0, MV_Flags::CASTLING_WK));
            }
        }
        if (pos.castlingWhiteQside){
            if ((castWQnotOccup & (pos.whiteOccupies | pos.blackOccupies)) == 0
                    && (castWQnotInCheck & pos.attackedSqByBlack) == 0){
                otherMoves.push_back(encodeMV(static_cast<U8>(E1),static_cast<U8>(C1), 5, 0, MV_Flags::CASTLING_WQ));
            }
        }
    } else {
        if (pos.castlingBlackKside){

            if ((castBK & (pos.whiteOccupies | pos.blackOccupies | pos.attackedSqByWhite)) == 0){
                otherMoves.push_back(encodeMV(static_cast<U8>(E8),static_cast<U8>(G8), 5, 0, MV_Flags::CASTLING_BK));
            }

        }
        if (pos.castlingBlackQside){
            if ((castBQnotOccup & (pos.whiteOccupies | pos.blackOccupies)) == 0
                    && (castBQnotInCheck & pos.attackedSqByWhite) == 0){
                otherMoves.push_back(encodeMV(static_cast<U8>(E8),static_cast<U8>(C8), 5, 0, MV_Flags::CASTLING_BQ));
            }
        }
    }
}

void MoveGenerator::updateCastlingRights(PositionBB &pos) const
{
    if (pos.castlingWhiteKside || pos.castlingWhiteQside){
        if ((pos.pieces[5] & sqE1) == 0) {
            pos.castlingWhiteKside = false;
            pos.castlingWhiteQside = false;
            return;
        }
        if ((pos.pieces[3] & sqH1) == 0) {
            pos.castlingWhiteKside = false;
        }
        if ((pos.pieces[3] & sqA1) == 0) {
            pos.castlingWhiteQside = false;
        }
    }
    if (pos.castlingBlackKside || pos.castlingBlackQside){
        if ((pos.pieces[11] & sqE8) == 0) {
            pos.castlingBlackKside = false;
            pos.castlingBlackQside = false;
            return;
        }
        if ((pos.pieces[9] & sqH8) == 0) {
            pos.castlingBlackKside = false;
        }
        if ((pos.pieces[9] & sqA8) == 0) {
            pos.castlingBlackQside = false;
        }
    }
}

void MoveGenerator::enPassant(BB pawnPos, BB targetSq, SideToPlay side, std::vector<MV> &otherMoves, const PositionBB& pos, BB sqToBlockCheck) const
{
    if (pos.enPassantPossible == false){
        return;
    }
    if (side == WHITE){
        if ((sqToBlockCheck & (targetSq >> 8)) == 0){
           return;
        }
        if ((pawnPos & ~fileA) << 7 & targetSq) {
            if (!enPasDiscoveredCheck(pos, targetSq >> 8, targetSq >> 7, rank5, pos.pieces[5], pos.pieces[9] | pos.pieces[10])){
                U8 capSq = Bitboard::find_lsb(targetSq);
                otherMoves.push_back(encodeMV(capSq - 7, capSq, 0, 1, MV_Flags::ENPASSANT_W));
            }
        }
        if ((pawnPos & ~fileH) << 9 & targetSq) {
            if (!enPasDiscoveredCheck(pos, targetSq >> 8, targetSq >> 9, rank5, pos.pieces[5], pos.pieces[9] | pos.pieces[10])){
                U8 capSq = Bitboard::find_lsb(targetSq);
                otherMoves.push_back(encodeMV(capSq - 9, capSq, 0, 1, MV_Flags::ENPASSANT_W));
            }
        }
    } else {
        if ((sqToBlockCheck & (targetSq << 8)) == 0){
           return;
        }
        if ((pawnPos & ~fileH) >> 7 & targetSq) {
            if (!enPasDiscoveredCheck(pos, targetSq << 8, targetSq << 7, rank4, pos.pieces[11], pos.pieces[3] | pos.pieces[4])){
                U8 capSq = Bitboard::find_lsb(targetSq);
                otherMoves.push_back(encodeMV(capSq + 7, capSq, 0, 1, MV_Flags::ENPASSANT_B));
            }
        }
        if ((pawnPos & ~fileA) >> 9 & targetSq) {
            if (!enPasDiscoveredCheck(pos, targetSq << 8, targetSq << 9, rank4, pos.pieces[11], pos.pieces[3] | pos.pieces[4])){
                U8 capSq = Bitboard::find_lsb(targetSq);
                otherMoves.push_back(encodeMV(capSq + 9, capSq, 0, 1, MV_Flags::ENPASSANT_B));
            }
        }
    }
}

bool MoveGenerator::enPasDiscoveredCheck(const PositionBB &pos, BB pawnThatIsTaken, BB movingPawn, BB rank, BB king, BB rooksOrQueens) const
{
    // Check for rare occassion of discovered check
    if ((king & rank)
            && (rooksOrQueens & rank)){
        U8 sqWithKing = Bitboard::find_lsb(king);
        BB allBlockers = RookMasks[sqWithKing] & (pos.whiteOccupies | pos.blackOccupies) & ~(pawnThatIsTaken | movingPawn);
        int hash = computeHash(allBlockers, rookMagicNumbers[sqWithKing], RookIndexBitCounts[sqWithKing]);
        BB allMoves = rookHashedAttacks[sqWithKing][hash] & rooksOrQueens & rank;
        if (allMoves){
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

BB MoveGenerator::identifyPinnedFigures(const PositionBB &pos, std::vector<MV>& otherMoves, bool isKingInCheck) const
{
    BB pinned = 0;
    BB occupied = pos.whiteOccupies | pos.blackOccupies;
    BB friendOccup;
    BB offset;
    if (pos.sideToPlay == WHITE){
        offset = 6;
        friendOccup = pos.whiteOccupies;
    } else {
        offset = 0;
        friendOccup = pos.blackOccupies;
    }

    // Not more than one king :D
    U8 sqWithKing = Bitboard::find_lsb(pos.pieces[11 - offset]);

    BB bishOnRays = bishopHashedAttacks[sqWithKing][0] & (pos.pieces[2 + offset] | pos.pieces[4 + offset]);
    BB rookOnRays = rookHashedAttacks[sqWithKing][0] & (pos.pieces[3 + offset] | pos.pieces[4 + offset]);

    while(bishOnRays){
        BB rayBetween;
        U8 sqWithBish = Bitboard::zero_lsb(bishOnRays);

        if (sqWithKing > sqWithBish){
            rayBetween = (bishopHashedAttacks[sqWithKing][0] & bishopHashedAttacks[sqWithBish][0])
                    & ((1ULL << sqWithKing) - 1) & ~((1ULL << sqWithBish) - 1);
        } else {
            rayBetween = (bishopHashedAttacks[sqWithKing][0] & bishopHashedAttacks[sqWithBish][0])
                    & ((1ULL << sqWithBish) - 1) & ~((1ULL << sqWithKing) - 1);
        }

        BB occupOnRay = rayBetween & occupied;
        if ((occupOnRay & (occupOnRay - 1)) == 0){
            if (occupOnRay & friendOccup){
                pinned |= occupOnRay;
                // pin, evaluate moves for given figure
                if (!isKingInCheck){
                    if (occupOnRay & pos.pieces[6 - offset]){
                        BB enPasInRay = rayBetween & (1ULL << static_cast<U8>(pos.enPassantTargetSquare));
                        if (enPasInRay){
                            pinnedPawnCapture(occupOnRay, enPasInRay, pos.sideToPlay, otherMoves, true);
                        } else {
                            pinnedPawnCapture(occupOnRay, 1ULL << sqWithBish, pos.sideToPlay, otherMoves, false);
                        }
                    }
                    if (occupOnRay & pos.pieces[8 - offset]){
                        pinnedSliderMoves(occupOnRay, 1ULL << sqWithBish, rayBetween, otherMoves, BISHOP);
                    }
                    if (occupOnRay & pos.pieces[10 - offset]){
                        pinnedSliderMoves(occupOnRay, 1ULL << sqWithBish, rayBetween, otherMoves, QUEEN);
                    }
                }
            } else {
                // possible discovered check
            }
        }
    }

    while(rookOnRays){
        BB rayBetween;
        U8 sqWithRook = Bitboard::zero_lsb(rookOnRays);

        if (sqWithKing > sqWithRook){
            rayBetween = (rookHashedAttacks[sqWithKing][0] & rookHashedAttacks[sqWithRook][0])
                    & ((1ULL << sqWithKing) - 1) & ~((1ULL << sqWithRook) - 1);
        } else {
            rayBetween = (rookHashedAttacks[sqWithKing][0] & rookHashedAttacks[sqWithRook][0])
                    & ((1ULL << sqWithRook) - 1) & ~((1ULL << sqWithKing) - 1);
        }

        BB occupOnRay = rayBetween & occupied;
        if ((occupOnRay & (occupOnRay - 1)) == 0){
            if (occupOnRay & friendOccup){
                pinned |= occupOnRay;
                // pin, evaluate moves for given figure
                if (!isKingInCheck){
                    if (occupOnRay & pos.pieces[6 - offset]){
                        pinnedPawnPush(occupOnRay, rayBetween, pos.sideToPlay, otherMoves);
                    }
                    if (occupOnRay & pos.pieces[9 - offset]){
                        pinnedSliderMoves(occupOnRay, 1ULL << sqWithRook, rayBetween, otherMoves, ROOK);
                    }
                    if (occupOnRay & pos.pieces[10 - offset]){
                        pinnedSliderMoves(occupOnRay, 1ULL << sqWithRook, rayBetween, otherMoves, QUEEN);
                    }
                }
            } else {
                // possible discovered check
            }
        }
    }

    return pinned;
}

BB MoveGenerator::identifyPinnedFigures_forCapture(const PositionBB &pos, std::vector<MV> &otherMoves, bool isKingInCheck) const
{
    BB pinned = 0;
    BB occupied = pos.whiteOccupies | pos.blackOccupies;
    BB friendOccup;
    BB offset;
    if (pos.sideToPlay == WHITE){
        offset = 6;
        friendOccup = pos.whiteOccupies;
    } else {
        offset = 0;
        friendOccup = pos.blackOccupies;
    }

    // Not more than one king :D
    U8 sqWithKing = Bitboard::find_lsb(pos.pieces[11 - offset]);

    BB bishOnRays = bishopHashedAttacks[sqWithKing][0] & (pos.pieces[2 + offset] | pos.pieces[4 + offset]);
    BB rookOnRays = rookHashedAttacks[sqWithKing][0] & (pos.pieces[3 + offset] | pos.pieces[4 + offset]);

    while(bishOnRays){
        BB rayBetween;
        U8 sqWithBish = Bitboard::zero_lsb(bishOnRays);

        if (sqWithKing > sqWithBish){
            rayBetween = (bishopHashedAttacks[sqWithKing][0] & bishopHashedAttacks[sqWithBish][0])
                    & ((1ULL << sqWithKing) - 1) & ~((1ULL << sqWithBish) - 1);
        } else {
            rayBetween = (bishopHashedAttacks[sqWithKing][0] & bishopHashedAttacks[sqWithBish][0])
                    & ((1ULL << sqWithBish) - 1) & ~((1ULL << sqWithKing) - 1);
        }

        BB occupOnRay = rayBetween & occupied;
        if ((occupOnRay & (occupOnRay - 1)) == 0){
            if (occupOnRay & friendOccup){
                pinned |= occupOnRay;
                // pin, evaluate moves for given figure
                if (!isKingInCheck){
                    if (occupOnRay & pos.pieces[6 - offset]){
                        BB enPasInRay = rayBetween & (1ULL << static_cast<U8>(pos.enPassantTargetSquare));
                        if (enPasInRay){
                            pinnedPawnCapture(occupOnRay, enPasInRay, pos.sideToPlay, otherMoves, true);
                        } else {
                            pinnedPawnCapture(occupOnRay, 1ULL << sqWithBish, pos.sideToPlay, otherMoves, false);
                        }
                    }
                    if (occupOnRay & pos.pieces[8 - offset]){
                        pinnedSliderCaptures(occupOnRay, 1ULL << sqWithBish, otherMoves, BISHOP);
                    }
                    if (occupOnRay & pos.pieces[10 - offset]){
                        pinnedSliderCaptures(occupOnRay, 1ULL << sqWithBish, otherMoves, QUEEN);
                    }
                }
            } else {
                // possible discovered check
            }
        }
    }

    while(rookOnRays){
        BB rayBetween;
        U8 sqWithRook = Bitboard::zero_lsb(rookOnRays);

        if (sqWithKing > sqWithRook){
            rayBetween = (rookHashedAttacks[sqWithKing][0] & rookHashedAttacks[sqWithRook][0])
                    & ((1ULL << sqWithKing) - 1) & ~((1ULL << sqWithRook) - 1);
        } else {
            rayBetween = (rookHashedAttacks[sqWithKing][0] & rookHashedAttacks[sqWithRook][0])
                    & ((1ULL << sqWithRook) - 1) & ~((1ULL << sqWithKing) - 1);
        }

        BB occupOnRay = rayBetween & occupied;
        if ((occupOnRay & (occupOnRay - 1)) == 0){
            if (occupOnRay & friendOccup){
                pinned |= occupOnRay;
                // pin, evaluate moves for given figure
                if (!isKingInCheck){
                    if (occupOnRay & pos.pieces[9 - offset]){
                        pinnedSliderCaptures(occupOnRay, 1ULL << sqWithRook, otherMoves, ROOK);
                    }
                    if (occupOnRay & pos.pieces[10 - offset]){
                        pinnedSliderCaptures(occupOnRay, 1ULL << sqWithRook, otherMoves, QUEEN);
                    }
                }
            } else {
                // possible discovered check
            }
        }
    }

    return pinned;
}

BB MoveGenerator::attackedSqBy(SideToPlay side, const PositionBB &pos) const {
    BB attackedSqBy = 0;
    BB invisibleKing = 0;
    int offset;

    if (side == WHITE){
        invisibleKing = pos.pieces[11];
        offset = 0;

        // Squares endangered by pawns
        //Left capture
        attackedSqBy |= (pos.pieces[0] & ~Bitboard::fileA) << 7;
        //Right capture
        attackedSqBy |= (pos.pieces[0] & ~Bitboard::fileH) << 9;

    } else if (side == BLACK) {
        invisibleKing = pos.pieces[5];
        offset = 6;

        // Squares endangered by pawns
        //Left capture (from point of view of white)
        attackedSqBy |= (pos.pieces[6] & ~Bitboard::fileA) >> 9;
        //Right capture (from point of view of white)
        attackedSqBy |= (pos.pieces[6] & ~Bitboard::fileH) >> 7;
    }

    BB knights = pos.pieces[1 + offset];
    BB bishopsAndQueens = pos.pieces[2 + offset] | pos.pieces[4 + offset];
    BB rooksAndQueens = pos.pieces[3 +offset] | pos.pieces[4 + offset];
    BB king = pos.pieces[5+offset];

    // This is because of checking slider, king cannot go "backwards", it will be still in check
    BB relevantOcc = (pos.whiteOccupies | pos.blackOccupies) & ~invisibleKing;

    while (knights) {
        U8 squareWithKnight = Bitboard::zero_lsb(knights);
        attackedSqBy |= KnightMasks[squareWithKnight];
    }

    while (bishopsAndQueens) {
        U8 sqWithBishop = Bitboard::zero_lsb(bishopsAndQueens);
        BB allBlockers = BishopMasks[sqWithBishop] & relevantOcc;
        int hash = computeHash(allBlockers, bishopMagicNumbers[sqWithBishop], BishopIndexBitCounts[sqWithBishop]);
        attackedSqBy |= bishopHashedAttacks[sqWithBishop][hash];
    }

    while (rooksAndQueens) {
        U8 sqWithRook = Bitboard::zero_lsb(rooksAndQueens);
        BB allBlockers = RookMasks[sqWithRook] & relevantOcc;
        int hash = computeHash(allBlockers, rookMagicNumbers[sqWithRook], RookIndexBitCounts[sqWithRook]);
        attackedSqBy |= rookHashedAttacks[sqWithRook][hash];
    }

    while (king){
        U8 squareWithKing = Bitboard::zero_lsb(king);
        attackedSqBy |= KingMasks[squareWithKing];
    }
    return attackedSqBy;
}

void MoveGenerator::pinnedPawnPush(BB pawnPos, BB rayBetween, SideToPlay side, std::vector<MV> &otherMoves) const
{
    if (side == WHITE){
        if  (pawnPos << 8 & rayBetween) {
            U8 fromSq = Bitboard::find_lsb(pawnPos);
            otherMoves.push_back(encodeMV(fromSq,fromSq+8,0,0, MV_Flags::NO));
            //otherMoves.push_back(((fromSq + 8) << 8) + fromSq);
            if ((pawnPos & rank2) && (pawnPos << 16 & rayBetween)) {
                otherMoves.push_back(encodeMV(fromSq,fromSq+16,0,0, MV_Flags::NO));
                //otherMoves.push_back(((fromSq + 16) << 8) + fromSq);
            }
        }
    } else {
        if  (pawnPos >> 8 & rayBetween) {
            U8 fromSq = Bitboard::find_lsb(pawnPos);
            otherMoves.push_back(encodeMV(fromSq,fromSq-8,0,0, MV_Flags::NO));
            //otherMoves.push_back(((fromSq - 8) << 8) + fromSq);
            if ((pawnPos & rank7) && (pawnPos >> 16 & rayBetween)) {
                otherMoves.push_back(encodeMV(fromSq,fromSq-16,0,0, MV_Flags::NO));
                //otherMoves.push_back(((fromSq - 16) << 8) + fromSq);
            }
        }
    }
    // Pinned pawn cannot push to promotion
}

void MoveGenerator::pinnedPawnCapture(BB pawnPos, BB sqToCap, SideToPlay side, std::vector<MV> &otherMoves, bool enPass) const
{
    if (side == WHITE){
        if ( ((pawnPos & ~fileA) << 7 == sqToCap) || ((pawnPos & ~fileH) << 9 == sqToCap) ) {
            U8 capSq = Bitboard::zero_lsb(sqToCap);
            U8 fromSq = Bitboard::zero_lsb(pawnPos);
            if (capSq < 56){
                if (enPass){
                    otherMoves.push_back(encodeMV(fromSq,capSq,0,1, MV_Flags::ENPASSANT_W));
                } else {
                    otherMoves.push_back(encodeMV(fromSq,capSq,0,1, MV_Flags::NO));
                }
            } else {
                otherMoves.push_back(encodeMV(fromSq,capSq,0,1, MV_Flags::PROMOTION_N));
                otherMoves.push_back(encodeMV(fromSq,capSq,0,1, MV_Flags::PROMOTION_B));
                otherMoves.push_back(encodeMV(fromSq,capSq,0,1, MV_Flags::PROMOTION_R));
                otherMoves.push_back(encodeMV(fromSq,capSq,0,1, MV_Flags::PROMOTION_Q));
            }
        }
    } else {
        if ( ((pawnPos & ~fileH) >> 7 == sqToCap) || ((pawnPos & ~fileA) >> 9 == sqToCap) ) {
            U8 capSq = Bitboard::zero_lsb(sqToCap);
            U8 fromSq = Bitboard::zero_lsb(pawnPos);
            if (capSq > 7){
                if (enPass){
                    otherMoves.push_back(encodeMV(fromSq,capSq,0,1, MV_Flags::ENPASSANT_B));
                } else {
                    otherMoves.push_back(encodeMV(fromSq,capSq,0,1, MV_Flags::NO));
                }
            } else {
                otherMoves.push_back(encodeMV(fromSq,capSq,0,1, MV_Flags::PROMOTION_N));
                otherMoves.push_back(encodeMV(fromSq,capSq,0,1, MV_Flags::PROMOTION_B));
                otherMoves.push_back(encodeMV(fromSq,capSq,0,1, MV_Flags::PROMOTION_R));
                otherMoves.push_back(encodeMV(fromSq,capSq,0,1, MV_Flags::PROMOTION_Q));
            }
            //otherMoves.push_back((capSq << 8) + fromSq);
        }
    }
}

void MoveGenerator::pinnedSliderMoves(BB sliderPos, BB pinningPiece, BB rayBetween, std::vector<MV> &otherMoves, FigureType figType) const
{
    U8 sqWithSlider = Bitboard::find_lsb(sliderPos);
    BB rayWithoutSlider = rayBetween & ~sliderPos; 
    // Pushes
    while (rayWithoutSlider) {
        U8 pushSq = Bitboard::zero_lsb(rayWithoutSlider);
        otherMoves.push_back(encodeMV(sqWithSlider,pushSq,static_cast<U8>(figType),0, MV_Flags::NO));
        //otherMoves.push_back((pushSq << 8) + sqWithSlider);
    }
    // Only one capture
    U8 capSq = Bitboard::zero_lsb(pinningPiece);
    otherMoves.push_back(encodeMV(sqWithSlider,capSq,static_cast<U8>(figType),1, MV_Flags::NO));
    //otherMoves.push_back((capSq << 8) + sqWithSlider);

}

void MoveGenerator::pinnedSliderCaptures(BB sliderPos, BB pinningPiece, std::vector<MV> &otherMoves, FigureType figType) const
{
    U8 sqWithSlider = Bitboard::find_lsb(sliderPos);
    U8 capSq = Bitboard::zero_lsb(pinningPiece);
    otherMoves.push_back(encodeMV(sqWithSlider,capSq,static_cast<U8>(figType),1, MV_Flags::NO));
    //otherMoves.push_back((capSq << 8) + sqWithSlider);
}

MV MoveGenerator::encodeMV(U8 fromSq, U8 toSquare, U8 piece, bool isCapture, MV_Flags flags) const
{
    return fromSq + (toSquare << 8) + ((piece + isCapture*100) << 16) + (static_cast<U8>(flags) << 24);
}

MV MoveGenerator::encodeMV(Square fromSq, Square toSq, FigureType piece, bool isCapture, MV_Flags flags) const
{
    U8 pieceU8;
    switch (piece) {
    case PAWN:
        pieceU8 = 0;
        break;
    case KNIGHT:
        pieceU8 = 1;
        break;
    case BISHOP:
        pieceU8 = 2;
        break;
    case ROOK:
        pieceU8 = 3;
        break;
    case QUEEN:
        pieceU8 = 4;
        break;
    case KING:
        pieceU8 = 5;
        break;
    default:
        break;
    }
    return encodeMV(static_cast<U8>(fromSq), static_cast<U8>(toSq), pieceU8, isCapture, flags);
}


void MoveGenerator::pawnPushes(BB pawns, SideToPlay side, BB allPiecesTogether, std::vector<MV> &otherMoves, BB sqToBlockCheck) const
{
    if (side == WHITE){
        BB pushes = (pawns << 8) & ~allPiecesTogether;
        BB doublePushes = ((pushes & rank3) << 8) & ~allPiecesTogether;

        pushes &= sqToBlockCheck;
        doublePushes &= sqToBlockCheck;

        BB promotionPushes = pushes & rank8;
        pushes &= ~rank8;

        while (pushes){
            U8 pushSq = Bitboard::zero_lsb(pushes);
            otherMoves.push_back(encodeMV(pushSq - 8, pushSq, 0, 0, MV_Flags::NO));
        }
        while (doublePushes){
            U8 pushSq = Bitboard::zero_lsb(doublePushes);
            otherMoves.push_back(encodeMV(pushSq - 16, pushSq, 0, 0, MV_Flags::NO));
        }
        while (promotionPushes){
            U8 pushSq = Bitboard::zero_lsb(promotionPushes);
            otherMoves.push_back(encodeMV(pushSq - 8,pushSq,0,0, MV_Flags::PROMOTION_N));
            otherMoves.push_back(encodeMV(pushSq - 8,pushSq,0,0, MV_Flags::PROMOTION_B));
            otherMoves.push_back(encodeMV(pushSq - 8,pushSq,0,0, MV_Flags::PROMOTION_R));
            otherMoves.push_back(encodeMV(pushSq - 8,pushSq,0,0, MV_Flags::PROMOTION_Q));
        }

    } else {
        BB pushes = (pawns >> 8) & ~allPiecesTogether;
        BB doublePushes = ((pushes & rank6) >> 8) & ~allPiecesTogether;

        pushes &= sqToBlockCheck;
        doublePushes &= sqToBlockCheck;

        BB promotionPushes = pushes & rank1;
        pushes &= ~rank1;

        while (pushes){
            U8 pushSq = Bitboard::zero_lsb(pushes);
            otherMoves.push_back(encodeMV(pushSq + 8, pushSq, 0, 0, MV_Flags::NO));
        }
        while (doublePushes){
            U8 pushSq = Bitboard::zero_lsb(doublePushes);
            otherMoves.push_back(encodeMV(pushSq + 16, pushSq, 0, 0, MV_Flags::NO));
        }
        while (promotionPushes){
            U8 pushSq = Bitboard::zero_lsb(promotionPushes);
            otherMoves.push_back(encodeMV(pushSq + 8,pushSq,0,0, MV_Flags::PROMOTION_N));
            otherMoves.push_back(encodeMV(pushSq + 8,pushSq,0,0, MV_Flags::PROMOTION_B));
            otherMoves.push_back(encodeMV(pushSq + 8,pushSq,0,0, MV_Flags::PROMOTION_R));
            otherMoves.push_back(encodeMV(pushSq + 8,pushSq,0,0, MV_Flags::PROMOTION_Q));
        }
    }
}

void MoveGenerator::pawnCaptures(BB pawns, BB enemyOccupies, SideToPlay side, std::vector<MV> &otherMoves, BB sqToBlockCheck) const
{
    BB captures;
    BB capturableEnemy = enemyOccupies & sqToBlockCheck;
    if (side == WHITE){
        //Left capture
        captures = ((pawns & ~fileA) << 7) & capturableEnemy;

        BB promotionCap = captures & rank8;
        captures &= ~rank8;
        while (captures){
            U8 captureSq = Bitboard::zero_lsb(captures);
            otherMoves.push_back(encodeMV(captureSq - 7, captureSq, 0, 1, MV_Flags::NO));
        }
        while (promotionCap){
            U8 captureSq = Bitboard::zero_lsb(promotionCap);
            otherMoves.push_back(encodeMV(captureSq - 7,captureSq,0,1, MV_Flags::PROMOTION_N));
            otherMoves.push_back(encodeMV(captureSq - 7,captureSq,0,1, MV_Flags::PROMOTION_B));
            otherMoves.push_back(encodeMV(captureSq - 7,captureSq,0,1, MV_Flags::PROMOTION_R));
            otherMoves.push_back(encodeMV(captureSq - 7,captureSq,0,1, MV_Flags::PROMOTION_Q));
        }
        //Right capture
        captures |= ((pawns & ~fileH) << 9) & capturableEnemy;

        promotionCap = captures & rank8;
        captures &= ~rank8;
        while (captures){
            U8 captureSq = Bitboard::zero_lsb(captures);
            otherMoves.push_back(encodeMV(captureSq - 9, captureSq, 0, 1, MV_Flags::NO));
        }
        while (promotionCap){
            U8 captureSq = Bitboard::zero_lsb(promotionCap);
            otherMoves.push_back(encodeMV(captureSq - 9,captureSq,0,1, MV_Flags::PROMOTION_N));
            otherMoves.push_back(encodeMV(captureSq - 9,captureSq,0,1, MV_Flags::PROMOTION_B));
            otherMoves.push_back(encodeMV(captureSq - 9,captureSq,0,1, MV_Flags::PROMOTION_R));
            otherMoves.push_back(encodeMV(captureSq - 9,captureSq,0,1, MV_Flags::PROMOTION_Q));
        }
    } else if (side == BLACK) {
        //Left capture (from point of view of white)
        captures = ((pawns & ~fileA) >> 9) & capturableEnemy;

        BB promotionCap = captures & rank1;
        captures &= ~rank1;
        while (captures){
            U8 captureSq = Bitboard::zero_lsb(captures);
            otherMoves.push_back(encodeMV(captureSq + 9, captureSq, 0, 1, MV_Flags::NO));
        }
        while (promotionCap){
            U8 captureSq = Bitboard::zero_lsb(promotionCap);
            otherMoves.push_back(encodeMV(captureSq + 9,captureSq,0,1, MV_Flags::PROMOTION_N));
            otherMoves.push_back(encodeMV(captureSq + 9,captureSq,0,1, MV_Flags::PROMOTION_B));
            otherMoves.push_back(encodeMV(captureSq + 9,captureSq,0,1, MV_Flags::PROMOTION_R));
            otherMoves.push_back(encodeMV(captureSq + 9,captureSq,0,1, MV_Flags::PROMOTION_Q));
        }

        //Right capture (from point of view of white)
        captures |= ((pawns & ~fileH) >> 7) & capturableEnemy;

        promotionCap = captures & rank1;
        captures &= ~rank1;
        while (captures){
            U8 captureSq = Bitboard::zero_lsb(captures);
            otherMoves.push_back(encodeMV(captureSq + 7, captureSq, 0, 1, MV_Flags::NO));
        }
        while (promotionCap){
            U8 captureSq = Bitboard::zero_lsb(promotionCap);
            otherMoves.push_back(encodeMV(captureSq + 7,captureSq,0,1, MV_Flags::PROMOTION_N));
            otherMoves.push_back(encodeMV(captureSq + 7,captureSq,0,1, MV_Flags::PROMOTION_B));
            otherMoves.push_back(encodeMV(captureSq + 7,captureSq,0,1, MV_Flags::PROMOTION_R));
            otherMoves.push_back(encodeMV(captureSq + 7,captureSq,0,1, MV_Flags::PROMOTION_Q));
        }
    }
}

void MoveGenerator::knightMoves(BB knights, BB enemyOccupies, BB friendOccupies, std::vector<MV> &otherMoves, BB sqToBlockCheck) const
{
    while (knights) {
        U8 sqWithKnight = Bitboard::zero_lsb(knights);
        BB allMoves = KnightMasks[sqWithKnight] & (~friendOccupies) & sqToBlockCheck;
        BB pushes = allMoves & (~enemyOccupies);
        BB captures = allMoves & enemyOccupies;
        while (pushes) {
            U8 pushSq = Bitboard::zero_lsb(pushes);
            otherMoves.push_back(encodeMV(sqWithKnight, pushSq, 1, 0, MV_Flags::NO));
            //otherMoves.push_back((pushSq << 8) + sqWithKnight);
        }
        while (captures) {
            U8 capSq = Bitboard::zero_lsb(captures);
            otherMoves.push_back(encodeMV(sqWithKnight, capSq, 1, 1, MV_Flags::NO));
            //otherMoves.push_back((capSq << 8) + sqWithKnight);
        }
    }
}

void MoveGenerator::knightCaptures(BB knights, BB enemyOccupies, std::vector<MV> &otherMoves, BB sqToBlockCheck) const
{
    while (knights) {
        U8 sqWithKnight = Bitboard::zero_lsb(knights);
        BB captures = KnightMasks[sqWithKnight] & enemyOccupies & sqToBlockCheck;
        while (captures) {
            U8 capSq = Bitboard::zero_lsb(captures);
            otherMoves.push_back(encodeMV(sqWithKnight, capSq, 1, 1, MV_Flags::NO));
        }
    }
}

void MoveGenerator::bishopMoves(BB bishops, BB enemyOccupies, BB friendOccupies, std::vector<MV> &otherMoves, BB sqToBlockCheck) const
{
    while (bishops) {
        U8 sqWithBishop = Bitboard::zero_lsb(bishops);
        BB allBlockers = BishopMasks[sqWithBishop] & (enemyOccupies | friendOccupies);
        int hash = computeHash(allBlockers, bishopMagicNumbers[sqWithBishop], BishopIndexBitCounts[sqWithBishop]);
        BB allMoves = bishopHashedAttacks[sqWithBishop][hash] & (~friendOccupies) & sqToBlockCheck;
        BB pushes = allMoves & (~enemyOccupies);
        BB captures = allMoves & enemyOccupies;
        while (pushes) {
            U8 pushSq = Bitboard::zero_lsb(pushes);
            otherMoves.push_back(encodeMV(sqWithBishop, pushSq, 2, 0, MV_Flags::NO));
           // otherMoves.push_back((pushSq << 8) + sqWithBishop);
        }
        while (captures) {
            U8 capSq = Bitboard::zero_lsb(captures);
            otherMoves.push_back(encodeMV(sqWithBishop, capSq, 2, 1, MV_Flags::NO));
            //otherMoves.push_back((capSq << 8) + sqWithBishop);
        }
    }
}

void MoveGenerator::bishopCaptures(BB bishops, BB enemyOccupies, BB friendOccupies, std::vector<MV> &otherMoves, BB sqToBlockCheck) const
{
    while (bishops) {
        U8 sqWithBishop = Bitboard::zero_lsb(bishops);
        BB allBlockers = BishopMasks[sqWithBishop] & (enemyOccupies | friendOccupies);
        int hash = computeHash(allBlockers, bishopMagicNumbers[sqWithBishop], BishopIndexBitCounts[sqWithBishop]);
        BB captures = bishopHashedAttacks[sqWithBishop][hash] & enemyOccupies & sqToBlockCheck;
        while (captures) {
            U8 capSq = Bitboard::zero_lsb(captures);
            otherMoves.push_back(encodeMV(sqWithBishop, capSq, 2, 1, MV_Flags::NO));
            //otherMoves.push_back((capSq << 8) + sqWithBishop);
        }
    }
}

void MoveGenerator::rookMoves(BB rooks, BB enemyOccupies, BB friendOccupies, std::vector<MV> &otherMoves, BB sqToBlockCheck) const
{
    while (rooks) {
        U8 sqWithRook = Bitboard::zero_lsb(rooks);
        BB allBlockers = RookMasks[sqWithRook] & (enemyOccupies | friendOccupies);
        int hash = computeHash(allBlockers, rookMagicNumbers[sqWithRook], RookIndexBitCounts[sqWithRook]);
        BB allMoves = rookHashedAttacks[sqWithRook][hash] & (~friendOccupies) & sqToBlockCheck;
        BB pushes = allMoves & (~enemyOccupies);
        BB captures = allMoves & enemyOccupies;
        while (pushes) {
            U8 pushSq = Bitboard::zero_lsb(pushes);
            otherMoves.push_back(encodeMV(sqWithRook, pushSq, 3, 0, MV_Flags::NO));
            //otherMoves.push_back((pushSq << 8) + sqWithRook);
        }
        while (captures) {
            U8 capSq = Bitboard::zero_lsb(captures);
            otherMoves.push_back(encodeMV(sqWithRook, capSq, 3, 1, MV_Flags::NO));
            //otherMoves.push_back((capSq << 8) + sqWithRook);
        }
    }
}

void MoveGenerator::rookCaptures(BB rooks, BB enemyOccupies, BB friendOccupies, std::vector<MV> &otherMoves, BB sqToBlockCheck) const
{
    while (rooks) {
        U8 sqWithRook = Bitboard::zero_lsb(rooks);
        BB allBlockers = RookMasks[sqWithRook] & (enemyOccupies | friendOccupies);
        int hash = computeHash(allBlockers, rookMagicNumbers[sqWithRook], RookIndexBitCounts[sqWithRook]);
        BB captures = rookHashedAttacks[sqWithRook][hash] & enemyOccupies & sqToBlockCheck;
        while (captures) {
            U8 capSq = Bitboard::zero_lsb(captures);
            otherMoves.push_back(encodeMV(sqWithRook, capSq, 3, 1, MV_Flags::NO));
        }
    }
}

void MoveGenerator::queenMoves(BB queens, BB enemyOccupies, BB friendOccupies, std::vector<MV> &otherMoves, BB sqToBlockCheck) const
{
    while (queens) {
        U8 sqWithQueen = Bitboard::zero_lsb(queens);
        BB occupied = enemyOccupies | friendOccupies;

        BB allBlockers = BishopMasks[sqWithQueen] & occupied;
        int hashB = computeHash(allBlockers, bishopMagicNumbers[sqWithQueen], BishopIndexBitCounts[sqWithQueen]);

        allBlockers = RookMasks[sqWithQueen] & occupied;
        int hashR = computeHash(allBlockers, rookMagicNumbers[sqWithQueen], RookIndexBitCounts[sqWithQueen]);

        BB allMoves = (bishopHashedAttacks[sqWithQueen][hashB] | rookHashedAttacks[sqWithQueen][hashR]) & (~friendOccupies) & sqToBlockCheck;
        BB pushes = allMoves & (~enemyOccupies);
        BB captures = allMoves & enemyOccupies;
        while (pushes) {
            U8 pushSq = Bitboard::zero_lsb(pushes);
            otherMoves.push_back(encodeMV(sqWithQueen, pushSq, 4, 0, MV_Flags::NO));
            //otherMoves.push_back((pushSq << 8) + sqWithQueen);
        }
        while (captures) {
            U8 capSq = Bitboard::zero_lsb(captures);
            otherMoves.push_back(encodeMV(sqWithQueen, capSq, 4, 1, MV_Flags::NO));
            //otherMoves.push_back((capSq << 8) + sqWithQueen);
        }
    }
}

void MoveGenerator::queenCaptures(BB queens, BB enemyOccupies, BB friendOccupies, std::vector<MV> &otherMoves, BB sqToBlockCheck) const
{
    while (queens) {
        U8 sqWithQueen = Bitboard::zero_lsb(queens);
        BB occupied = enemyOccupies | friendOccupies;

        BB allBlockers = BishopMasks[sqWithQueen] & occupied;
        int hashB = computeHash(allBlockers, bishopMagicNumbers[sqWithQueen], BishopIndexBitCounts[sqWithQueen]);

        allBlockers = RookMasks[sqWithQueen] & occupied;
        int hashR = computeHash(allBlockers, rookMagicNumbers[sqWithQueen], RookIndexBitCounts[sqWithQueen]);

        BB captures = (bishopHashedAttacks[sqWithQueen][hashB] | rookHashedAttacks[sqWithQueen][hashR]) & enemyOccupies & sqToBlockCheck;
        while (captures) {
            U8 capSq = Bitboard::zero_lsb(captures);
            otherMoves.push_back(encodeMV(sqWithQueen, capSq, 4, 1, MV_Flags::NO));
        }
    }
}

void MoveGenerator::kingMoves(BB king, BB enemyOccupies, BB friendOccupies, BB dangerSq, std::vector<MV> &otherMoves) const
{
    while (king) {
        U8 sqWithKing = Bitboard::zero_lsb(king);
        BB allMoves = KingMasks[sqWithKing] & (~friendOccupies) & (~dangerSq);

        BB pushes = allMoves & (~enemyOccupies);
        BB captures = allMoves & enemyOccupies;
        while (pushes) {
            U8 pushSq = Bitboard::zero_lsb(pushes);
            otherMoves.push_back(encodeMV(sqWithKing, pushSq, 5, 0, MV_Flags::NO));
           // otherMoves.push_back((pushSq << 8) + sqWithKing);
        }
        while (captures) {
            U8 capSq = Bitboard::zero_lsb(captures);
            otherMoves.push_back(encodeMV(sqWithKing, capSq, 5, 1, MV_Flags::NO));
           // otherMoves.push_back((capSq << 8) + sqWithKing);
        }
    }
}

void MoveGenerator::kingCaptures(BB king, BB enemyOccupies, BB dangerSq, std::vector<MV> &otherMoves) const
{
    while (king) {
        U8 sqWithKing = Bitboard::zero_lsb(king);
        BB captures = KingMasks[sqWithKing] & (enemyOccupies) & (~dangerSq);

        while (captures) {
            U8 capSq = Bitboard::zero_lsb(captures);
            otherMoves.push_back(encodeMV(sqWithKing, capSq, 5, 1, MV_Flags::NO));
        }
    }
}

// void MoveGenerator::updatePosOrdinaryMV(PositionBB pos, MV move) const
//{
//    ++pos.fullMoveClock;
//    ++pos.halfMoveClock;

//    BB fromSq = (1ULL << static_cast<U8>(move));
//    BB toSq = (1ULL << static_cast<U8>(move >> 8));
//    U8 info = move >> 16;
//    U8 piece = info%100;

//    int offset;
//    BB* attackSqToBeUpdated = 0;
//    // Which side played this MV?
//    if (pos.sideToPlay == WHITE){
//        pos.whiteOccupies &= ~fromSq;
//        pos.whiteOccupies |= toSq;

//        offset = 0;
//        attackSqToBeUpdated = &pos.attackedSqByWhite; // we need them for calculating black king moves
//    } else {
//        pos.blackOccupies &= ~fromSq;
//        pos.blackOccupies |= toSq;

//        offset = 6;
//        attackSqToBeUpdated = &pos.attackedSqByBlack; // we need them for calculating white king moves
//    }

//    pos.pieces[piece + offset] &= ~fromSq;
//    pos.pieces[piece + offset] |= toSq;


//    if (piece == 0){
//        pos.halfMoveClock = 0;
//        int dif = toSq - fromSq;
//        if ((dif == 16) || (dif == -16)){
//            pos.enPassantPossible = true;
//            pos.enPassantTargetSquare = static_cast<Square>(fromSq + dif/2);
//        }
//    } else {
//        pos.enPassantPossible = false;
//    }

//    if (info >= 100){
//        for (int i = 6; i < 10; ++i) {
//            pos.pieces[i - offset] &= ~toSq;
//        }
//        pos.halfMoveClock = 0;
//    }

//    *attackSqToBeUpdated = attackedSqBy(pos.sideToPlay, pos);
//    pos.sideToPlay = static_cast<SideToPlay>(~pos.sideToPlay);

//}

void MoveGenerator::updatePosPartial(PositionBB& pos, MV move) const
{

    // Update basic info
    ++pos.fullMoveClock;
    ++pos.halfMoveClock;
    pos.enPassantPossible = false;
    pos.enPassantTargetSquare = A1;


    BB fromSqBB = (1ULL << static_cast<U8>(move));
    BB toSqBB = (1ULL << static_cast<U8>(move >> 8));
    U8 info = move >> 16;
    U8 piece = info%100;
    U8 flag = move >> 24;

    int offset;
    BB* attackSqToBeUpdated = 0;

    // Which side played this MV?
    if (pos.sideToPlay == WHITE){
        pos.whiteOccupies &= ~fromSqBB;
        pos.whiteOccupies |= toSqBB;
        pos.blackOccupies &= ~toSqBB;

        offset = 0;
        attackSqToBeUpdated = &pos.attackedSqByWhite; // we need them for calculating black king moves
    } else {
        pos.blackOccupies &= ~fromSqBB;
        pos.blackOccupies |= toSqBB;
        pos.whiteOccupies &= ~toSqBB;

        offset = 6;
        attackSqToBeUpdated = &pos.attackedSqByBlack; // we need them for calculating white king moves
    }

    // If the MV is capture, update enemy occupancy BBs
    if (info >= 100){
        for (int i = 6; i < 11; ++i) {
            pos.pieces[i - offset] &= ~toSqBB;
        }
        pos.halfMoveClock = 0;
    }

    // Update friend occupancy BBs
    switch (static_cast<MV_Flags>(flag)) {
    case MV_Flags::NO:
        pos.pieces[piece + offset] &= ~fromSqBB;
        pos.pieces[piece + offset] |= toSqBB;
        break;
    case MV_Flags::PROMOTION_N:
        pos.pieces[0 + offset] &= ~fromSqBB;
        pos.pieces[1 + offset] |= toSqBB;
        break;
    case MV_Flags::PROMOTION_B:
        pos.pieces[0 + offset] &= ~fromSqBB;
        pos.pieces[2 + offset] |= toSqBB;
        break;
    case MV_Flags::PROMOTION_R:
        pos.pieces[0 + offset] &= ~fromSqBB;
        pos.pieces[3 + offset] |= toSqBB;
        break;
    case MV_Flags::PROMOTION_Q:
        pos.pieces[0 + offset] &= ~fromSqBB;
        pos.pieces[4 + offset] |= toSqBB;
        break;
    case MV_Flags::CASTLING_WK:
        pos.pieces[5] &= ~fromSqBB;
        pos.pieces[5] |= toSqBB;

        pos.pieces[3] &= ~sqH1;
        pos.pieces[3] |= sqF1;
        pos.whiteOccupies &= ~sqH1;
        pos.whiteOccupies |= sqF1;
        break;
    case MV_Flags::CASTLING_WQ:
        pos.pieces[5] &= ~fromSqBB;
        pos.pieces[5] |= toSqBB;

        pos.pieces[3] &= ~sqA1;
        pos.pieces[3] |= sqD1;
        pos.whiteOccupies &= ~sqA1;
        pos.whiteOccupies |= sqD1;
        break;
    case MV_Flags::CASTLING_BK:
        pos.pieces[11] &= ~fromSqBB;
        pos.pieces[11] |= toSqBB;

        pos.pieces[9] &= ~sqH8;
        pos.pieces[9] |= sqF8;
        pos.blackOccupies &= ~sqH8;
        pos.blackOccupies |= sqF8;
        break;
    case MV_Flags::CASTLING_BQ:
        pos.pieces[11] &= ~fromSqBB;
        pos.pieces[11] |= toSqBB;

        pos.pieces[9] &= ~sqA8;
        pos.pieces[9] |= sqD8;
        pos.blackOccupies &= ~sqA8;
        pos.blackOccupies |= sqD8;
        break;
    case MV_Flags::ENPASSANT_W:
        pos.pieces[0] &= ~fromSqBB;
        pos.pieces[0] |= toSqBB;
        pos.pieces[6] &= ~(toSqBB >> 8);
        pos.blackOccupies &= ~(toSqBB >> 8);
        break;
    case MV_Flags::ENPASSANT_B:
        pos.pieces[6] &= ~fromSqBB;
        pos.pieces[6] |= toSqBB;
        pos.pieces[0] &= ~(toSqBB << 8);
        pos.whiteOccupies &= ~(toSqBB << 8);
        break;
    default:
        break;
    }

    // Prepare en passant target square
    if (piece == 0){
        pos.halfMoveClock = 0;
        U8 fromSqNumber = static_cast<U8>(move);
        U8 toSqNumber = static_cast<U8>(move >> 8);
        int dif = 0;
        if (toSqNumber >= fromSqNumber){
            dif = toSqNumber - fromSqNumber;
        } else {
            dif = -(fromSqNumber - toSqNumber);
        }
        if ((dif == 16) || (dif == -16)){
            pos.enPassantPossible = true;
            pos.enPassantTargetSquare = static_cast<Square>(static_cast<U8>(move) + dif/2);
        }
    }

    updateCastlingRights(pos);

    *attackSqToBeUpdated = attackedSqBy(pos.sideToPlay, pos);

    // Last thing - switch side to play
    pos.sideToPlay = static_cast<SideToPlay>(~pos.sideToPlay);
}


int MoveGenerator::computeHash(BB blockers, U64 magic, int hashBitCount)
{
    // More compact form is:  return (int)((b * magic) >> (64 - bits));
    // But 32-bit multiplication is faster
    return (unsigned)((int)blockers*(int)magic ^ (int)(blockers>>32)*(int)(magic>>32)) >> (32-hashBitCount);
}

﻿/**************************************************************************
**
**   Name: TextProccesing.h
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#ifndef TEXTPROCCESING_H
#define TEXTPROCCESING_H

#include <vector>
#include <string>
#include <algorithm>
#include <functional>

/**
 * Helper class for working with string during parsing commands
 * With static functions only.
 */
class TextProccesing
{
public:
    TextProccesing();

    /**
     * Creates new copy of input string with extra whitespaces removed.
     *
     * Removes all consecutive white spaces and leaves only one 'space' instead.
     * Also removes leading and trailing whitespaces.
     * For example string " aaa  \t\t  bbb\t  g g " turns into "aaa bbb g g".
     *
     * @param input Input string.
     * @return String withnout extra whitespaces.
     */
    static std::string removeExtraWhitespaces(const std::string &input);

    /**
     * Returns part of input from beginning to position of separator (char),
     * returned part does not include separator.
     * Erase this part and separator itself from input.
     *
     * Set of chars, working as separators, is defined through boolean functor.
     *
     * @param input Input string.
     * @param separator_pred Boolean functor, that defines set of separators.
     * @return Returns part of input from beginning to position of separator
     */
    static std::string popFirstPart(std::string &input,
                                    const std::function <bool (char)>& separator_pred);

    /**
     * Returns part of input from beginning to position of separator (char),
     * returned part does not include separator.
     *
     * Set of chars, working as separators, is defined through boolean functor.
     *
     * @param input Input string.
     * @param separator_pred Boolean functor, that defines set of separators.
     * @return Returns part of input from beginning to position of separator
     */
    static std::string getFirstPart(const std::string &input,
                                    const std::function <bool (char)>& separator_pred);

    /**
     * Separate input string based on separator
     * defined by function.
     *
     * Predicate should be function with signature:
     *
     * bool is_separator(char a);
     *
     * Individual parts are stored in vector.
     *
     * @param input Input string.
     * @param is_separator Boolean functor, that defines set of separators.
     * @return Returns vector containing parts of string.
     */
    template<class Predicate>
    static std::vector<std::string> separateString(const std::string &input,
                                                   Predicate is_separator)
    {
        std::vector<std::string> result;
        auto iterLeader = input.cbegin();
        auto iterBehind = input.cbegin();
        while (iterLeader != input.cend()) {
            iterLeader = std::find_if(iterBehind, input.cend(), is_separator);
            result.push_back(std::string(iterBehind, iterLeader));
            iterBehind = iterLeader;
            ++iterBehind; // jump over separator
        }
        return result;
    }

};

#endif // TEXTPROCCESING_H

/**************************************************************************
**
**   Name: Kernel.cpp
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#include "Kernel.h"
#include "spdlog/spdlog.h"
#include <chrono>

Kernel::Kernel()
    : creator(moveGen), evaluator(moveGen), perft(moveGen),
      latestResults(creator.createPositionBB(PositionWith8x8::startposFEN)),
      root_posBB(creator.createPositionBB(PositionWith8x8::startposFEN)),
      maxDepth_(Kernel::default_maxDepth_), maxNodes_(Kernel::default_maxNodes_),
      evalType(PositionEvaluator::EvalType::NORMAL),
      nodesVisited(0), current_eval(0)
{

}

void Kernel::setEvalType(const PositionEvaluator::EvalType eval_type)
{
    evalType = eval_type;
}

void Kernel::setRootPosBB(const PositionBB &pos)
{
    root_posBB = pos;
}

void Kernel::setRootPosBB(const PositionWith8x8 &pos)
{
    root_posBB = creator.createPositionBB(pos);
}

void Kernel::setRootPosBB(const std::string &FEN)
{
    root_posBB = creator.createPositionBB(FEN);
}

std::string Kernel::getRootPosFEN()
{
    return root_posBB.toPositionWith8x8().toFEN();
}

void Kernel::setSearchDepth(U8 searchDepth)
{
    if (searchDepth > 0){
        maxDepth_ = searchDepth;
    }
}

void Kernel::setMaxNodes(U32 maxNodes)
{
    if (maxNodes > 0){
        maxNodes_ = maxNodes;
    }
}

SearchResult Kernel::go()
{
    nodesVisited = 0;
    princVar_reverseOrder.clear();

    adjustEvalType(root_posBB);

    using namespace std::chrono;
    spdlog::debug("Starting tree search..");
    high_resolution_clock::time_point t1 = high_resolution_clock::now();

    double alpha = -std::numeric_limits<double>::max();
    double beta = std::numeric_limits<double>::max();
    current_eval = alphabeta_recursive(princVar_reverseOrder, maxDepth_, root_posBB, alpha, beta);

    if (current_eval*(2*root_posBB.sideToPlay-1) == PositionEvaluator::mateValue){
        // Mate found somewhere in the game tree, from the point of view of engine
        // Engine now have to find the shortest possible path to mate
        princVar_reverseOrder = getShortestPathtoMate(princVar_reverseOrder.size(), root_posBB);
    }

    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    auto time_span = duration_cast<milliseconds>(t2 - t1);

    createResults(time_span.count());

    setDefaultDepth();
    setDefaultNodes();

    return latestResults;
}

U64 Kernel::go_perft(size_t depth)
{
    return perft.go_perft(root_posBB, depth);
}

U64 Kernel::go_qperft(size_t depth)
{
    return perft.go_qperft(root_posBB, depth);
}

std::vector<std::pair<MV, U64> > Kernel::go_qperft_divide(size_t depth)
{
   std::vector<std::pair<MV, U64> > moveQPerft;

   std::vector<MV> movesFromRoot;
   moveGen.allPossibleMoves(root_posBB, movesFromRoot);

   for (auto&& move : movesFromRoot){
       PositionBB newPos = root_posBB;
       moveGen.updatePosPartial(newPos, move);
       if (depth > 1){
           moveQPerft.push_back(std::make_pair(move, perft.go_qperft(newPos, depth - 1)));
       } else {
           moveQPerft.push_back(std::make_pair(move, 1));
       }
   }
   return moveQPerft;
}

std::vector<MV> Kernel::possibleMoves()
{
    std::vector<MV> result;
    moveGen.allPossibleMoves(root_posBB, result);
    return result;
}

std::vector<MV> Kernel::getShortestPathtoMate(U8 maxMovesToMate, const PositionBB pos)
{
    // Normal evaluation is unable to find mate at depth 0
    // This evaluation can do it
    setEvalType(PositionEvaluator::EvalType::MATE_ONLY);

    maxMovesToMate++;
    std::vector<MV> shortest_pv;

    while (true){
        std::vector<MV> pv; // pv = principal variation
        double alpha = -std::numeric_limits<double>::max();
        double beta = std::numeric_limits<double>::max();
        double eval = alphabeta_recursive(pv, maxMovesToMate-1, pos, alpha, beta);

        if (eval*(2*pos.sideToPlay-1) == PositionEvaluator::mateValue){
            // Mate found
            maxMovesToMate = pv.size();
            shortest_pv = pv;
        } else {
            break;
        }
    }
    return shortest_pv;
}

void Kernel::adjustEvalType(const PositionBB &pos)
{
    if (evaluator.arePawnsOnBoard(pos)){
        evalType = PositionEvaluator::EvalType::NORMAL;
    } else {
        evalType = PositionEvaluator::EvalType::ENDGAME;
    }
}

void Kernel::setDefaultDepth(){
    maxDepth_ = Kernel::default_maxDepth_;
}

void Kernel::setDefaultNodes(){
    maxNodes_ = Kernel::default_maxNodes_;
}

void Kernel::createResults(U32 searchTime_milis)
{
    if (searchTime_milis == 0) searchTime_milis = 1;

    latestResults.root = root_posBB;
    latestResults.searchDepth = maxDepth_;
    latestResults.nodesVisited = nodesVisited;

    latestResults.princVar.clear();
    std::reverse_copy(princVar_reverseOrder.begin(),
                      princVar_reverseOrder.end(),
                      std::back_inserter(latestResults.princVar));

    latestResults.eval_score = current_eval;

    latestResults.searchTime_milis = searchTime_milis;
    latestResults.nodesPerSecond = nodesVisited*1000/searchTime_milis;

    if (latestResults.eval_score == std::abs(PositionEvaluator::mateValue)){
        if (latestResults.root.sideToPlay == WHITE && latestResults.eval_score > 0){
            latestResults.movesToMate = latestResults.princVar.size();
        } else if (latestResults.root.sideToPlay == BLACK && latestResults.eval_score < 0) {
            latestResults.movesToMate = latestResults.princVar.size();
        } else {
            latestResults.movesToMate = -latestResults.princVar.size();
        }
    } else {
        latestResults.movesToMate = std::numeric_limits<int>::max();
    }
}

double Kernel::alphabeta_recursive(std::vector<MV> &moveSequence, size_t depth, const PositionBB pos, double alpha, double beta)
{
    nodesVisited++;
    double value;
    if (depth == 0) {
        return evaluator.eval(pos, evalType);
    }

    std::vector<MV> moves;
    if (moveGen.allPossibleMoves(pos, moves)){
        // moves are empty - mate or stalemate
        if (evaluator.isKingInCheck(pos, pos.sideToPlay)){
            return (1-2*pos.sideToPlay)*PositionEvaluator::mateValue;
        } else {
            return 0;
        }
    }

    if (pos.sideToPlay == WHITE) {
        value = -std::numeric_limits<double>::max();
        for (auto&& move : moves) {
            if (nodesVisited >= maxNodes_) break;

            PositionBB newPos = pos;
            moveGen.updatePosPartial(newPos, move);
            if (depth == 1){
                // Making full update of position before evaluation at depth 0
                newPos.attackedSqByBlack = moveGen.attackedSqBy(BLACK, newPos);
            }

            std::vector<MV> newMoveSequence;
            double newValue = alphabeta_recursive(newMoveSequence, depth - 1, newPos, alpha, beta);
            if (newValue > value){
                moveSequence = newMoveSequence;
                moveSequence.push_back(move);
                value = newValue;
                alpha = std::max(alpha, value);
            }
            if (alpha >= beta) break;
        }

    } else {
        value = std::numeric_limits<double>::max();
        for (auto&& move : moves) {
            if (nodesVisited >= maxNodes_) break;

            PositionBB newPos = pos;
            moveGen.updatePosPartial(newPos, move);
            if (depth == 1){
                // Making full update of position before evaluation at depth 0
                newPos.attackedSqByWhite = moveGen.attackedSqBy(WHITE, newPos);
            }

            std::vector<MV> newMoveSequence;
            double newValue = alphabeta_recursive(newMoveSequence, depth - 1, newPos, alpha, beta);
            if (newValue < value){
                moveSequence = newMoveSequence;
                moveSequence.push_back(move);
                value = newValue;
                beta = std::min(beta, value);
            }
            if (alpha >= beta) break;
        }
    }
    return value;
}

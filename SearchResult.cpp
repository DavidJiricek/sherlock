/**************************************************************************
**
**   Name: SearchResults.cpp
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#include "SearchResult.h"

SearchResult::SearchResult(PositionBB only_for_proper_init) :
    root(only_for_proper_init)
{

}

std::string SearchResult::formatForOutput()
{
    std::string result;
    result = "info ";
    result += "depth " + std::to_string(searchDepth) + " ";
    result += "time " + std::to_string(searchTime_milis) + " ";
    result += "nodes " + std::to_string(nodesVisited) + " ";
    result += "score cp " + std::to_string(int(eval_score*100)) + " ";
    result += "nps " + std::to_string(nodesPerSecond) + " ";
    result += "pv ";
    for (auto&& x : princVar){
        result += Move_LongNotation(x).text_form_ + " ";
    }
    return result;
}

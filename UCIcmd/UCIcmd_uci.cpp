/**************************************************************************
**
**   Name: UCIcmd_uci.cpp
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#include "UCIcmd_uci.h"

void UCIcmd_uci::executeCmd(){
        /*
            * uci
                tell engine to use the uci (universal chess interface),
                this will be sent once as a first command after program boot
                to tell the engine to switch to uci mode.

                After receiving the uci command the engine must identify itself with the "id" command
                and send the "option" commands to tell the GUI which engine settings the engine supports if any.
                After that the engine should send "uciok" to acknowledge the uci mode.
                If no uciok is sent within a certain time period, the engine task will be killed by the GUI.

                Id command format

                        * name <x>
                            this must be sent after receiving the "uci" command to identify the engine,
                            e.g. "id name Shredder X.Y\n"
                        * author <x>
                            this must be sent after receiving the "uci" command to identify the engine,
                            e.g. "id author Stefan MK\n"
        */

        if (!restOfCmd_.empty()){
            throw CommExcep(id_name + " " + restOfCmd_, "uci");
        }

        comm_->sendToGUI("id name Sherlock");
        comm_->sendToGUI("id author David Jiricek");
        comm_->sendToGUI("uciok");

        spdlog::debug("UCI command succesfully resolved");
}


/**************************************************************************
**
**   Name: AbstractPosition.cpp
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#include "AbstractPosition.h"

AbstractPosition::AbstractPosition()
{

}

bool AbstractPosition::operator ==(const AbstractPosition &rhs)
{
    if ((sideToPlay == rhs.sideToPlay)
            && (castlingWhiteKside == rhs.castlingWhiteKside)
            && (castlingWhiteQside == rhs.castlingWhiteQside)
            && (castlingBlackKside == rhs.castlingBlackKside)
            && (castlingBlackQside == rhs.castlingBlackQside)
            && (enPassantPossible == rhs.enPassantPossible)
            && (enPassantTargetSquare == rhs.enPassantTargetSquare))
    {
        return true;
    }
    return false;
}

/**************************************************************************
**
**   Name: RecBuffer.h
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#ifndef RECBUFFER_H
#define RECBUFFER_H

#include <queue>
#include <string>
#include <mutex>

class RecBuffer
{
public:
    RecBuffer();

    void push(const std::string& input)
    {
        std::lock_guard<std::mutex> guard(buffer_mutex);
        buffer.push(input);
    }

    std::string try_pop()
    {
        std::lock_guard<std::mutex> guard(buffer_mutex);
        if (buffer.empty()){
            return "";
        } else {
            std::string first_element = buffer.front();
            buffer.pop();
            return first_element;
        }
    }

private:
    std::mutex buffer_mutex;
    std::queue<std::string> buffer;
};

#endif // RECBUFFER_H

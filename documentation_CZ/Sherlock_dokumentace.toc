\select@language {czech}
\contentsline {section}{\numberline {1}\IeC {\'U}vod}{1}{section.1}
\contentsline {section}{\numberline {2}Postup instalace pro Linux}{2}{section.2}
\contentsline {section}{\numberline {3}Pou\IeC {\v z}\IeC {\'\i }v\IeC {\'a}n\IeC {\'\i } programu}{2}{section.3}
\contentsline {section}{\numberline {4}Struktura programu}{3}{section.4}
\contentsline {subsection}{\numberline {4.1}Sekce Communicator}{3}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Sekce Kernel}{3}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}\IeC {\v S}achov\IeC {\'a} pozice}{4}{subsubsection.4.2.1}
\contentsline {subsubsection}{\numberline {4.2.2}Generace tah\IeC {\r u}}{4}{subsubsection.4.2.2}
\contentsline {subsubsection}{\numberline {4.2.3}Magic Bitboards}{5}{subsubsection.4.2.3}
\contentsline {subsubsection}{\numberline {4.2.4}Perft - n\IeC {\'a}stroj na odhalen\IeC {\'\i } chyb p\IeC {\v r}i generaci tah\IeC {\r u}}{6}{subsubsection.4.2.4}
\contentsline {subsubsection}{\numberline {4.2.5}Evaluace pozice}{6}{subsubsection.4.2.5}
\contentsline {subsubsection}{\numberline {4.2.6}\IeC {\v R}e\IeC {\v s}en\IeC {\'\i } probl\IeC {\'e}mu horizontu = SEE (static exchange evaluation)}{7}{subsubsection.4.2.6}
\contentsline {subsubsection}{\numberline {4.2.7}Prohled\IeC {\'a}v\IeC {\'a}n\IeC {\'\i } hern\IeC {\'\i }ho stromu}{7}{subsubsection.4.2.7}
\contentsline {section}{\numberline {5}Mo\IeC {\v z}n\IeC {\'e} vylep\IeC {\v s}en\IeC {\'\i } do budoucna}{8}{section.5}

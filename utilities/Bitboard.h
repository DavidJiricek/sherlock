/**************************************************************************
**
**   Name: Bitboard.h
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2018
**
**************************************************************************/
#ifndef BITBOARD_H
#define BITBOARD_H

#include <array>
#include <vector>

#include "types.h"

/**
 * Chess board is represented by unsingned 64-bit integer,
 * each square can be either 0 or 1.
 *
 * This integer is called bitboard and has a typename BB.
 *
 * This can have different meanings, for example 0 or 1 could mean
 * empty or occupied square.
 *
 * First bit of 64-bit integer represent square a1,
 * second bit square a2 and so on.
 *
 * E.g.
 * 8th  bit represent square h1
 * 9th  bit represent square a2,
 * 57th bit represent square a8,
 * 64th bit represent square h8.
 *
 * For example number 5 can represent empty board
 * with the exception of squares a1 and c1 (only non-zero bits).
 *
 */
class Bitboard
{
public:    
    Bitboard();

    // TODO: Make functions compatible with Win compilators
    // __builtin_ctzll relies on gcc

    /// Get least significant bit
    static int find_lsb(const BB& bitBoard) {
        return __builtin_ctzll(bitBoard);
    }

    /// Pop least significant bit
    static U8 zero_lsb(BB& bitBoard) {
        int index = __builtin_ctzll(bitBoard);
        bitBoard &= ~(1ULL << index);
        return index;
    }

    /// Count the number of ones in bit representation of a number
    static int count_1s(BB bitBoard){
       return __builtin_popcountll(bitBoard);
    }

    /// Transforms bitboard into array
    static std::array<bool, 64> BBtoArray(BB bitBoard);

    /// Print bitboard to std cout
    static void visualizeBB(BB bitBoard);

    /// Transforms array into bitboard
    static BB arrayToBB(std::array<bool, 64> board);

    /// Useful representations of different board configurations
    static const BB sqA1 = 1;
    static const BB sqB1 = 2;
    static const BB sqC1 = 4;
    static const BB sqD1 = 8;
    static const BB sqE1 = 16;
    static const BB sqF1 = 32;
    static const BB sqG1 = 64;
    static const BB sqH1 = 128;

    static const BB sqA8 = sqA1 << 56;
    static const BB sqB8 = sqB1 << 56;
    static const BB sqC8 = sqC1 << 56;
    static const BB sqD8 = sqD1 << 56;
    static const BB sqE8 = sqE1 << 56;
    static const BB sqF8 = sqF1 << 56;
    static const BB sqG8 = sqG1 << 56;
    static const BB sqH8 = sqH1 << 56;

    static const BB castWK = 32 + 64;
    static const BB castWQnotOccup = 8 + 4 + 2;
    static const BB castWQnotInCheck = 8 + 4;
    static const BB castBK = castWK << 56;
    static const BB castBQnotOccup = castWQnotOccup << 56;
    static const BB castBQnotInCheck = castWQnotInCheck << 56;

    static const BB emptyBB = 0;
    static const BB fullBB = ~emptyBB;

    static const BB fileA = 1ULL + (1ULL << 8) +
                            (1ULL << 16) + (1ULL << 24) +
                            (1ULL << 32) + (1ULL << 40) +
                            (1ULL << 48) + (1ULL << 56);

    static const BB fileB = (fileA << 1);
    static const BB fileC = (fileA << 2);
    static const BB fileD = (fileA << 3);
    static const BB fileE = (fileA << 4);
    static const BB fileF = (fileA << 5);
    static const BB fileG = (fileA << 6);


    static const BB fileH = (1ULL << 7)  + (1ULL << 15) +
                            (1ULL << 23) + (1ULL << 31) +
                            (1ULL << 39) + (1ULL << 47) +
                            (1ULL << 55) + (1ULL << 63);



    static const BB rank1 = 255;
    static const BB rank2 = rank1 << 8;
    static const BB rank3 = rank1 << 16;
    static const BB rank4 = rank1 << 24;
    static const BB rank5 = rank1 << 32;
    static const BB rank6 = rank1 << 40;
    static const BB rank7 = rank1 << 48;
    static const BB rank8 = rank1 << 56;

    static const BB smallCenter = (rank4 | rank5) & (fileD | fileE);
    static const BB bigCenter = (rank3 | rank4 | rank5 | rank6) & (fileC | fileD | fileE | fileF);

private:    

};

#endif // BITBOARD_H

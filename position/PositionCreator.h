/**************************************************************************
**
**   Name: PositionCreator.h
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#ifndef POSITIONCREATOR_H
#define POSITIONCREATOR_H

#include "move_generation/MoveGenerator.h"
#include "PositionBB.h"
#include "PositionWith8x8.h"

/**
 * Factory (wrapper) for constructing positionBB.
 */
class PositionCreator
{
public:
    PositionCreator(const MoveGenerator& move_gen);

    const MoveGenerator& move_gen_;

    PositionBB createPositionBB(const PositionWith8x8& pos8x8);

    PositionBB createPositionBB(const std::string& FEN);
};

#endif // POSITIONCREATOR_H

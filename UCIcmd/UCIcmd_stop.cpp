/**************************************************************************
**
**   Name: UCIcmd_stop.cpp
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#include "UCIcmd_stop.h"

void UCIcmd_stop::executeCmd(){
    /*  stop

    stop calculating as soon as possible,
    don't forget the "bestmove" and possibly the "ponder" token when finishing the search
    */

    if (!restOfCmd_.empty()){
        throw CommExcep(id_name + " " + restOfCmd_);
    }

    //spdlog::debug("isready recieved");
    //comm_->sendToGUI("readyok");
}

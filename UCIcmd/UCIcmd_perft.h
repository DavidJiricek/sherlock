/**************************************************************************
**
**   Name: UCIcmd_perft.h
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#ifndef UCICMD_PERFT_H
#define UCICMD_PERFT_H

#include "UCIcmd.h"

class UCIcmd_perft : public UCIcmd
{
public:
    UCIcmd_perft(const std::string& id_name, const std::string& restOfCmd, Communicator* comm):
        UCIcmd(id_name, restOfCmd, comm)
    {

    }

    virtual void executeCmd();
};

#endif // UCICMD_PERFT_H

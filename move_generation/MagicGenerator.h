/**************************************************************************
**
**   Name: MagicGenerator.h
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2018
**
**************************************************************************/
#ifndef MAGICGENERATOR_H
#define MAGICGENERATOR_H

#include "types.h"
#include <array>
#include <random>

/**
 * Generates so called "magic" numbers,
 * used for fast generation of moves of sliders (bishops, rooks, queens).
 *
 * See documentation for details.
 */
class MagicGenerator
{
public:
    MagicGenerator(std::array<BB, 64>& RookMasks, std::array<BB, 64>& BishopMasks);

    std::random_device rd;
    std::mt19937 gen;
    std::uniform_int_distribution<U64> randomU64;

    /// Every square has one magic number for rook and one for bishop.
    std::array<U64, 64> RookMagicNumbers;
    std::array<U64, 64> BishopMagicNumbers;

    /// For each square there is a mask
    /// containing "ones" on squares, where can be a blocker
    /// Blocker can affect possible moves for slider on given square
    std::array<BB, 64>& RookMasks;
    std::array<BB, 64>& BishopMasks;

    std::array<int, 64> RookIndexBitCounts { {
        12, 11, 11, 11, 11, 11, 11, 12,
        11, 10, 10, 10, 10, 10, 10, 11,
        11, 10, 10, 10, 10, 10, 10, 11,
        11, 10, 10, 10, 10, 10, 10, 11,
        11, 10, 10, 10, 10, 10, 10, 11,
        11, 10, 10, 10, 10, 10, 10, 11,
        11, 10, 10, 10, 10, 10, 10, 11,
        12, 11, 11, 11, 11, 11, 11, 12
    }};

    std::array<int, 64> BishopIndexBitCounts { {
        6, 5, 5, 5, 5, 5, 5, 6,
        5, 5, 5, 5, 5, 5, 5, 5,
        5, 5, 7, 7, 7, 7, 5, 5,
        5, 5, 7, 9, 9, 7, 5, 5,
        5, 5, 7, 9, 9, 7, 5, 5,
        5, 5, 7, 7, 7, 7, 5, 5,
        5, 5, 5, 5, 5, 5, 5, 5,
        6, 5, 5, 5, 5, 5, 5, 6,
    }};

    /// Given blockers, magic number and hashBitCount
    /// computes hash for attack hash table
    static int computeHash(BB blockers, U64 magic, int hashBitCount);

    BB indexToBlockers(BB figureMask, int index, int indexBitCount) const;

    /// Computes all possible moves, given certain blockers configuration
    BB possibleAttacksBB(const Square sq, const FigureType figType, const BB blockers);

    U64 generateSparseU64();

    /// For given square and figure type (bishop or rook)
    /// Computes both magic number and corresponding hash table
    void generateMagicAndHashTable(const Square sq, const FigureType figType, U64& magicNumber, std::array<BB, 4096>& hashedAttacks);

    /// For given square, figure type (bishop or rook) and magic number
    /// Computes corresponding hash table
    void fillHashTable(const Square sq, const FigureType figType, const U64 magicNumber, std::array<BB, 4096>& hashedAttacks);

    /// Equivalent to generateMagicAndHashTable with better syntax, currenty not used
    std::pair<U64, std::array<BB, 4096>> generateMagicPair(const Square sq, const FigureType figType);
};

#endif // MAGICGENERATOR_H

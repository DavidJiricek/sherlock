/**************************************************************************
**
**   Name: AbstractPosition.h
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#ifndef ABSTRACTPOSITION_H
#define ABSTRACTPOSITION_H

#include "types.h"

// Forward declaration of PositionCreator class.
class PositionCreator;

// Base class for different representation of chess position.
class AbstractPosition
{

public:
    AbstractPosition();

    bool operator ==(const AbstractPosition& rhs);

    SideToPlay sideToPlay;

    /// Castling rights - true = can castle
    bool castlingWhiteQside;
    bool castlingWhiteKside;
    bool castlingBlackQside;
    bool castlingBlackKside;

    /// True = sideToPlay can make en passant
    /// but only! if it has pawn in the right position
    bool enPassantPossible; 
    Square enPassantTargetSquare;

    int halfMoveClock; // the number of halfmoves since the last capture or pawn advance
    int fullMoveClock;

    friend class PositionCreator;

private:

};

#endif // ABSTRACTPOSITION_H

/**************************************************************************
**
**   Name: UCIcmd_quit.h
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#ifndef UCICMD_QUIT_H
#define UCICMD_QUIT_H

#include "spdlog/spdlog.h"
#include "UCIcmd/UCIcmd.h"

class UCIcmd_quit : public UCIcmd
{
public:
    UCIcmd_quit(const std::string& id_name, const std::string& restOfCmd, Communicator* comm):
        UCIcmd(id_name, restOfCmd, comm)

    {

    }

    virtual void executeCmd();
};

#endif // UCICMD_QUIT_H

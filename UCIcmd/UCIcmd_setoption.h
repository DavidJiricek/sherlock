/**************************************************************************
**
**   Name: UCIcmd_setoption.h
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#ifndef UCICMD_SETOPTION_H
#define UCICMD_SETOPTION_H

#include "UCIcmd/UCIcmd.h"

class UCIcmd_setoption : public UCIcmd
{
public:
    UCIcmd_setoption(const std::string& id_name, const std::string& restOfCmd, Communicator* comm):
        UCIcmd(id_name, restOfCmd, comm)
    {

    }

    virtual void executeCmd();
};

#endif // UCICMD_SETOPTION_H

/**************************************************************************
**
**   Name: Tests.cpp
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#include "Tests.h"

#include <iostream>
#include <sstream>
#include <vector>
#include <chrono>

#include "comm/Communicator.h"
#include "utilities/Bitboard.h"
#include "position/PositionWith8x8.h"
#include "position/PositionBB.h"
#include "move_generation/MoveGenerator.h"
#include "utilities/TextProccesing.h"
#include "types.h"
#include "move_generation/Move_LongNotation.h"
#include "Kernel.h"
#include "Perft.h"
#include "position/PositionCreator.h"

#include "spdlog/spdlog.h"

Tests::Tests()
{

}

U64 Tests::depthFirstSearch(const PositionBB& root, int max_depth, int current_depth, const MoveGenerator& gen, bool divide)
{
    int visited = 0;
    if (current_depth < max_depth){
        std::vector<MV> moves;
        gen.allPossibleMoves(root, moves);
        for(auto&& move : moves){
            PositionBB newPos = root;
            gen.updatePosPartial(newPos, move);
            U64 visitedForMV = depthFirstSearch(newPos, max_depth, current_depth + 1, gen);
            if ((current_depth == 0) && divide){
                std::vector<MV> ts;
                gen.allPossibleMoves(newPos, ts);
                //gen.visualizeMV(move);
                std::cout << SquareToString(static_cast<Square>(static_cast<U8>(move)))
                          << SquareToString(static_cast<Square>(static_cast<U8>(move >> 8)))
                          << " " << visitedForMV << std::endl;
            }
            visited += visitedForMV;
        }
        return visited;
    } else {
        return 1;
    }
}

U64 Tests::onlyCapSearch(const PositionBB &root, int max_depth, int current_depth, const MoveGenerator &gen, bool divide)
{
    int visited = 0;
    if (current_depth < max_depth){
        std::vector<MV> moves;
        gen.allPossibleMoves(root, moves);
        for(auto&& move : moves){
            if (static_cast<U8>(move >> 16) < 100){
                continue;
            }
            PositionBB newPos = root;
            gen.updatePosPartial(newPos, move);
            U64 visitedForMV = onlyCapSearch(newPos, max_depth, current_depth + 1, gen);
            if ((current_depth == 0) && divide){
                std::vector<MV> ts;
                gen.allPossibleMoves(newPos, ts);
                //gen.visualizeMV(move);
                std::cout << SquareToString(static_cast<Square>(static_cast<U8>(move)))
                          << SquareToString(static_cast<Square>(static_cast<U8>(move >> 8)))
                          << " " << visitedForMV << std::endl;
            }
            visited += visitedForMV;
        }
        return visited;
    } else {
        return 1;
    }
}

/* OLD:
    std::array<bool, 64> fileA {{
            1,0,0,0,0,0,0,0,
            1,0,0,0,0,0,0,0,
            1,0,0,0,0,0,0,0,
            1,0,0,0,0,0,0,0,
            1,0,0,0,0,0,0,0,
            1,0,0,0,0,0,0,0,
            1,0,0,0,0,0,0,0,
            1,0,0,0,0,0,0,0
                                }};
    std::array<bool, 64> fileH {{
            0,0,0,0,0,0,0,1,
            0,0,0,0,0,0,0,1,
            0,0,0,0,0,0,0,1,
            0,0,0,0,0,0,0,1,
            0,0,0,0,0,0,0,1,
            0,0,0,0,0,0,0,1,
            0,0,0,0,0,0,0,1,
            0,0,0,0,0,0,0,1
                                }};

    std::array<bool, 64> rank7 {{
            0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,
            1,1,1,1,1,1,1,1,
            0,0,0,0,0,0,0,0
                                }};

    std::array<bool, 64> rank2 {{
            0,0,0,0,0,0,0,0,
            1,1,1,1,1,1,1,1,
            0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0
                                }};

    BB a = Bitboard::arrayToBB(fileA);
    BB b = Bitboard::arrayToBB(fileH);
    BB c = Bitboard::arrayToBB(rank2);
    BB d = Bitboard::arrayToBB(rank7);

    Bitboard::visualizeBB(a);
    Bitboard::visualizeBB(b);
    Bitboard::visualizeBB(c);
    Bitboard::visualizeBB(d);

MOVE TEST:
    MV move;
    U8 from = 51;
    U8 to = 43;
    move = from + (to << 8);
    myBB.updateLegalNoFlags(move);

    myBB.visualize();

    from = D2;
    to = D4;
    move = from + (to << 8);
    myBB.updateLegalNoFlags(move);

    std::vector<Square> squares;
    squares.push_back(A1);
    squares.push_back(H1);
    BB result = Bitboard::squaresToBB(squares);
    Bitboard::visualizeBB(result);
*/


void Tests::run()
{
    Kernel kernel;

    //Communicator comm(std::cin, std::cout, kernel);
    //using namespace std::chrono;

    //high_resolution_clock::time_point t1 = high_resolution_clock::now();
    //std::cout << comm.kernel_.go_perft(6) << std::endl;
    //high_resolution_clock::time_point t2 = high_resolution_clock::now();
    //duration<double> time_span = duration_cast<duration<double>>(t2 - t1);

    //std::cout << time_span.count() << std::endl;

    Communicator comm(std::cin, std::cout, kernel);
    try {
        comm.commWithGUI();
        spdlog::info("Engine exited succesfully with error code 0");
    } catch (CommExcep& e) {
        std::cout << e.msg() << std::endl;
        spdlog::info("Engine encountered error, error code 1");
    }
}

void Tests::run_just_kernel(const std::string& FEN)
{
    Kernel kernel;
    PositionWith8x8 root8x8(FEN);
    root8x8.visualize();
    kernel.setRootPosBB(root8x8);


    using namespace std::chrono;
    high_resolution_clock::time_point t1 = high_resolution_clock::now();

    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
    std::cout << "It took me " << time_span.count() << " seconds." << std::endl;
}

void Tests::perft(const std::string& FEN)
{
    MoveGenerator move_gen;
    PositionCreator creator(move_gen);

    PositionBB root(creator.createPositionBB(FEN));
    root.visualize();

    Perft perft(move_gen);
    using namespace std::chrono;
    high_resolution_clock::time_point t1 = high_resolution_clock::now();

    //U64 numOfNodes = perft.go_perft(root, 4);
    U64 numOfNodesQ = perft.go_qperft(root, 6);
    //std::cout << std::to_string(numOfNodes) << std::endl;
    std::cout << std::to_string(numOfNodesQ) << std::endl;
    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
    std::cout << "It took me " << time_span.count() << " seconds." << std::endl;
    std::cout << "Speed is " << numOfNodesQ/time_span.count() << " nodes per second." << std::endl;
}

void Tests::capture_perft(const std::string &FEN)
{
    MoveGenerator move_gen;
    PositionCreator creator(move_gen);

    PositionBB root(creator.createPositionBB(FEN));
    root.visualize();
    Perft perft(move_gen);
    using namespace std::chrono;
    high_resolution_clock::time_point t1 = high_resolution_clock::now();
    //U64 numOfCapNodesfast = perft.go_capture_only_fast(root, 8);
    U64 numOfCapNodes = perft.go_capture_only_perft(root, 8);
    //std::cout << std::to_string(numOfCapNodesfast) << std::endl;
    std::cout << std::to_string(numOfCapNodes) << std::endl;
    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
    std::cout << "It took me " << time_span.count() << " seconds." << std::endl;
    std::cout << "Speed is " << numOfCapNodes/time_span.count() << " nodes per second." << std::endl;
}

void Tests::test_see(const std::string &FEN)
{
    MoveGenerator mv_gen;
    PositionCreator creator(mv_gen);

    PositionBB root(creator.createPositionBB(FEN));

    root.visualize();
    PositionEvaluator pos_eval(mv_gen);

    std::vector<MV> moves;
    mv_gen.allPossibleMoves(root, moves);
    size_t i = 0;
    U8 targetSq;
    for (i = 0; i < moves.size(); i++){
        U8 info = moves[i] >> 16;
        if (info >= 100){
            targetSq = moves[i] >> 8;
            std::cout << "Target sq " << SquareToString(static_cast<Square>(targetSq)) << std::endl;
            std::cout << "SEE " << std::to_string(pos_eval.see(root,static_cast<Square>(targetSq))) << std::endl;
        }
    }
}

void Tests::test_eval(const std::string &FEN)
{
    MoveGenerator mv_gen;
    PositionCreator creator(mv_gen);

    PositionBB root(creator.createPositionBB(FEN));

    root.visualize();
    PositionEvaluator pos_eval(mv_gen);

    std::cout << "Eval endgame " << pos_eval.eval_endgame(root) << std::endl;
    std::cout << "Eval new " << pos_eval.eval_with_see(root) << std::endl;
    std::cout << "Eval " << pos_eval.eval_without_see(root) << std::endl;
}


void Tests::test_distance()
{
    MoveGenerator mv;
    PositionEvaluator ev(mv);
    std::cout << "Distance " << std::to_string(ev.distance(A1, A2)) << std::endl;
}


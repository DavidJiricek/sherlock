/**************************************************************************
**
**   Name: UCIcmd_perft.cpp
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#include "UCIcmd_perft.h"
#include "spdlog/spdlog.h"
#include <chrono>

void UCIcmd_perft::executeCmd(){

    spdlog::debug("Perft command recieved");

    size_t depth;
    if (restOfCmd_.empty()){
        throw CommExcep(id_name + " " + restOfCmd_,
                        "After perft, there should be number between 1 and 255, indicating depth of perft");
    } else {
        depth = std::stol(restOfCmd_);
    }

    if (depth < 1 || depth > 255){
        throw CommExcep(id_name + " " + restOfCmd_,
                        "After perft, there should be number between 1 and 255, indicating depth of perft");
    }

    using namespace std::chrono;
    high_resolution_clock::time_point t1 = high_resolution_clock::now();

    std::vector<std::pair<MV, U64>> result = comm_->kernel_.go_qperft_divide(depth);
    std::string posFEN = comm_->kernel_.getRootPosFEN();

    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    duration<double> time_span = duration_cast<duration<double>>(t2 - t1);

    comm_->sendToGUI("Perft");
    comm_->sendToGUI("Position: " + posFEN);

    U64 total_nodes = 0;
    for (auto&& x : result) {
       total_nodes += x.second;
       comm_->sendToGUI(Move_LongNotation(x.first).text_form_ + " " + std::to_string(x.second));
    }
    comm_->sendToGUI("Total " + std::to_string(total_nodes));
    comm_->sendToGUI("Finished in " + std::to_string(time_span.count()) + " seconds.");

    spdlog::debug("Perft finished.");
}

/**************************************************************************
**
**   Name: UCIcmd_go.h
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#ifndef UCICMD_GO_H
#define UCICMD_GO_H

#include "spdlog/spdlog.h"

#include "UCIcmd/UCIcmd.h"
#include <functional>

class UCIcmd_go : public UCIcmd
{
public:
    UCIcmd_go(const std::string& id_name, const std::string& restOfCmd, Communicator* comm):
        UCIcmd(id_name, restOfCmd, comm)
    {
        initParseMap();
    }

    virtual void executeCmd();

    //static const std::map<std::string, int> parseMap;

    std::map<std::string, std::function<void(std::deque<std::string>&)>> parseMap;

    void initParseMap();

    void setDepth(std::deque<std::string> &params);
    void setNodes(std::deque<std::string> &params);
};

#endif // UCICMD_GO_H

/**************************************************************************
**
**   Name: UCIcmd_quit.cpp
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#include "UCIcmd_quit.h"

void UCIcmd_quit::executeCmd(){
    /* quit

    quit the program as soon as possible

    */

    spdlog::debug("Quit command recieved.");

    comm_->sendToGUI("Engine is stopping...");
    comm_->sendAndPrintInGUI("Engine is stopping...");

    comm_->quit();

}

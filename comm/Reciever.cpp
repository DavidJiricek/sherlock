/**************************************************************************
**
**   Name: Reciever.cpp
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#include "Reciever.h"


void Reciever::readFromGUI()
{
    spdlog::debug("Recieving...");
    while (true) {
        std::string line;
        std::getline(inStream_, line);
        spdlog::debug("Message from GUI recieved.");
        if (inStream_.fail()){
            throw CommExcep("Error during reading input.");
        } else {
            buffer_.push(line);
            spdlog::debug("GUI to Engine: " + line);
        }
    }
}

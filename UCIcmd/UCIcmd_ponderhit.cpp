/**************************************************************************
**
**   Name: UCIcmd_ponderhit.cpp
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#include "UCIcmd_ponderhit.h"

void UCIcmd_ponderhit::executeCmd(){
    /* ponderhit
    the user has played the expected move. This will be sent if the engine was told to ponder on the same move
    the user has played. The engine should continue searching but switch from pondering to normal search.
    */
}

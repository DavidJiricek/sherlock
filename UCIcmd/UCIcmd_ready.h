/**************************************************************************
**
**   Name: UCIcmd_ready.h
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#ifndef UCICMD_READY_H
#define UCICMD_READY_H

#include "spdlog/spdlog.h"
#include "UCIcmd/UCIcmd.h"

class UCIcmd_ready : public UCIcmd
{
public:
    UCIcmd_ready(const std::string& id_name, const std::string& restOfCmd, Communicator* comm):
        UCIcmd(id_name, restOfCmd, comm)
    {

    }

    virtual void executeCmd();
};

#endif // UCICMD_READY_H

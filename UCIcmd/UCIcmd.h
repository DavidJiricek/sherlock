/**************************************************************************
**
**   Name: UCIcmd.h
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#ifndef UCICMD_H
#define UCICMD_H

#include "comm/Communicator.h"

class UCIcmd
{
public:
    UCIcmd(const std::string& id_name, const std::string& restOfCmd, Communicator* comm):
        id_name(id_name), restOfCmd_(restOfCmd), comm_(comm)
    {
        restOfCmd_ = restOfCmd;
    }

    const std::string id_name;
    std::string restOfCmd_;
    Communicator* comm_;
    virtual void executeCmd() = 0;

    void setRestOfCmd(std::string restOfCmd){
        restOfCmd_ = restOfCmd;
    }

};

#endif // UCICMD_H

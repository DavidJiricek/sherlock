/**************************************************************************
**
**   Name: Move_LongNotation.h
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#ifndef MOVE_LONGNOTATION_H
#define MOVE_LONGNOTATION_H

#include "types.h"

/**
 *  Class representing moves in long notation (e.g. e4e5, g8f6)
 *  This notation is consisted of "from square" and "to square"
 *  with a possible promotion flag (e.g. b7b8q)
 */
struct Move_LongNotation{
    enum Promotion_Flag : char {
        QUEEN = 'q',
        KNIGHT = 'n',
        BISHOP = 'b',
        ROOK = 'r',
        NONE = 'N'
    };


    Move_LongNotation(Square fromSq, Square toSq, Promotion_Flag flag)
        : fromSq_(fromSq), toSq_(toSq), flag_(flag)
    {
        text_form_ = SquareToString(fromSq) + SquareToString(toSq);
        if (flag != Promotion_Flag::NONE){
            text_form_ += static_cast<char>(flag);
        }
    }

    Move_LongNotation(std::string text_form)
        : text_form_(text_form)
    {
        fromSq_ =  static_cast<Square>(8*(text_form_[1]-'1')+(text_form_[0]-'a'));
        toSq_ =  static_cast<Square>(8*(text_form_[3]-'1')+(text_form_[2]-'a'));
        if (text_form.length() > 4){
           flag_ = static_cast<Promotion_Flag>(text_form_[4]);
        } else {
           flag_ = Promotion_Flag::NONE;
        }
    }

    Move_LongNotation(MV encoded_move)
    {
        fromSq_ = static_cast<Square>(static_cast<U8>(encoded_move));
        toSq_ = static_cast<Square>(static_cast<U8>(encoded_move >> 8));
        MV_Flags encoded_flag = static_cast<MV_Flags>(encoded_move >> 24);

        switch (encoded_flag) {
        case MV_Flags::PROMOTION_N:
            flag_ = Promotion_Flag::KNIGHT;
            break;
        case MV_Flags::PROMOTION_B:
            flag_ = Promotion_Flag::BISHOP;
            break;
        case MV_Flags::PROMOTION_R:
            flag_ = Promotion_Flag::ROOK;
            break;
        case MV_Flags::PROMOTION_Q:
            flag_ = Promotion_Flag::QUEEN;
            break;
        default:
            flag_ = Promotion_Flag::NONE;
            break;
        }

        text_form_ = SquareToString(fromSq_) + SquareToString(toSq_);
        if (flag_ != Promotion_Flag::NONE){
            text_form_ += static_cast<char>(flag_);
        }
    }

    Square fromSq_;
    Square toSq_;
    Promotion_Flag flag_;

    std::string text_form_;
};

#endif // MOVE_LONGNOTATION_H

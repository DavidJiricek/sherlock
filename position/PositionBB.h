/**************************************************************************
**
**   Name: Position.h
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2018
**
**************************************************************************/
#ifndef POSITION_H
#define POSITION_H

#include <array>
#include <map>
#include "utilities/Bitboard.h"
#include "types.h"
#include "AbstractPosition.h"
#include "PositionWith8x8.h"

// Forward declaration of PositionCreator class.
class PositionCreator;

/**
 * Position with bitboards (BB). Used for all internal computation
 * in MoveGenerator, PositionEvaluator etc.
 *
 * Most important members of this class are
 * pieces[], whiteOccupies, blackOccupies, attackedSqByWhite, attackedSqByBlack
 *
 * Together with members of AbstractPosition gives complete info about position.
 */
class PositionBB : public AbstractPosition
{
public:
    PositionWith8x8 toPositionWith8x8() const;

    // Used during conversion to usual 8x8 chess board
    void alterChessBoard(BB figPositions, FigureTypeColoured figType,
                         std::array<FigureTypeColoured, 64>& chessBoard) const;

    /// Print visualized position to std cout
    /// Can be used for debug
    void visualize() const;

    bool operator ==(const PositionBB& rhs){
        for (int i = 0; i < 12; ++i) {
            if (pieces[i] != rhs.pieces[i]){
                return false;
            }
        }
        if ((whiteOccupies == rhs.whiteOccupies)
                && blackOccupies == rhs.blackOccupies
                && attackedSqByWhite == rhs.attackedSqByWhite
                && attackedSqByBlack == rhs.attackedSqByBlack
                && AbstractPosition::operator ==(rhs))
        {
            return true;
        } else {
            return false;
        }
    }

    friend class PositionCreator;

    /**
     * Pieces is array of occupation bitboards.
     *
     * 0 - wPawns, 1 - wKnights, 2 - wBishops,
     * 3 - wRooks, 4 - wQueens, 5 - wKing
     *
     * 6 - bPawns, 7 - bKnights, 8 - bBishops,
     * 9 - bRooks, 10 - bQueens, 11 - bKing
     *
     */
    std::array<BB, 12> pieces;

    /// Sum of pieces occupation bitboards
    BB whiteOccupies;
    BB blackOccupies;

    /// Squares attacked by pieces of given color
    /// Useful for generating king moves
    BB attackedSqByWhite;
    BB attackedSqByBlack;

private:
    /// Constructor is private
    /// Can be constructed only by class PositionCreator

    /// This is because to initialize attackedSqBy,
    /// we need MoveGenerator.
    /// Including MoveGenerator in PositionBB
    /// would lead to circular dependecies.
    PositionBB(PositionWith8x8 pos);


};

#endif // POSITION_H

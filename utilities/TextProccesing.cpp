/**************************************************************************
**
**   Name: TextProccesing.cpp
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#include "TextProccesing.h"

TextProccesing::TextProccesing()
{

}

std::string TextProccesing::removeExtraWhitespaces(const std::string &input)
{
    std::string result;

    // Leading whitespaces
    auto iterBegin = input.cbegin();
    while (isspace(*iterBegin)) {
        ++iterBegin;
    }

    if (iterBegin == input.cend()){return result;}

    // Trailing whitespaces
    auto iterEnd = input.crbegin();
    while (isspace(*iterEnd)) {
        ++iterEnd;
    }

    std::unique_copy (iterBegin, iterEnd.base(), std::back_insert_iterator<std::string>(result),
                      [](char a,char b){ return isspace(a) && isspace(b);});


    std::transform(result.begin(), result.end(), result.begin(),
                   [](char a){if (isspace(a)) {return ' ';} else {return a;}});

    return result;
}

std::string TextProccesing::popFirstPart(std::string &input, const std::function<bool (char)> &separator_pred)
{
    auto iter = std::find_if(input.cbegin(), input.cend(), separator_pred);
    std::string result(input.cbegin(), iter);
    if (iter != input.cend()){
        iter++;
    }
    input.erase(input.cbegin(), iter);
    return result;
}

std::string TextProccesing::getFirstPart(const std::string &input, const std::function<bool (char)> &separator_pred)
{
    auto iter = std::find_if(input.cbegin(), input.cend(), separator_pred);
    std::string result(input.cbegin(), iter);
    return result;
}

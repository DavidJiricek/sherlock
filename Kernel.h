/**************************************************************************
**
**   Name: Kernel.h
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#ifndef KERNEL_H
#define KERNEL_H

#include "position/PositionBB.h"
#include "move_generation/Move_LongNotation.h"
#include "move_generation/MoveGenerator.h"
#include "position/PositionCreator.h"
#include "position/PositionEvaluator.h"
#include "SearchResult.h"
#include "Perft.h"


/**
 *  Core of the engine, perform search of game tree.
 *
 */
class Kernel
{
public:
    Kernel();

    /**
     * Performs alphabeta tree search.
     * Nodes at depth 0 are evaluated according to state of variable Kernel::evalType
     *
     * If Kernel::evalType is NORMAL,
     * then this search is able to find mate only at depth greater than 0.
     *
     * With MATE_ONLY evaluation, this search will look for mate even in depth 0.
     *
     * @param input moveSequence Principal variation, in reverse order, will be created here.
     *                              Last element is first move to play and best move found.
     *                              Lenght will be less or equal to the depth.
     * @param depth Desired depth of search, also depth "to go". It is gradually lowered during recursion.
     * @param pos Root position for the search.
     * @param alpha Best option available for maximizer along the path to the root.
     * @param beta Best option available for minimizer along the path to the root.
     * @return Returns Evaluation of node reached through optimal play from both sides
     */
    double alphabeta_recursive(std::vector<MV> &moveSequence, size_t depth, const PositionBB pos, double alpha, double beta);

    /**
     * Execute UCI go command.
     *
     * Performs search from position Kernel::rootPosBB.
     * Search is affected by state of kernel (maxDepth, maxNodes, etc.)
     */
    SearchResult go();
    U64 go_perft(size_t depth);
    U64 go_qperft(size_t depth);
    std::vector<std::pair<MV,U64>> go_qperft_divide(size_t depth);
    std::vector<MV> possibleMoves();

    void adjustEvalType(const PositionBB &pos);
    void createResults(U32 timeOfSearching);

    void setSearchDepth(U8 searchDepth);
    void setMaxNodes(U32 searchDepth);

    void setRootPosBB(const PositionBB &pos);
    void setRootPosBB(const PositionWith8x8 &pos);
    void setRootPosBB(const std::string &FEN);
    std::string getRootPosFEN();

    void setEvalType(const PositionEvaluator::EvalType eval_type);

    void setDefaultDepth();
    void setDefaultNodes();

    /**
     * Helper function, only to be used during go command.
     *
     * It is used when we know for sure, that from position pos
     * mate is maxMovesToMate away.
     * It has to be mate (win) from to pos.SideToPlay view.
     *
     * Then this function returns shortest possible winning sequence of moves.
     *
     * Else (in the case mate is not maxMovesToMate away) returns empty vector.
     */
    std::vector<MV> getShortestPathtoMate(U8 maxMovesToMate, const PositionBB pos);

private:
    MoveGenerator moveGen;
    PositionCreator creator;
    PositionEvaluator evaluator;
    Perft perft;

    SearchResult latestResults;

    PositionBB root_posBB;

    U8 maxDepth_;
    U32 maxNodes_;

    PositionEvaluator::EvalType evalType;

    U32 nodesVisited;
    double current_eval;
    std::vector<MV> princVar_reverseOrder;

    static constexpr U8 default_maxDepth_ = 6;
    static constexpr U32 default_maxNodes_ = std::numeric_limits<U32>::max();

};

#endif // KERNEL_H

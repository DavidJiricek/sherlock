/**************************************************************************
**
**   Name: CommExcep.h
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#ifndef COMMEXCEP_H
#define COMMEXCEP_H

#include <string>
#include <exception>
#include <iostream>

/**
 * Exception that is thrown during communication with user or GUI.
 */
class CommExcep : public std::exception
{
public:
    CommExcep(){}
    CommExcep(const std::string& badCmd): badCmd_(badCmd)
    {

    }

    CommExcep(const std::string& badCmd, const std::string& expectedCmd):
        badCmd_(badCmd), expectedCmd_(expectedCmd)
    {

    }

    std::string msg()
    {
        std::string what;
        if (badCmd_.empty()){
            what = "Undefined error during communication.";
        } else {
            what = "User command not recognized: " + badCmd_;
            if (!expectedCmd_.empty()){
                what += "\nExpected format is: " + expectedCmd_;
            }
        }
        return what;
    }
private:
    std::string badCmd_;
    std::string expectedCmd_;
};

#endif // COMMEXCEP_H

/**************************************************************************
**
**   Name: MoveGenerator.h
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2018
**
**************************************************************************/
#ifndef MOVEGENERATOR_H
#define MOVEGENERATOR_H

#include <array>
#include <vector>

#include "MagicGenerator.h"
#include "position/PositionBB.h"
#include "types.h"

class MoveGenerator
{
public:

    const std::array<int, 8> KnightHelpTableRanks {{2, 1, -1, -2, -2, -1, 1, 2}}; //only for generating KnightMasks
    const std::array<int, 8> KnightHelpTableFiles {{1, 2, 2, 1, -1, -2, -2, -1}}; //only for generating KnightMasks
    
    std::array<BB, 64> RookMasks;
    std::array<BB, 64> BishopMasks;
    std::array<BB, 64> QueenMasks;
    std::array<BB, 64> KingMasks;
    std::array<BB, 64> KnightMasks;

    /// Hash table with all different attacks,
    /// depending on blockers configurations.
    std::vector<std::array<BB, 4096>> bishopHashedAttacks;
    std::vector<std::array<BB, 4096>> rookHashedAttacks;

    const std::array<U64, 64> bishopMagicNumbers
    {{0X400620400101146, 0X4510A20402241004, 0X25280241084801, 0X1005201A22082,
            0X21080A0200A0A10, 0XC80310020020804, 0X8240140740192407, 0X8184908100050C1,
            0X408048C62482003, 0X2062044800010A1, 0X4610488000008434, 0X23005042022080A,
            0X1041742020138212, 0X20100240040CA432, 0X2809040422407403, 0X4210C5812202076,
            0X4040400044C2040, 0XC20800404210263C, 0X28449100A2010810, 0X4411688401080001,
            0XA8400144088044, 0X490040010812802, 0X5808920000108201, 0X10842080E002021,
            0X2908010000200988, 0X45404800008A005, 0X284102401480500A, 0X20281018418200,
            0X22404400081804, 0XA141108438040811, 0X3042480400110510, 0X8A0840409004220,
            0X44280520104461, 0X202880090088C14, 0X9080020042104, 0X5008025000202C04,
            0X41028408002C, 0X4044509100480A2, 0X20120000510B4, 0X2E40210221341082,
            0X2000180668180994, 0X120004295805E410, 0X490100024010191, 0X411088100002214,
            0X480004C4782C031, 0X3000802042440C, 0X5020280C0022688, 0X8048610C021414,
            0X120A544400004804, 0X30060A0001083164, 0XC110005018040C2, 0X1044101200740002,
            0X206002280501254, 0X282000004012442, 0X8401000600C40410, 0X8501114400C40148,
            0X11092004080808B, 0X890120412010C101, 0X4421800800422A2, 0X420212000830C0,
            0X1002022420809080, 0X200C111080900124, 0X1260442200C0850, 0X102003200101170}};

    const std::array<U64, 64> rookMagicNumbers
    {{0X40081401008001, 0X4300620000402010, 0X1024008804200008, 0X252288000202084,
            0X5082002003002, 0X490252806002C12, 0X801022A00800081, 0X801020C02008081,
            0X192400022010080, 0X2022028000008040, 0X2810C004002004, 0X8032040808810021,
            0X90058980008008, 0X80D20200008004, 0X211081480020086, 0X2400200410201,
            0X2028810000004040, 0X1040601400204040, 0X6000940102021010, 0X1080090000100808,
            0X900C432800000404, 0X2010080400082202, 0X1044060064008101, 0X582458000480101,
            0X4B028040A06106, 0X810026007800C28A, 0X42042000108912, 0X202812000015400A,
            0X801020C08101009, 0X422013020140A, 0X4801090400000201, 0X8120018200408141,
            0X6C40004080800020, 0X880400022008020, 0X8010080106C020, 0X810080000103241,
            0X41008000100B004, 0X200840800C03102, 0X600040158004812, 0X40800180300240C9,
            0X88080040412004, 0X4000100020042008, 0X2208080004030012, 0X41040200000801,
            0X1200049023600A, 0X4002000800008472, 0X2200206000408021, 0X4440808020428C99,
            0X6242310000012090, 0X48004020100226, 0X40A0010110084110, 0X4018008000008010,
            0X4200202008090884, 0X614020000402806, 0X201104004604006, 0X804C020180008201,
            0X1090C30100022080, 0X1100248210204412, 0X4100C031110008E0, 0X1204900180012209,
            0X104102200020802, 0X2010900080804, 0X1000506008082, 0X408A6100000053}};
    
    const std::array<int, 64> RookIndexBitCounts { {
        12, 11, 11, 11, 11, 11, 11, 12,
        11, 10, 10, 10, 10, 10, 10, 11,
        11, 10, 10, 10, 10, 10, 10, 11,
        11, 10, 10, 10, 10, 10, 10, 11,
        11, 10, 10, 10, 10, 10, 10, 11,
        11, 10, 10, 10, 10, 10, 10, 11,
        11, 10, 10, 10, 10, 10, 10, 11,
        12, 11, 11, 11, 11, 11, 11, 12
    }};

    const std::array<int, 64> BishopIndexBitCounts { {
        6, 5, 5, 5, 5, 5, 5, 6,
        5, 5, 5, 5, 5, 5, 5, 5,
        5, 5, 7, 7, 7, 7, 5, 5,
        5, 5, 7, 9, 9, 7, 5, 5,
        5, 5, 7, 9, 9, 7, 5, 5,
        5, 5, 7, 7, 7, 7, 5, 5,
        5, 5, 5, 5, 5, 5, 5, 5,
        6, 5, 5, 5, 5, 5, 5, 6,
    }};

    static const BB emptyBB = Bitboard::emptyBB;
    static const BB fullBB = Bitboard::fullBB;

    static const BB sqA1 = Bitboard::sqA1;
    static const BB sqD1 = Bitboard::sqD1;
    static const BB sqE1 = Bitboard::sqE1;
    static const BB sqF1 = Bitboard::sqF1;
    static const BB sqH1 = Bitboard::sqH1;
    static const BB sqA8 = Bitboard::sqA8;
    static const BB sqD8 = Bitboard::sqD8;
    static const BB sqE8 = Bitboard::sqE8;
    static const BB sqF8 = Bitboard::sqF8;
    static const BB sqH8 = Bitboard::sqH8;

    static const BB castWK = Bitboard::castWK;
    static const BB castWQnotInCheck = Bitboard::castWQnotInCheck;
    static const BB castWQnotOccup = Bitboard::castWQnotOccup;
    static const BB castBK = Bitboard::castBK;
    static const BB castBQnotInCheck = Bitboard::castBQnotInCheck;
    static const BB castBQnotOccup = Bitboard::castBQnotOccup;

    BB fileA = Bitboard::fileA;
    BB fileH= Bitboard::fileH;
    BB rank1= Bitboard::rank1;
    BB rank2= Bitboard::rank2;
    BB rank3= Bitboard::rank3;
    BB rank4= Bitboard::rank4;
    BB rank5= Bitboard::rank5;
    BB rank6= Bitboard::rank6;
    BB rank7= Bitboard::rank7;
    BB rank8= Bitboard::rank8;
    
    MoveGenerator();

    /// Main functions of MoveGenerator
    /// Vector otherMoves is emptied and overwritten in this function
    /// Return value is 1 in case of empty otherMoves vector, 0 otherwise.
    bool allPossibleMoves(const PositionBB& pos, std::vector<MV>& otherMoves) const;
    bool allPossibleCaptures(const PositionBB& pos, std::vector<MV>& otherMoves) const;

    /// For debug reasons
    void visualizeMV(MV move) const;

    /// Is king of the side to move in double check?
    /// Also creates BB sqToBlockCheck to be used later during move generation
    bool isInDoubleCheck(const PositionBB& pos, BB &sqToBlockCheck, const int offset, SideToPlay side) const;

    /**
     * Main function for updating position with move MV.
     * There is currently no reverse update implemented.
     *
     * Be careful !!!
     * This is only partial update for purpose of move gen only.
     * It is a little bit quicker than full update.
     *
     * sqAttackedBy are updated only for one side,
     * so that other side can generate moves for king.
     *
     * It cannot be used for see (static exchange evaluation),
     * because we need sqAttackedBy updated for both sides.
     *
     * This issue is currently resolved directly in alphabeta routine.
     */
    void updatePosPartial(PositionBB &pos, MV move) const;

    /// Compute squares attacked by given side
    BB attackedSqBy(SideToPlay side, const PositionBB& pos) const;

    /// Compute hash for attack hash tables (rookHashedAttacks, bishopHashAttacks)
    static int computeHash(BB blockers, U64 magic, int hashBitCount);

private:

    // Miscellaneous and helper functions
    // ..................................

    BB generateMask(Square sq, FigureType figType);

    /**
     * How is move MV encoded?
     *
     * 1 - 8   bit... from square (number from 0 to 64)
     * 9 - 16  bit... to square (number from 0 to 64)
     * 17 - 24 bit... info = pieceNumber + isCapture*100
     * 25 - 32 bit... flag (promotion, castling, enpassant)
     *
     * isCapture is 0 or 1
     * pieceNumber correspond to positionBB.pieces[] (see PositionBB)
     * flag is defined in types.h - MV_Flags
     */
    MV encodeMV(U8 fromSq, U8 toSquare, U8 piece, bool isCapture, MV_Flags flags) const;
    MV encodeMV(Square fromSq, Square toSq, FigureType piece, bool isCapture, MV_Flags flags) const;

    void castling(const PositionBB &pos, std::vector<MV> &otherMoves) const;

    /// Helper function only used during updatePos
    void updateCastlingRights(PositionBB &pos) const;

    void enPassant(BB pawnPos, BB targetSq, SideToPlay side, std::vector<MV> &otherMoves, const PositionBB &pos, BB sqToBlockCheck = fullBB) const;
    bool enPasDiscoveredCheck(const PositionBB &pos, BB pawnThatIsTaken, BB movingPawn, BB rank, BB king, BB rooksOrQueens) const;

    // .................................



    // Dealing with pinned figures
    // >>>>>>>>>>>>>>>>>>>>>>>>>>>

    BB identifyPinnedFigures(const PositionBB& position, std::vector<MV> &otherMoves, bool isKingInCheck) const;
    BB identifyPinnedFigures_forCapture(const PositionBB& pos, std::vector<MV> &otherMoves, bool isKingInCheck) const;

    void pinnedPawnPush(BB pawnPos, BB rayBetween, SideToPlay side, std::vector<MV> &otherMoves) const;
    void pinnedPawnCapture(BB pawnPos, BB sqToCap, SideToPlay side, std::vector<MV> &otherMoves, bool enPass) const;

    void pinnedSliderMoves(BB sliderPos, BB pinningPiece, BB rayBetween, std::vector<MV> &otherMoves, FigureType figTyp) const;
    void pinnedSliderCaptures(BB sliderPos, BB pinningPiece, std::vector<MV> &otherMoves, FigureType figTyp) const;

    // >>>>>>>>>>>>>>>>>>>>>>>>>>>



    // Only used when king is in SINGLE check - generate moves or captures for all figures
    // -----------------------------------------------------------------------------------

    // Every move should end on one of the squares of @sqToBlockCheck.

    // If king is not in check, it is a full bitboard.
    // If the king is in check, it is a set of squares that will block the check
    // (including capturing checking figure).

    // Every move is pushed into otherMoves vector.

    void pawnPushes(BB pawns, SideToPlay side, BB allPiecesTogether, std::vector<MV> &otherMoves, BB sqToBlockCheck = fullBB) const;
    void pawnCaptures(BB pawns, BB enemyOccupies, SideToPlay side, std::vector<MV> &otherMoves, BB sqToBlockCheck = fullBB) const;

    void knightMoves(BB knights, BB enemyOccupies, BB friendOccupies, std::vector<MV> &otherMoves, BB sqToBlockCheck = fullBB) const;
    void knightCaptures(BB knights, BB enemyOccupies, std::vector<MV> &otherMoves, BB sqToBlockCheck = fullBB) const;

    void bishopMoves(BB bishops, BB enemyOccupies, BB friendOccupies, std::vector<MV> &otherMoves, BB sqToBlockCheck = fullBB) const;
    void bishopCaptures(BB bishops, BB enemyOccupies, BB friendOccupies, std::vector<MV> &otherMoves, BB sqToBlockCheck = fullBB) const;

    void rookMoves(BB rooks, BB enemyOccupies, BB friendOccupies, std::vector<MV> &otherMoves, BB sqToBlockCheck = fullBB) const;
    void rookCaptures(BB rooks, BB enemyOccupies, BB friendOccupies, std::vector<MV> &otherMoves, BB sqToBlockCheck = fullBB) const;

    void queenMoves(BB queens, BB enemyOccupies, BB friendOccupies, std::vector<MV> &otherMoves, BB sqToBlockCheck = fullBB) const;
    void queenCaptures(BB queens, BB enemyOccupies, BB friendOccupies, std::vector<MV> &otherMoves, BB sqToBlockCheck = fullBB) const;

    void kingMoves(BB king, BB enemyOccupies, BB friendOccupies, BB dangerSq, std::vector<MV> &otherMoves) const;
    void kingCaptures(BB king, BB enemyOccupies, BB dangerSq, std::vector<MV> &otherMoves) const;

    // -------------------------------------------------------------------------------------
};

#endif // MOVEGENERATOR_H

/**************************************************************************
**
**   Name: PositionCreator.cpp
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#include "PositionCreator.h"

PositionCreator::PositionCreator(const MoveGenerator& move_gen) :
    move_gen_(move_gen)
{

}

PositionBB PositionCreator::createPositionBB(const PositionWith8x8 &pos8x8)
{
    PositionBB posBB(pos8x8);
    posBB.attackedSqByWhite = move_gen_.attackedSqBy(WHITE, posBB);
    posBB.attackedSqByBlack = move_gen_.attackedSqBy(BLACK, posBB);
    return posBB;
}

PositionBB PositionCreator::createPositionBB(const std::string &FEN)
{
    PositionWith8x8 pos8x8(FEN);
    PositionBB posBB(pos8x8);
    posBB.attackedSqByWhite = move_gen_.attackedSqBy(WHITE, posBB);
    posBB.attackedSqByBlack = move_gen_.attackedSqBy(BLACK, posBB);
    return posBB;
}

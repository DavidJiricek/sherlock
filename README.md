Sherlock
========

Chess engine in C++ for Linux, which communicates using terminal and standart I/O. 

It support Universal Chess Interface - [UCI](https://www.shredderchess.com/chess-features/uci-universal-chess-interface.html). 
UCI describes communication between engine and chess graphical user interface (GUI). 
Its specification is located in the file _engine\_interface.txt_ in the folder _documentation_.

May be run from the command line, but for the best experience, 
it is recommended to load it into the chess GUI, such as [Arena](http://www.playwitharena.de/).
The GUI has to support UCI. 

Created as a semestral programming project. Documentation is available only in the Czech language.



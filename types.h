/**************************************************************************
**
**   Name: types.h
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2018
**
**************************************************************************/
#ifndef TYPES_H
#define TYPES_H

#include <cstdint>
#include <string>
#include <array>
#include <algorithm>
#include <map>
#include <exception>

using MV = std::uint32_t;
using BB = std::uint64_t; //BB is BitBoard - symbol for unsigned int64, that represent the game board
using U64 = std::uint64_t; //symbol for unsingned int64, that does NOT represent the game board
using U32 = std::uint32_t; //symbol for unsingned int32
using U8 = std::uint8_t; //symbol for unsingned int8

extern int mainCaptures;

extern int mainChecks;

enum FigureType {PAWN, KNIGHT, BISHOP, ROOK, QUEEN, KING};

enum FigureTypeColoured {W_PAWN, W_KNIGHT, W_BISHOP, W_ROOK, W_QUEEN, W_KING,
                         B_PAWN, B_KNIGHT, B_BISHOP, B_ROOK, B_QUEEN, B_KING, NONE};

enum SideToPlay : bool {
    WHITE = true,
    BLACK = false,
};

enum Square {
    A1, B1, C1, D1, E1, F1, G1, H1,
    A2, B2, C2, D2, E2, F2, G2, H2,
    A3, B3, C3, D3, E3, F3, G3, H3,
    A4, B4, C4, D4, E4, F4, G4, H4,
    A5, B5, C5, D5, E5, F5, G5, H5,
    A6, B6, C6, D6, E6, F6, G6, H6,
    A7, B7, C7, D7, E7, F7, G7, H7,
    A8, B8, C8, D8, E8, F8, G8, H8,
};

inline std::string SquareToString(Square sq){
    std::string result;
    result = static_cast<char>('a' + sq%8);
    result += static_cast<char>('1' + sq/8);
    return result;
}

enum class MV_Flags : U8 {NO,
                          CASTLING_WK, CASTLING_WQ, CASTLING_BK, CASTLING_BQ,
                          ENPASSANT_W,ENPASSANT_B,
                          PROMOTION_N, PROMOTION_B, PROMOTION_R, PROMOTION_Q};


#endif // TYPES_H

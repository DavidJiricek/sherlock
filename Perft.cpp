/**************************************************************************
**
**   Name: Pertf.cpp
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#include "Perft.h"

Perft::Perft(const MoveGenerator &move_gen):
    moveGen(move_gen)
{

}

U64 Perft::go_perft(PositionBB pos, size_t depth)
{
    U64 result = 0;
    if (depth == 0) {
        return 1;
    }

    std::vector<MV> moves;
    moveGen.allPossibleMoves(pos, moves);
    for (auto&& move : moves) {
        PositionBB newPos = pos;
        moveGen.updatePosPartial(newPos, move);
        result += go_perft(newPos, depth - 1);
    }
    return result;
}

U64 Perft::go_qperft(PositionBB pos, size_t depth)
{
    U64 result = 0;
    if (depth == 1) {
        std::vector<MV> moves;
        moveGen.allPossibleMoves(pos, moves);
        return moves.size();
    }

    std::vector<MV> moves;
    moveGen.allPossibleMoves(pos, moves);
    for (auto&& move : moves) {
        PositionBB newPos = pos;
        moveGen.updatePosPartial(newPos, move);
        result += go_qperft(newPos, depth - 1);
    }
    return result;
}

U64 Perft::go_capture_only_perft(PositionBB pos, size_t depth)
{
    U64 result = 0;
    if (depth == 0) {
        return 1;
    }

    std::vector<MV> moves;
    moveGen.allPossibleCaptures(pos, moves);
    for (auto&& move : moves) {
        PositionBB newPos = pos;
        moveGen.updatePosPartial(newPos, move);
        result += go_capture_only_perft(newPos, depth - 1);
    }
    return result;
}

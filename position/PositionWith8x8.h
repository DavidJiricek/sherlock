/**************************************************************************
**
**   Name: PositionWith8x8.h
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#ifndef POSITIONWITH8X8_H
#define POSITIONWITH8X8_H

#include <array>
#include <map>
#include <iostream>

#include "types.h"
#include "AbstractPosition.h"
#include "utilities/TextProccesing.h"
#include "move_generation/Move_LongNotation.h"

class PositionWith8x8 : public AbstractPosition
{
public:
    PositionWith8x8(const std::string& FENstring);

    PositionWith8x8(AbstractPosition &otherInfo, std::array<FigureTypeColoured, 64> &chessBoard)
        : AbstractPosition(otherInfo), chessBoard(chessBoard)
    {}

    void FENrank_ToChessBoard(int rankNumber, const std::string& FEN_rank){
            int fileNumber = 0;
            auto iter = FEN_rank.begin();
            while (iter != FEN_rank.end()){
                if (isalpha(*iter)){
                    FigureTypeColoured fig = FEN_FigureTypeMap.find(*iter)->second;
                    chessBoard[rankNumber*8+fileNumber] = fig;
                    ++fileNumber;
                } else {
                    fileNumber = (*iter - '0') + fileNumber;
                }
                ++iter;
            }
    }

    void visualize(){
        std::array<char, 8> rankNames {{'1','2','3','4','5','6','7','8'}};
        std::array<char, 8> fileNames {{'A','B','C','D','E','F','G','H'}};

        if (sideToPlay == WHITE){
            std::cout << "White to play:" << std::endl << std::endl;
        } else {
            std::cout << "Black to play:" << std::endl << std::endl;
        }

        for (int rank = 7; rank >= 0; --rank){
            int file = 0;
            std::cout << rankNames[rank] << " | ";
            for (file = 0; file <= 7; ++file) {
                auto iter = std::find_if(FEN_FigureTypeMap.begin(), FEN_FigureTypeMap.end(),
                             [this, rank, file](const std::pair<char,FigureTypeColoured>& pair)
                                -> bool { return pair.second == chessBoard[rank*8+file]; });
                if (iter != FEN_FigureTypeMap.end()){
                    std::cout << iter->first << " ";
                } else {
                    std::cout << "  ";
                }
            }
            std::cout << std::endl;
        }
        std::cout << "    ________________" << std::endl;
        std::cout << "    ";
        for (auto &&x : fileNames){
            std::cout << x << " ";
        }
        std::cout << std::endl << std::endl;
        std::cout << "Castling rights" << std::endl;
        std::cout << "W Qside: " << castlingWhiteQside << "  W Kside: " << castlingWhiteKside << std::endl;
        std::cout << "B Qside: " << castlingBlackQside << "  B Kside: " << castlingBlackKside << std::endl;
        std::cout << std::endl;
        std::cout << "En passant possible: " << std::to_string(enPassantPossible) << " " << SquareToString(enPassantTargetSquare) << std::endl;
        std::cout << "Half moves: " << halfMoveClock << " Full moves: " << fullMoveClock << std::endl;
        std::cout << "FEN of this position: " << std::endl << this->toFEN() << std::endl << std::endl;
    }

    std::string toFEN(){
        std::string FEN;

        // Positions of figures
        int numberOfEmptySquares = 0;
        for (int rank = 7; rank >= 0; --rank) {
            for (int file = 0; file < 8; ++file) {
                auto iter = std::find_if(FEN_FigureTypeMap.begin(), FEN_FigureTypeMap.end(),
                             [this, rank, file](const std::pair<char,FigureTypeColoured>& pair)
                                -> bool { return pair.second == chessBoard[rank*8 + file]; });
                if (iter != FEN_FigureTypeMap.end()){
                    if(numberOfEmptySquares != 0){
                        FEN += numberOfEmptySquares + '0';
                        numberOfEmptySquares = 0;
                    }
                    FEN += iter->first;
                } else {
                    numberOfEmptySquares++;
                }
            }

            if(numberOfEmptySquares != 0){
                FEN += numberOfEmptySquares + '0';
                numberOfEmptySquares = 0;
            }

            if (rank != 0){
                FEN += '/';
            }

        }
       /* for (uint i = chessBoard.size() - 1; i >= 0; --i) {
            if ((i + 1)%8 == 0 && i != chessBoard.size() - 1){
                if(numberOfEmptySquares != 0){
                    FEN += numberOfEmptySquares + '0';
                    numberOfEmptySquares = 0;
                }
                FEN += '/';
            }
            auto iter = std::find_if(FEN_FigureTypeMap.begin(), FEN_FigureTypeMap.end(),
                         [this, i](const std::pair<char,FigureTypeColoured>& pair)
                            -> bool { return pair.second == chessBoard[i]; });
            if (iter != FEN_FigureTypeMap.end()){
                if(numberOfEmptySquares != 0){
                    FEN += numberOfEmptySquares + '0';
                    numberOfEmptySquares = 0;
                }
                FEN += iter->first;
            } else {
                numberOfEmptySquares++;
            }
        }*/

        // Side to play
        FEN += ' ';
        if (sideToPlay == WHITE){
            FEN += 'w';
        } else {
            FEN += 'b';
        }

        // Castling
        FEN += ' ';
        if (castlingWhiteKside){
            FEN += 'K';
        }
        if (castlingWhiteQside){
            FEN += 'Q';
        }
        if (castlingBlackKside){
            FEN += 'k';
        }
        if (castlingBlackQside){
            FEN += 'q';
        }

        if (!castlingBlackKside || !castlingBlackQside || !castlingWhiteKside || !castlingWhiteQside){
            FEN += '-';
        }

        // En passant
        FEN += ' ';
        if (enPassantPossible){
            FEN += enPassantTargetSquare%8 +'a';
            FEN += (enPassantTargetSquare/8)+'1';
        } else {
            FEN += '-';
        }

        // Half move and full move count
        FEN += ' ' + std::to_string(halfMoveClock) + ' ' + std::to_string(fullMoveClock);

        return FEN;
    }

    std::array<FigureTypeColoured, 64> chessBoard;

    static const std::map<char,FigureTypeColoured> FEN_FigureTypeMap;
    static const std::string startposFEN;

    void update(Move_LongNotation move);

};

#endif // POSITIONWITH8X8_H

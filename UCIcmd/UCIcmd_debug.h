/**************************************************************************
**
**   Name: UCIcmd_debug.h
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#ifndef UCICMD_DEBUG_H
#define UCICMD_DEBUG_H

#include "UCIcmd/UCIcmd.h"

class UCIcmd_debug : public UCIcmd
{
public:
    UCIcmd_debug(const std::string& id_name, const std::string& restOfCmd, Communicator* comm):
        UCIcmd(id_name, restOfCmd, comm)
    {

    }

    virtual void executeCmd();
};

#endif // UCICMD_DEBUG_H

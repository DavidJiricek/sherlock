/**************************************************************************
**
**   Name: UCIcmd_debug.cpp
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#include "UCIcmd_debug.h"

void UCIcmd_debug::executeCmd(){
    /*
    * debug [ on | off ]
        switch the debug mode of the engine on and off.
        In debug mode the engine should send additional infos to the GUI, e.g. with the "info string" command,
        to help debugging, e.g. the commands that the engine has received etc.
        This mode should be switched off by default and this command can be sent
        any time, also when the engine is thinking.
    */

    if (restOfCmd_ == "on"){
        comm_->setDebugMode(true);
    } else if (restOfCmd_ == "off") {
        comm_->setDebugMode(false);
    } else {
        throw CommExcep(id_name + " " + restOfCmd_, "debug [ on | off ]");
    }
}

TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++17 -pthread
LIBS += -pthread

CONFIG += c++17 console
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += object_parallel_to_source

SOURCES += \
    main.cpp \
    Kernel.cpp \
    Perft.cpp \
    SearchResult.cpp \
    utilities/Bitboard.cpp \
    utilities/TextProccesing.cpp \
    move_generation/MagicGenerator.cpp \
    move_generation/MoveGenerator.cpp \
    position/PositionWith8x8.cpp \
    position/PositionBB.cpp \
    position/AbstractPosition.cpp \
    position/PositionCreator.cpp \
    position/PositionEvaluator.cpp \
    comm/Reciever.cpp \
    comm/RecBuffer.cpp \
    comm/Communicator.cpp \
    UCIcmd/UCIcmd.cpp \
    UCIcmd/UCIcmd_uci.cpp \
    UCIcmd/UCIcmd_ready.cpp \
    UCIcmd/UCIcmd_debug.cpp \
    UCIcmd/UCIcmd_setoption.cpp \
    UCIcmd/UCIcmd_registration.cpp \
    UCIcmd/UCIcmd_quit.cpp \
    UCIcmd/UCIcmd_stop.cpp \
    UCIcmd/UCIcmd_ponderhit.cpp \
    UCIcmd/UCIcmd_newgame.cpp \
    UCIcmd/UCIcmd_position.cpp \
    UCIcmd/UCIcmd_go.cpp \
    UCIcmd/UCIcmd_perft.cpp \
    tests/Tests.cpp

HEADERS += \
    Kernel.h \
    Perft.h \
    SearchResult.h \
    utilities/Bitboard.h \
    utilities/TextProccesing.h \
    move_generation/MagicGenerator.h \
    move_generation/MoveGenerator.h \
    move_generation/Move_LongNotation.h \
    position/PositionWith8x8.h \
    position/PositionBB.h \
    position/AbstractPosition.h \
    position/PositionCreator.h \
    position/PositionEvaluator.h \
    comm/CommExcep.h \
    comm/Communicator.h \
    comm/Reciever.h \
    comm/RecBuffer.h \
    UCIcmd/UCIcmd.h \
    UCIcmd/UCIcmd_uci.h \
    UCIcmd/UCIcmd_ready.h \
    UCIcmd/UCIcmd_newgame.h \
    UCIcmd/UCIcmd_position.h \
    UCIcmd/UCIcmd_go.h \
    UCIcmd/UCIcmd_debug.h \
    UCIcmd/UCIcmd_setoption.h \
    UCIcmd/UCIcmd_registration.h \
    UCIcmd/UCIcmd_quit.h \
    UCIcmd/UCIcmd_stop.h \
    UCIcmd/UCIcmd_ponderhit.h \
    UCIcmd/UCIcmd_perft.h \
    tests/Tests.h \
    types.h

/**************************************************************************
**
**   Name: MagicGenerator.cpp
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2018
**
**************************************************************************/
#include "MagicGenerator.h"
#include "utilities/Bitboard.h"

#include <random>
#include <limits.h>
#include <iostream>



MagicGenerator::MagicGenerator(std::array<BB, 64> &RookMasks,
                               std::array<BB, 64> &BishopMasks):
    gen(rd()), randomU64(0,ULLONG_MAX),
    RookMasks(RookMasks), BishopMasks(BishopMasks)
{
    /*for (int sq = 0; sq < 64; ++sq) {
        RookMagicNumbers[sq] = generateMagicNumber(static_cast<Square>(sq), ROOK);
        BishopMagicNumbers[sq] = generateMagicNumber(static_cast<Square>(sq), BISHOP);
    }
    std::cout<<trial_count<<std::endl;*/
}

BB MagicGenerator::indexToBlockers(BB figureMask, int index, int indexBitCount) const
{
    BB blockers = 0;
    for (int i = 0; i < indexBitCount; ++i) {
        if (index & (1 << i)){
            blockers |= (1ULL << Bitboard::zero_lsb(figureMask));
        } else {
            Bitboard::zero_lsb(figureMask);
        }
    }
    return blockers;
}

BB MagicGenerator::possibleAttacksBB(const Square sq, const FigureType figType, const BB blockers)
{
    BB possibleAttacks = 0;
    int sq_rank = sq/8;
    int sq_file = sq%8;

    int rank = sq_rank;
    int file = sq_file;
    if (figType == BISHOP) {
        for (rank = sq_rank + 1, file = sq_file + 1; rank <= 7 && file <= 7; ++rank, ++file){
            BB square = 1ULL << (rank*8 + file);
            possibleAttacks |= square;
            if (blockers & square) {
                break;
            }
        }
        for (rank = sq_rank - 1, file = sq_file + 1; rank >= 0  && file <= 7; --rank, ++file){
            BB square = 1ULL << (rank*8 + file);
            possibleAttacks |= square;
            if (blockers & square) {
                break;
            }
        }
        for (rank = sq_rank - 1, file = sq_file - 1; rank >= 0  && file >= 0; --rank, --file){
            BB square = 1ULL << (rank*8 + file);
            possibleAttacks |= square;
            if (blockers & square) {
                break;
            }
        }
        for (rank = sq_rank + 1, file = sq_file - 1; rank <= 7  && file >= 0; ++rank, --file){
            BB square = 1ULL << (rank*8 + file);
            possibleAttacks |= square;
            if (blockers & square) {
                break;
            }
        }
    } else if (figType == ROOK) {
        for (rank = sq_rank + 1, file = sq_file; rank <= 7; ++rank){
            BB square = 1ULL << (rank*8 + file);
            possibleAttacks |= square;
            if (blockers & square) {
                break;
            }
        }
        for (rank = sq_rank, file = sq_file + 1; file <= 7; ++file){
            BB square = 1ULL << (rank*8 + file);
            possibleAttacks |= square;
            if (blockers & square) {
                break;
            }
        }
        for (rank = sq_rank - 1, file = sq_file; rank >= 0; --rank){
            BB square = 1ULL << (rank*8 + file);
            possibleAttacks |= square;
            if (blockers & square) {
                break;
            }
        }
        for (rank = sq_rank, file = sq_file - 1; file >= 0; --file){
            BB square = 1ULL << (rank*8 + file);
            possibleAttacks |= square;
            if (blockers & square) {
                break;
            }
        }
    }
    return possibleAttacks;
}
std::pair<U64, std::array<BB, 4096>> MagicGenerator::generateMagicPair(const Square sq, const FigureType figType)
{
    U64 mask = 0;
    int indexBitCount = 0;

    if (figType == BISHOP){
        mask = BishopMasks[sq];
        indexBitCount = BishopIndexBitCounts[sq];
    } else if (figType == ROOK) {
        mask = RookMasks[sq];
        indexBitCount = RookIndexBitCounts[sq];
    }

    std::vector<BB> allAttacks;
    std::vector<BB> allBlockers;
    std::array<BB, 4096> used;


    for (int index = 0; index < (1 << indexBitCount); ++index) {
        allBlockers.push_back(indexToBlockers(mask, index, indexBitCount));
        allAttacks.push_back(possibleAttacksBB(sq,figType, allBlockers[index]));
    }


    for (int trial = 0;  trial < 1000000; ++trial) {

        U64 trial_magic = generateSparseU64();
        bool trial_magicOKsoFar = true;
        if (Bitboard::count_1s((trial_magic * mask) & 0xFF00000000000000ULL) < 6) continue;
        std::fill(std::begin( used ), std::end( used ), 0 );
        for (int index = 0;  index < (1 << indexBitCount); ++index) {
            int hash = computeHash(allBlockers[index], trial_magic, indexBitCount);
            if (used[hash] == 0) {
                used[hash] = allAttacks[index];
            } else if (used[hash] == allAttacks[index]) {
                continue;
            } else {
                trial_magicOKsoFar = false;
                break;
            }
        }

        if (trial_magicOKsoFar){
            std::cout << "Square: " << sq << " Trial: " << trial << std::endl;
            //return trial_magic;
            return std::make_pair(trial_magic, used);
        }
    }
    return std::make_pair(0, used);
}



U64 MagicGenerator::generateSparseU64()
{
    return randomU64(gen) & randomU64(gen) & randomU64(gen);
}

void MagicGenerator::generateMagicAndHashTable(const Square sq, const FigureType figType, U64 &magicNumber, std::array<BB, 4096> &hashedAttacks)
{
    U64 mask = 0;
    int indexBitCount = 0;

    if (figType == BISHOP){
        mask = BishopMasks[sq];
        indexBitCount = BishopIndexBitCounts[sq];
    } else if (figType == ROOK) {
        mask = RookMasks[sq];
        indexBitCount = RookIndexBitCounts[sq];
    }

    std::vector<BB> allAttacks;
    std::vector<BB> allBlockers;


    for (int index = 0; index < (1 << indexBitCount); ++index) {
        allBlockers.push_back(indexToBlockers(mask, index, indexBitCount));
        allAttacks.push_back(possibleAttacksBB(sq,figType, allBlockers[index]));
    }


    for (int trial = 0;  trial < 1000000; ++trial) {

        U64 trial_magic = generateSparseU64();
        bool trial_magicOKsoFar = true;
        if (Bitboard::count_1s((trial_magic * mask) & 0xFF00000000000000ULL) < 6) continue;
        std::fill(std::begin( hashedAttacks ), std::end( hashedAttacks ), 0 );

        for (int index = 0;  index < (1 << indexBitCount); ++index) {
            int hash = computeHash(allBlockers[index], trial_magic, indexBitCount);
            if (hashedAttacks[hash] == 0) {
                hashedAttacks[hash] = allAttacks[index];
            } else if (hashedAttacks[hash] == allAttacks[index]) {
                continue;
            } else {
                trial_magicOKsoFar = false;
                break;
            }
        }

        if (trial_magicOKsoFar){
            magicNumber = trial_magic;
            std::cout << ", " << std::showbase << std::hex << std::uppercase << magicNumber << std::endl;
            return;
        }
    }

}

void MagicGenerator::fillHashTable(const Square sq, const FigureType figType, const U64 magicNumber, std::array<BB, 4096> &hashedAttacks)
{
    U64 mask = 0;
    int indexBitCount = 0;

    if (figType == BISHOP){
        mask = BishopMasks[sq];
        indexBitCount = BishopIndexBitCounts[sq];
    } else if (figType == ROOK) {
        mask = RookMasks[sq];
        indexBitCount = RookIndexBitCounts[sq];
    }

    std::vector<BB> allAttacks;
    std::vector<BB> allBlockers;


    for (int index = 0; index < (1 << indexBitCount); ++index) {
        allBlockers.push_back(indexToBlockers(mask, index, indexBitCount));
        allAttacks.push_back(possibleAttacksBB(sq,figType, allBlockers[index]));
    }

    std::fill(std::begin( hashedAttacks ), std::end( hashedAttacks ), 0 );

    for (int index = 0;  index < (1 << indexBitCount); ++index) {
        int hash = computeHash(allBlockers[index], magicNumber, indexBitCount);
        if (hashedAttacks[hash] == 0) {
            hashedAttacks[hash] = allAttacks[index];
        } else if (hashedAttacks[hash] == allAttacks[index]) {
            continue;
        } else {
            std::cout << std::endl << "ERROR on " << sq << std::endl;
            break;
        }
    }
}



int MagicGenerator::computeHash(BB blockers, U64 magic, int hashBitCount)
{
    return (unsigned)((int)blockers*(int)magic ^ (int)(blockers>>32)*(int)(magic>>32)) >> (32-hashBitCount);
}


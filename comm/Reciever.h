/**************************************************************************
**
**   Name: Reciever.h
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#ifndef RECIEVER_H
#define RECIEVER_H

#include <istream>
#include <queue>
#include <string>
#include <istream>

#include <spdlog/spdlog.h>

#include "RecBuffer.h"
#include "CommExcep.h"


class Reciever
{
public:
    Reciever(RecBuffer& buffer, std::istream& inStream):
        buffer_(buffer), inStream_(inStream)
    {

    }

    void operator()()
    {
        readFromGUI();
    }

    void readFromGUI();

    RecBuffer& buffer_;
    std::istream& inStream_;

};

#endif // RECIEVER_H

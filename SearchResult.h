/**************************************************************************
**
**   Name: SearchResults.h
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#ifndef SEARCHRESULTS_H
#define SEARCHRESULTS_H

#include "position/PositionBB.h"
#include "types.h"

class SearchResult
{
public:
    SearchResult(PositionBB only_for_proper_init);

    PositionBB root;
    U8 searchDepth;
    U32 nodesVisited;
    std::vector<MV> princVar;
    double eval_score; //mate has score 10000
    int movesToMate;
    U32 searchTime_milis;
    U32 nodesPerSecond;

    std::string formatForOutput();
};

#endif // SEARCHRESULTS_H

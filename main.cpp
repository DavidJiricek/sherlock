/**************************************************************************
**
**   Name: main.cpp
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2018
**
**************************************************************************/

#include <iostream>
#include <string>
#include <chrono>

#include "Kernel.h"
#include "comm/Communicator.h"
#include "tests/Tests.h"

#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h"

#include <stdio.h>
#include <stdlib.h>

#define USE_32_BIT_MULTIPLICATIONS

int main(int argc, char *argv[])
{
    remove("logs/basic_log.txt");
    auto file_logger = spdlog::basic_logger_mt("basic_logger", "logs/basic.txt");
    spdlog::set_default_logger(file_logger);

    Kernel kernel;
    Communicator comm(std::cin, std::cout, kernel);
    try {
        comm.commWithGUI();
        spdlog::info("Engine exited succesfully with error code 0");
        return 0;
    } catch (CommExcep& e) {
        std::cout << e.msg() << std::endl;
        spdlog::info("Engine encountered error, error code 1");
        return 1;
    }
}


/**************************************************************************
**
**   Name: UCIcmd_newgame.h
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#ifndef UCICMD_NEWGAME_H
#define UCICMD_NEWGAME_H


#include "spdlog/spdlog.h"

#include "UCIcmd/UCIcmd.h"

class UCIcmd_newgame : public UCIcmd
{
public:
    UCIcmd_newgame(const std::string& id_name, const std::string& restOfCmd, Communicator* comm):
        UCIcmd(id_name, restOfCmd, comm)
    {

    }

    virtual void executeCmd();
};

#endif // UCICMD_NEWGAME_H

/**************************************************************************
**
**   Name: Communicator.h
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2018
**
**************************************************************************/
#ifndef COMMUNICATOR_H
#define COMMUNICATOR_H

#include <iostream>
#include <string>
#include <vector>
#include <queue>
#include <array>
#include <set>
#include <map>
#include <memory>
#include <mutex>

#include "SearchResult.h"
#include "Kernel.h"
#include "CommExcep.h"
#include "utilities/TextProccesing.h"
#include "comm/RecBuffer.h"

// Forward declaration of UCIcmd classes.
class UCIcmd;
class UCIcmd_uci;

/**
 * Main class responsible for communicating with user or GUI.
 *
 * Commands are recieved and stored in RecBuffer.
 * Communicator then read commands from buffer and executes them.
 */
class Communicator
{
public:
    Communicator(std::istream& inStream,
                 std::ostream& outStream,
                 Kernel& kernel);

    /// Main loop - reads commands from buffer and executes them.
    void commWithGUI();

    void setDebugMode(bool debugMode);

    void sendToGUI(const std::string &message) const;
    void sendAndPrintInGUI(const std::string &message) const;

    Kernel& kernel_;

    void quit();

private:
    std::istream& inStream_;
    std::ostream& outStream_;

    bool debugMode_;
    bool isCommunicatorWorking;

    RecBuffer buffer_;

    /// Input is whole line that is read from RecBuffer.
    /// Output is executable UCI command.
    std::unique_ptr<UCIcmd> parseCmd(const std::string &input);

    /// Create UCIcmd from name and parameters
    std::unique_ptr<UCIcmd> builtUCIcmd(std::string &id, std::string &restOfCmd);
};

#endif // COMMUNICATOR_H

/**************************************************************************
**
**   Name: Bitboard.cpp
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2018
**
**************************************************************************/
#include "Bitboard.h"

#include <iostream>
#include <array>

Bitboard::Bitboard()
{

}


void Bitboard::visualizeBB(BB bitBoard)
{
    std::array<char, 8> rankNames {{'1','2','3','4','5','6','7','8'}};
    std::array<char, 8> fileNames {{'A','B','C','D','E','F','G','H'}};

    for (int rank = 7; rank >= 0; --rank){
        int file = 0;
        std::cout << rankNames[rank] << " | ";
        for (file = 0; file <= 7; ++file) {
            if (bitBoard & (1ULL << (rank*8 + file))){
                std::cout << "1 ";
            } else {
                std::cout << "0 ";
            }
        }
        std::cout << std::endl;
    }
    std::cout << "    ________________" << std::endl;
    std::cout << "    ";
    for (auto &&x : fileNames){
        std::cout << x << " ";
    }
    std::cout << std::endl << std::endl;
}

BB Bitboard::arrayToBB(std::array<bool, 64> board)
{
    BB result = 0;
    for(int i = 0; i < 64; ++i){
        if(board[i] == 1){
            result |= (1ULL << i);
        } else {
            continue;
        }
    }
    return result;
}

std::array<bool, 64> Bitboard::BBtoArray(BB bitBoard)
{
    std::array<bool, 64> result;
    for (unsigned int i = 0; i < result.size(); ++i) {
        if (bitBoard & (1ULL << i)){
            result[i] = true;
        } else {
            result[i] = false;
        }
    }
    return result;
}

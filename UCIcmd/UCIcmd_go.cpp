/**************************************************************************
**
**   Name: UCIcmd_go.cpp
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#include "UCIcmd_go.h"
#include <deque>

void UCIcmd_go::executeCmd(){
    /* go
        start calculating on the current position set up with the "position" command.
        There are a number of commands that can follow this command, all will be sent in the same string.
        If one command is not sent its value should be interpreted as it would not influence the search.
        * searchmoves <move1> .... <movei>
            restrict search to this moves only
            Example: After "position startpos" and "go infinite searchmoves e2e4 d2d4"
            the engine should only search the two moves e2e4 and d2d4 in the initial position.
        * ponder
            start searching in pondering mode.
            Do not exit the search in ponder mode, even if it's mate!
            This means that the last move sent in in the position string is the ponder move.
            The engine can do what it wants to do, but after a "ponderhit" command
            it should execute the suggested move to ponder on. This means that the ponder move sent by
            the GUI can be interpreted as a recommendation about which move to ponder. However, if the
            engine decides to ponder on a different move, it should not display any mainlines as they are
            likely to be misinterpreted by the GUI because the GUI expects the engine to ponder
           on the suggested move.
        * wtime <x>
            white has x msec left on the clock
        * btime <x>
            black has x msec left on the clock
        * winc <x>
            white increment per move in mseconds if x > 0
        * binc <x>
            black increment per move in mseconds if x > 0
        * movestogo <x>
          there are x moves to the next time control,
            this will only be sent if x > 0,
            if you don't get this and get the wtime and btime it's sudden death
        * depth <x>
            search x plies only.
        * nodes <x>
           search x nodes only,
        * mate <x>
            search for a mate in x moves
        * movetime <x>
            search exactly x mseconds
        * infinite
            search until the "stop" command. Do not exit the search without being told so in this mode!
    */

    spdlog::debug("Go recieved.");

    // String restOfCmd is already without duplicate whitespaces
    // and all whitespaces are equal to ordinary space ' '
    // We can separate them.
    const std::vector<std::string> params_vec = TextProccesing::separateString(restOfCmd_, [](char a){return isspace(a);});
    std::deque<std::string> params;
    std::copy(params_vec.begin(), params_vec.end(), std::back_inserter(params));

    //
    while (!params.empty()){
        std::string param = params.front();
        params.pop_front();

        auto iter = parseMap.find(param);
        if (iter != parseMap.end()){
            iter->second(params);
        }
    }

    SearchResult result = comm_->kernel_.go();


    Move_LongNotation bestMoveLN(result.princVar[0]);
    for (auto&& x : result.princVar){ comm_->sendToGUI(Move_LongNotation(x).text_form_);}
    comm_->sendToGUI("bestmove " + bestMoveLN.text_form_);
    comm_->sendToGUI(result.formatForOutput());
}

void UCIcmd_go::initParseMap()
{
    parseMap.emplace(std::make_pair("depth", std::bind(&UCIcmd_go::setDepth, this, std::placeholders::_1)));
    parseMap.emplace(std::make_pair("nodes", std::bind(&UCIcmd_go::setNodes, this, std::placeholders::_1)));
}

void UCIcmd_go::setDepth(std::deque<std::string>& params)
{
    std::string depth = params.front();
    params.pop_front();

    U32 depth_;
    try {
        depth_ = std::stoul(depth);
        if (depth_ < 1 || depth_ > 255){
            throw;
        }
    } catch (...) {
        throw CommExcep(id_name + " " + restOfCmd_, "After \"depth\" there should be an integer between 1 and 255.");
    }
    comm_->kernel_.setSearchDepth(depth_);
}

void UCIcmd_go::setNodes(std::deque<std::string>& params)
{
    std::string nodes_to_search = params.front();
    params.pop_front();

    U32 nodes_to_search_;
    try {
        nodes_to_search_ = std::stoul(nodes_to_search);
        if (nodes_to_search_ < 1 || nodes_to_search_ > std::numeric_limits<U32>::max()){
            throw;
        }
    } catch (...) {
        throw CommExcep(id_name + " " + restOfCmd_, "After \"nodes\" there should be an integer greater than 0.");
    }
    comm_->kernel_.setMaxNodes(nodes_to_search_);
}



/**************************************************************************
**
**   Name: UCIcmd_position.h
**   Author: David Jiricek <jiricek.dave@seznam.cz>
**   Date: 2019
**
**************************************************************************/
#ifndef UCICMD_POSITION_H
#define UCICMD_POSITION_H

//2020-03-28 14:38:55.535-->1:position startpos moves e2e4
//2020-03-28 14:38:55.535-->1:go wtime 300000 btime 300000 winc 0 binc 0

#include "spdlog/spdlog.h"

#include "UCIcmd/UCIcmd.h"
#include "position/PositionWith8x8.h"

class UCIcmd_position : public UCIcmd
{
public:
    UCIcmd_position(const std::string& id_name, const std::string& restOfCmd, Communicator* comm):
        UCIcmd(id_name, restOfCmd, comm)
    {

    }

    virtual void executeCmd();
};

#endif // UCICMD_POSITION_H

